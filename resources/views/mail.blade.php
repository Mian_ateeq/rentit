<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link  href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<style>
    
    #thank .area_menu a, #thank .booknowbtn a, #thank .footer_menu a, #thank .img_btn a, #thank .myButton, #thank .requestquote_btn1 a, #thank .text_btn a {
        text-decoration: none;
    }
    #thank a {
        font-family: utsaahregular;
    }
    @font-face {
        font-family: MyriadPro-Light;
        src: url(../fonts/fontie_14357351183568/MyriadPro-Light_gdi.eot);
        src: url(../fonts/fontie_14357351183568/MyriadPro-Light_gdi.eot?#iefix) format('embedded-opentype'), url(../fonts/fontie_14357351183568/MyriadPro-Light_gdi.woff) format('woff'), url(../fonts/fontie_14357351183568/MyriadPro-Light_gdi.ttf) format('truetype'), url(../fonts/fontie_14357351183568/MyriadPro-Light_gdi.svg#MyriadPro-Light) format('svg');
        font-weight: 300;
        font-style: normal;
        font-stretch: normal;
        unicode-range: U20-25CA;
    }
    @font-face {
        font-family: MyriadPro-Cond;
        src: url(../fonts/fontie_14357359934330/MyriadPro-Cond_gdi.eot);
        src: url(../fonts/fontie_14357359934330/MyriadPro-Cond_gdi.eot?#iefix) format('embedded-opentype'), url(../fonts/fontie_14357359934330/MyriadPro-Cond_gdi.woff) format('woff'), url(../fonts/fontie_14357359934330/MyriadPro-Cond_gdi.ttf) format('truetype'), url(../fonts/fontie_14357359934330/MyriadPro-Cond_gdi.svg#MyriadPro-Cond) format('svg');
        font-weight: 400;
        font-style: normal;
        font-stretch: condensed;
        unicode-range: U20-25CA;
    }
    @font-face {
        font-family: Vrinda;
        src: url(../fonts/Vrinda/Vrinda.eot?#iefix) format('embedded-opentype'), url(../fonts/Vrinda/Vrinda.woff) format('woff'), url(../fonts/Vrinda/Vrinda.ttf) format('truetype'), url(../fonts/Vrinda/Vrinda.svg#Vrinda) format('svg');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: ErasITC-Medium;
        src: url(../fonts/ErasITC-Medium/ErasITC-Medium.eot?#iefix) format('embedded-opentype'), url(../fonts/ErasITC-Medium/ErasITC-Medium.woff) format('woff'), url(../fonts/ErasITC-Medium/ErasITC-Medium.ttf) format('truetype'), url(../fonts/ErasITC-Medium/ErasITC-Medium.svg#ErasITC-Medium) format('svg');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'ralewaysemibold';
        src: url(../fonts/ralewaybold/raleway-semibold-webfont.woff2) format('woff2'), url(../fonts/ralewaybold/raleway-semibold-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'ralewaylight';
        src: url(../fonts/ralewaylight/raleway-light-webfont.woff2) format('woff2'), url(../fonts/ralewaylight/raleway-light-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'ralewayregular';
        src: url(../fonts/raleway-regular/raleway-regular-webfont.woff2) format('woff2'), url(../fonts/raleway-regular/raleway-regular-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'ralewaymedium';
        src: url(../fonts/raleyway-medium/raleway-medium-webfont.woff2) format('woff2'), url(../fonts/raleyway-medium/raleway-medium-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'oswaldmedium';
        src: url(../fonts/oswald-medium/oswald-medium-590b15ef9cf67-webfont.woff2) format('woff2'), url(../fonts/oswald-medium/oswald-medium-590b15ef9cf67-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'robotobold';
        src: url(../fonts/roboto-bold/roboto-bold-webfont.woff2) format('woff2'), url(../fonts/roboto-bold/roboto-bold-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'robotoregular';
        src: url(../fonts/roboto-regular/roboto-regular-webfont.woff2) format('woff2'), url(../fonts/roboto-regular/roboto-regular-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'robotomedium';
        src: url(../fonts/roboto-medium/roboto-medium-webfont.woff2) format('woff2'), url(../fonts/roboto-medium/roboto-medium-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'robotoblack';
        src: url(../fonts/robto-black/roboto-black-webfont.woff2) format('woff2'), url(../fonts/robto-black/roboto-black-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'robotothin';
        src: url(../fonts/roboto-thin/roboto-thin-webfont.woff2) format('woff2'), url(../fonts/roboto-thin/roboto-thin-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @font-face {
        font-family: 'robotolight';
        src: url(../fonts/Roboto-light/roboto-light-webfont.woff2) format('woff2'), url(../fonts/Roboto-light/roboto-light-webfont.woff) format('woff');
        font-weight: 400;
        font-style: normal;
    }
    @media (min-width: 768px) {
        #thank .leading_bg {
            background-color: #1e2b37;
        }
    }
    @media (max-width: 768px) {
        #thank .copy_right p {
            text-align: center !important;
        }
        #thank .copy_left {
            text-align: center !important;
        }
        #thank .leading_bg {
            background-color: #1f2b37 !important;
        }
        #thank .leading_txt p {
            padding-left: 0px !important;
        }
        #thank .leading_txt1 p {
            padding-left: 0px !important;
        }
        #thank .leading_txt h2 {
            padding-left: 0px !important;
        }
        #thank .leading_txt2 h2 {
            padding-left: 0px !important;
        }
        #thank .leading_txt2 p {
            padding-left: 0px !important;
        }
        #thank .carousel-caption {
            left: 8% !important;
            right: 8% !important;
        }
        #thank .label1, #thank .label2 {
            margin-bottom: 6px !important;
        }
        #thank .rentcar_bookingform {
            background-color: #1e2b37 !important;
        }
        #thank .logo {
            z-index: 2147483647 !important;
        }
        #thank .guote_btn {
            border: 1px solid #ccc !important;
            border-radius: 4px !important;
            margin-top: 25px !important;
        }
        #thank .quoteme_now1 {
            margin-top: 1px !important;
        }
        #thank .main.clearfix.visible-xs {
            margin-top: -52px !important;
            margin-bottom: 28px !important;
        }
        #thank .specialdis_sec.col-lg-7 > iframe {
            width: 328px !important;
            margin-top: 18px !important;
            border: 1px solid #ccc !important;
        }
        #thank .specialdis_sec {
            margin-left: -30px !important;
        }
        #thank .whychose_us {
            margin-top: 15px !important;
        }
        #thank .logo {
            margin-bottom: -40px !important;
        }
        #thank .column {
            z-index: 2147483647 !important;
        }
        #thank .main.clearfix.visible-xs {
            float: left !important;
            height: 80px !important;
        }
        #thank .callus.col-xs-12.col-lg-3 > a {
            color: #4c4c4c !important;
        }
        #thank .callus p {
            margin-top: -13px !important;
        }
        #thank .callus.col-xs-12.col-lg-3 {
            margin-top: 63px !important;
        }
        #thank #subject {
            width: 250px !important;
        }
        #thank .submit_btn1 {
            margin-top: 22px !important;
            margin-bottom: 11px !important;
            float: left !important;
        }
        #thank .whychose_us {
            height: 226px !important;
        }
        #thank .vehichle_det {
            margin-top: 20px !important;
        }
        #thank .contactform input {
            width: 242px !important;
        }
        #thank .contactform select, #thank .contactform textarea {
            width: 242px !important;
        }
        #thank .contactform textarea {
            height: 90px !important;
        }
        #thank .address_p {
            margin-left: 25px !important;
            margin-bottom: 14px !important;
        }
        #thank .callus {
            margin-bottom: -6px !important;
            border-radius: 4px !important;
        }
        #thank .callus p {
            width: 88px;
            float: left;
        }
        #thank .callus a {
            text-align: center !important;
        }
        #thank .booking_form input {
            width: 262px !important;
        }
        #thank .label1, #thank .label2 {
            margin-left: 14px !important;
        }
        #thank .quoteme_now1 {
            margin-left: 13px !important;
        }
        #thank .map {
            width: 222px !important;
            margin-left: 12px !important;
        }
        #thank .notice_p {
            float: left;
            margin-left: 8px !important;
        }
        #thank .notice_p1 {
            float: left;
            margin-top: 31px !important;
            margin-left: 10px !important;
        }
        #thank .contactbtn12, #thank .contactbtn123 {
            float: left;
            width: 100%;
            margin-top: 0;
        }
        #thank .noticeboard {
            float: left;
            min-height: 510px !important;
        }
        #thank .contactbtn12 {
            margin-bottom: 20px;
            margin-left: 8px !important;
        }
        #thank .contactbtn123 {
            margin-bottom: 8px;
        }
        #thank .form1 input {
            width: 249px !important;
        }
        #thank .logo {
            margin-top: 23px !important;
        }
        #thank .banner_section {
            margin-bottom: 15px !important;
        }
        #thank .dubai_bg {
            background-color: black !important;
        }
        #thank .dubai1_bx1 {
            margin-bottom: 14px !important;
        }
        #thank .dubai_bg2 {
            background-color: #222 !important;
        }
        #thank .pakistanimg_bg {
            background-color: #866a38 !important;
        }
        #thank .rawlpindi_bg {
            background-color: #8a8c87 !important;
        }
        #thank .sialkotbg {
            background: none !important;
        }
        #thank .sialkot_sc {
            background-color: #5a676f !important;
            margin-top: 19px !important;
        }
        #thank .sialkot_Sc1 {
            background-color: #5a676f !important;
            margin-top: 19px !important;
        }
        #thank .sialkotcar_txt1 h2 {
            color: white !important;
            font-size: 18px !important;
            padding-left: 16px !important;
        }
        #thank .sialkotcar_txt1 p {
            color: white !important;
            padding-left: 16px !important;
            padding-right: 16px !important;
        }
        #thank .sialkotcar_txt12 {
            padding-right: 16px !important;
            padding-left: 16px !important;
        }
        #thank .bookbtn11 {
            margin-bottom: 12px !important;
        }
        #thank .sargodha_sc {
            background-color: #ffcc01 !important;
        }
        #thank .sargodha_sc1 {
            background-color: #ffcc01 !important;
        }
        #thank .sargodha_bg {
            background: none !important;
        }
        #thank .sargodha_sc {
            margin-top: 12px !important;
        }
        #thank .sargodha_sc1 {
            margin-top: 12px !important;
        }
        #thank .sargodha_txt h1 {
            padding-left: 12px !important;
        }
        #thank .sargodha_txt h2 {
            padding-left: 12px !important;
        }
        #thank .sargodha_txt p {
            padding-left: 12px !important;
        }
        #thank .vicale_ocations h2 {
            padding-left: 12px !important;
            color: #000 !important;
        }
        #thank .vicale_ocations p {
            padding-left: 12px !important;
            color: #000 !important;
        }
        #thank .vicale_ocations1 h2 {
            padding-left: 12px !important;
            color: #000 !important;
        }
        #thank .vicale_ocations1 p {
            padding-left: 12px !important;
            color: #000 !important;
        }
        #thank .peshware_bg {
            background: none !important;
        }
        #thank .pesh_sc {
            margin-top: 22px !important;
        }
        #thank .renta_Txt {
            margin-left: 12px !important;
        }
        #thank .renta_p {
            margin-left: 12px !important;
            margin-top: 12px !important;
        }
        #thank .peshware_port {
            background-color: #2e76ad !important;
            margin-bottom: 12px !important;
        }
        #thank .peshware_port h2 {
            color: white !important;
            font-size: 30px !important;
            padding-left: 12px !important;
        }
        #thank .peshware_port p {
            color: white !important;
            padding-left: 12px !important;
        }
        #thank .peshware_Trv h2 {
            color: white !important;
            font-size: 30px !important;
            padding-left: 12px !important;
        }
        #thank .peshware_Trv p {
            color: white !important;
            padding-left: 12px !important;
        }
        #thank .faislabad_bg {
            background: none !important;
        }
        #thank .faislabad_sc {
            background-color: #b1001c !important;
        }
        #thank .rentcar_fais h2 {
            color: white !important;
            padding-left: 12px !important;
            font-size: 22px !important;
        }
        #thank .rentcar_fais h1 {
            color: white !important;
            padding-left: 12px !important;
            font-size: 22px !important;
        }
        #thank .rentcar_fais p {
            color: white !important;
            padding-right: 12px !important;
            padding-left: 12px !important;
        }
        #thank .bookvechle_sc {
            background-color: #b1001c !important;
            margin-top: 26px !important;
        }
        #thank .bookvechle_sc h2 {
            color: white !important;
            padding-left: 12px !important;
            font-size: 22px !important;
        }
        #thank .bookvechle_sc p {
            color: white !important;
            padding-left: 12px !important;
            padding-right: 12px !important;
        }
        #thank .dtllabel_l {
            margin-left: 14px !important;
        }
        #thank .dtlbtn_12 {
            margin-left: 14px !important;
        }
        #thank .detailpage_frm input {
            width: 267px !important;
        }
        #thank .col-lg-7 li {
            text-align: center !important;
        }
    }
    @media (min-width: 1200px) {
        #thank .copy_right p {
            float: right !important;
        }
        #thank .leading_bg {
            background: rgba(0, 0, 0, 0) url(../images/RentCarsWebsiteBlue-With-Images-Whitish-BG-2_02.jpg) no-repeat scroll 12% 0% !important;
        }
        #thank .slider_bggg {
            background: rgba(0, 0, 0, 0) url(../images/carlisting-bg_02.jpg) no-repeat scroll 12% 0% !important;
        }
        #thank .botm_bx {
            width: 357px !important;
            margin: 8px !important;
        }
        #thank .form-group.col-lg-2 {
            width: 23% !important;
        }
        #thank .form-group.col-lg-2 {
            margin-right: 24px !important;
        }
        #thank .navbar-fixed-top {
            top: 61px !important;
        }
        #thank .navbar-default.affix {
            margin-top: 104px !important;
        }
        #thank .navbar-default.affix {
            margin-top: -71px !important;
            width: 100% !important;
            margin-left: 0px !important;
            color: white !important;
            padding: 6px 0 0 173px !important;
            background-color: #1e2b37 !important;
        }
        #thank .navbar-default.affix .nav.navbar-nav {
            float: left !important;
            margin-left: 47px !important;
        }
        #thank .nav.navbar-nav {
            float: right !important;
            margin-right: 143px !important;
        }
        #thank .carousel-caption {
            position: absolute !important;
        }
        #thank .callus, #thank .copyright, #thank .submit_btn1, #thank .welcome_img {
            float: right !important;
        }
        #thank .bg_top {
            height: 5px;
        }
        #thank .menu {
            width: 810px !important;
            margin-left: 48px !important;
        }
        #thank .banner_section {
            width: 100%;
            height: 444px;
        }
        #thank .whychose_us {
            min-height: 195px !important;
            margin-top: 0 !important;
        }
        #thank .welcomeimg_text {
            width: 288px !important;
        }
        #thank .welcome_sec {
            width: 817px !important;
        }
        #thank .footerarea_menu, #thank .footermenu_bg {
            width: 100%;
        }
        #thank .searchform {
            margin-top: -413px !important;
        }
        #thank .carbox {
            width: 346px !important;
        }
        #thank .img_btn {
            width: 344px !important;
        }
        #thank .vehichle_det {
            width: 340px !important;
            margin-top: 127px !important;
        }
        #thank .booknowbtn {
            width: 176px !important;
        }
        #thank .contactform input, #thank .contactform textarea {
            width: 390px !important;
        }
        #thank .contactform textarea {
            height: 149px !important;
        }
        #thank .callus p {
            float: left !important;
        }
        #thank .callus a, #thank .navbar-nav {
            float: right !important;
        }
        #thank .booking_form input {
            width: 260px !important;
        }
        #thank .label1 {
            width: 273px !important;
        }
        #thank .label2 {
            width: 473px !important;
        }
        #thank .label2 input, #thank .label2 select {
            width: 534px !important;
        }
        #thank .booking_form {
            margin-left: 25px !important;
            margin-top: 4px !important;
            margin-bottom: 16px !important;
        }
        #thank #width3 {
            width: 430px !important;
        }
        #thank .bullet_sec {
            width: 220px !important;
        }
        #thank .callus p {
            margin-left: 50px !important;
        }
        #thank .dubai_bx {
            width: 565px !important;
            margin-right: 15px !important;
        }
        #thank .dubai1_bx1 {
            width: 372px !important;
            margin-right: 15px !important;
        }
        #thank .pakistan_h1 p {
            padding-right: 21px !important;
        }
        #thank .pakistan_hh2 p {
            padding-right: 21px !important;
        }
        #thank .pakistan_hh2 {
            margin-bottom: 80px !important;
        }
        #thank .pakistan1_h1 h2 {
            padding-left: 19px !important;
        }
        #thank .pakistan1_h1 p {
            padding-left: 19px !important;
        }
        #thank .pakistan2_h1 h2 {
            padding-left: 19px !important;
        }
        #thank .pakistan2_h1 p {
            padding-left: 19px !important;
        }
        #thank .rawalpindi_bx {
            width: 538px !important;
            min-height: 350px !important;
        }
        #thank .rawalpindi_sc {
            margin-bottom: 175px !important;
        }
        #thank .sialkot_Sc1 {
            margin-top: 246px !important;
        }
        #thank .sialkotcar_txt12 p {
            padding-right: 48px !important;
            padding-left: 34px !important;
        }
        #thank .bookbtn11 {
            margin-left: 35px !important;
        }
        #thank .vicale_ocations1 {
            margin-left: 48px !important;
        }
        #thank .rentcar_fais {
            margin-left: 222px !important;
        }
        #thank .bookvehciles12 {
            width: 516px !important;
        }
        #thank .rentcar247 {
            width: 396px !important;
        }
        #thank .karchibx1 {
            width: 444px !important;
        }
        #thank .karachi_txt {
            width: 712px !important;
        }
        #thank .lahore_bg {
            margin-bottom: -21px !important;
        }
        #thank .karachi_bg {
            margin-bottom: -23px !important;
        }
        #thank .faislabad_bg {
            margin-bottom: -23px !important;
        }
        #thank .peshware_bg {
            margin-bottom: -23px !important;
        }
        #thank .sargodha_bg {
            margin-bottom: -23px !important;
        }
        #thank .sialkotbg {
            margin-bottom: -23px !important;
        }
        #thank .rawlpindi_bg {
            margin-bottom: -34px !important;
        }
        #thank .dubai_bg2 {
            margin-bottom: -23px !important;
        }
        #thank .recentjob {
            width: 554px !important;
        }
        #thank .detailpage_bg {
            width: 490px !important;
        }
        #thank .dtllabel_l {
            margin-left: 98px !important;
        }
        #thank .dtlbtn_12 {
            margin-left: 150px !important;
        }
        #thank .vehiclerates {
            width: 352px !important;
        }
    }
    #thank .bullet_sec, #thank .logo, #thank .menu, #thank .menu > ul > li, #thank .searchform, #thank .slider_img, #thank .sliderbx, #thank .slidertxt1, #thank .slidertxt1 h1 {
        float: left;
    }
    @media screen and (-webkit-min-device-pixel-ratio: 0) {
        #thank .specialdis_sec {
            margin-top: 11px !important;
        }
    }
    #thank .col-lg-1, #thank .col-lg-10, #thank .col-lg-11, #thank .col-lg-12, #thank .col-lg-2, #thank .col-lg-3, #thank .col-lg-4, #thank .col-lg-5, #thank .col-lg-6, #thank .col-lg-7, #thank .col-lg-8, #thank .col-lg-9, #thank .col-md-1, #thank .col-md-10, #thank .col-md-11, #thank .col-md-12, #thank .col-md-2, #thank .col-md-3, #thank .col-md-4, #thank .col-md-5, #thank .col-md-6, #thank .col-md-7, #thank .col-md-8, #thank .col-md-9, #thank .col-sm-1, #thank .col-sm-10, #thank .col-sm-11, #thank .col-sm-12, #thank .col-sm-2, #thank .col-sm-3, #thank .col-sm-4, #thank .col-sm-5, #thank .col-sm-6, #thank .col-sm-7, #thank .col-sm-8, #thank .col-sm-9, #thank .col-xs-1, #thank .col-xs-10, #thank .col-xs-11, #thank .col-xs-12, #thank .col-xs-2, #thank .col-xs-3, #thank .col-xs-4, #thank .col-xs-5, #thank .col-xs-6, #thank .col-xs-7, #thank .col-xs-8, #thank .col-xs-9 {
        min-height: 1px;
        padding-left: 0;
        padding-right: 0;
        position: relative;
    }
    #thank ul {
        padding: 0;
        margin: 0;
    }
    #thank .bg_top {
        background-color: #325d84;
    }
    #thank .logo {
        margin-top: 12px;
    }
    #thank .callus {
        margin-top: 47px !important;
        margin-right: 56px;
    }
    #thank .callus a, #thank .callus p {
        font-family: tahoma;
        font-size: 20px;
        color: #4c4c4c;
    }
    #thank .banner_section {
        margin-top: 10px;
    }
    #thank .searchform {
        border-radius: 6px;
        box-shadow: 0 2px 2px rgba(0, 0, 0, .4);
        background-color: #f4d260;
    }
    #thank .form1 {
        float: left;
        margin: 20px;
    }
    #thank .searchform h3 {
        color: #325d84;
        font-size: 28px;
        padding-left: 18px;
        margin-bottom: -6px !important;
        font-family: MyriadProRegular;
    }
    #thank .label_one {
        margin-top: 8px;
    }
    #thank .bullet_sec {
        margin-top: 12px;
    }
    #thank .bullet_sec li {
        list-style: none;
        font-size: 14pxp;
        padding-left: 23px;
        color: #fff;
        font-family: Tahoma, Geneva, sans-serif;
    }
    #thank #bult {
        background: url(../images/blt.png) 0 166% no-repeat rgba(0, 0, 0, 0);
    }
    #thank .submit_btn1 {
        margin-top: 31px;
        margin-bottom: 23px;
    }
    #thank .submit_btn1 input {
        font-family: Tahoma, Geneva, sans-serif !important;
        font-size: 20px !important;
        border: none !important;
        color: #fff !important;
        height: 47px !important;
        background-color: #325d84 !important;
        width: 183px !important;
    }
    #thank .slidertxt1 a, #thank .slidertxt1 h1 {
        font-family: 'robotomedium';
    }
    #thank .sldier_bg {
        margin-top: 33px;
        margin-bottom: 60px;
    }
    #thank .slider_img img {
        height: 216px;
    }
    #thank .slidertxt1 h1 {
        color: #325d84;
        margin-bottom: 1px;
        padding-left: 7px;
    }
    #thank .slidertxt1 a {
        color: #325d84 !important;
    }
    #thank .slidertxt1 h1 {
        font-size: 18px !important;
        padding-left: 12px;
    }
    #thank .area_menu, #thank .car_img, #thank .car_section1, #thank .carbox, #thank .choseus, #thank .footer_bottom, #thank .footer_lft, #thank .footer_menu, #thank .footer_socialicons, #thank .imgtext, #thank .inner_page, #thank .map, #thank .welcmimg, #thank .welcome_sec, #thank .welcomeimg_text {
        float: left;
    }
    #thank .whychose_us h3 {
        font-size: 28px;
        padding-left: 12px;
        font-family: MyriadPro-Cond;
        margin-bottom: 0;
        margin-top: 6px;
        color: #fff;
    }
    #thank .whychose_us p {
        font-size: 17px;
        padding-left: 12px;
        margin-bottom: 2px;
        font-family: Tahoma, Geneva, sans-serif;
        color: #fff;
    }
    #thank #arrow2 {
        background: url(../images/arrow2.png) 1% 49% no-repeat rgba(0, 0, 0, 0);
    }
    #thank .choseus {
        margin-left: 12px;
    }
    #thank .choseus ul li {
        list-style: none;
        font-size: 21px;
        line-height: 25px;
        font-family: MyriadProRegular;
        color: #fff;
        padding-left: 22px;
    }
    #thank .area_menu ul li, #thank .footer_menu ul li {
        line-height: 60px;
        display: inline-block;
    }
    #thank .welcome_sec h1, #thank .welcome_sec h2 {
        font-family: MyriadPro-Cond;
    }
    #thank .welcome_sec {
        margin-top: 12px;
    }
    #thank .welcome_sec h2 {
        font-size: 27px !important;
        color: #325d84;
    }
    #thank .welcome_sec h2 {
        font-size: 23px;
        margin-top: 0;
        margin-bottom: 3px;
        color: #000;
    }
    #thank .welcome_sec p {
        font-size: 13px;
        text-align: justify;
        font-family: tahoma;
        color: #4c4c4c;
    }
    #thank .welcome_img {
        margin-top: 40px;
    }
    #thank .welcomeimg_text {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        background-color: #325d84;
        opacity: 0.8;
    }
    #thank .welcomeimg_text p {
        font-size: 15px;
        padding-top: 16px;
        padding-bottom: 16px;
        padding-left: 12px;
        font-family: tahoma;
        color: #fff;
    }
    #thank .welcomeimg_text a {
        font-size: 17px;
        font-family: tahoma;
        color: #fff;
    }
    #thank .footerarea_menu {
        background: #00000b;
        background: -moz-linear-gradient(-45deg, #00000b 0, #00000b 67%, #00000b 67%, #325d84 67%, #325d84 67%, #325d84 100%);
        background: -webkit-linear-gradient(-45deg, #00000b 0, #00000b 67%, #00000b 67%, #325d84 67%, #325d84 67%, #325d84 100%);
        background: linear-gradient(135deg, #00000b 0, #00000b 86.5%, #00000b 86.5%, #325d84 67%, #325d84 67%, #325d84 100%);
        margin-top: 23px;
    }
    #thank .area_menu ul li {
        list-style: outside;
    }
    #thank .area_menu a {
        font-family: MyriadPro-Cond;
        font-size: 21px;
        padding: 0 32px 0 0;
        color: #636262;
    }
    #thank .area_menu a:hover {
        color: #fff;
    }
    #thank .footer_calus {
        float: left;
        margin-top: 50px;
        background: url(../images/support.png) 1% 11% no-repeat rgba(0, 0, 0, 0);
    }
    #thank .footer_calus h3 {
        font-family: MyriadProRegular;
        color: #000;
        margin-top: -23px !important;
        padding-left: 12px;
        font-size: 15px;
    }
    #thank .footer_calus p {
        font-size: 13px;
        text-align: justify;
        font-family: tahoma;
        color: #fc7716;
        margin-top: 10px;
        padding-left: 56px;
    }
    #thank .footer_socialicons ul li {
        list-style: none;
        display: inline-block;
    }
    #thank .footer_socialicons ul li img {
        padding: 3px;
        opacity: 0.8;
    }
    #thank .footermenu_bg {
        background-color: #000;
    }
    #thank .footer_menu ul li {
        list-style: outside;
    }
    #thank .footer_menu a {
        font-family: MyriadPro-Cond;
        font-size: 21px;
        padding: 0 20px 0 0;
        color: #636262;
    }
    #thank .booknowbtn, #thank .img_btn {
        padding: 6px;
        text-align: center;
    }
    #thank .footer_menu a:hover {
        color: #fff;
    }
    #thank .copyright {
        margin-top: 19px;
    }
    #thank .copyright p {
        font-family: tahoma;
        font-size: 16px;
        color: #4c4c4c;
    }
    #thank .inner_bg {
        background-color: #f3f3f3;
    }
    #thank .inner_page p {
        color: #4c4c4c;
        font-family: tahoma;
        font-size: 13px;
        text-align: justify;
    }
    #thank .carbox {
        margin-bottom: 27px;
        margin-right: 34px;
        background-color: #fff;
        border: 1px solid pink;
    }
    #thank .img_btn {
        background-color: #325d84;
        float: right;
    }
    #thank .img_btn a {
        color: #fff !important;
        font-family: tahoma !important;
        font-size: 16px !important;
    }
    #thank .imgtext a, #thank .imgtext h2 {
        color: #325d84;
        font-family: MyriadPro-Cond;
        font-size: 34px;
        text-align: center;
    }
    #thank .vehicle_section {
        float: left;
        background-color: #fff;
        margin-top: 12pxp;
        margin-bottom: 25px;
    }
    #thank .vehicle_img {
        float: left;
        margin-left: 12px;
    }
    #thank .vehichle_det {
        float: left;
        margin-left: 9px;
        margin-bottom: 20px;
    }
    #thank .vehichle_det ul li {
        list-style: none;
        font-family: tahoma;
        color: #4c4c4c;
        line-height: 31px;
        border-bottom: 1px solid #ccc;
        font-size: 19px;
    }
    #thank .vehichle_det h2 {
        color: #325d84;
        border-bottom: 4px solid #000;
        font-family: utsaahregular;
        font-size: 26px !important;
        margin-top: 0;
        margin-bottom: 0;
    }
    #thank .booknowbtn {
        background-color: #1e2b37;
        border-radius: 2px;
        float: right;
        margin: 2px 22px 10px;
    }
    #thank .address_p, #thank .contactform {
        margin-left: 25px;
        float: left;
    }
    #thank .booknowbtn a {
        color: #fff;
        font-family: tahoma;
        font-size: 15px;
    }
    #thank .booknowbtn a:hover {
        color: #f4d260;
    }
    #thank .vehiclerates {
        float: left;
        margin-bottom: 0;
        margin-left: 9px;
    }
    #thank .vehiclerates h1 {
        color: #325d84;
        border-bottom: 4px solid #000;
        font-family: utsaahregular;
        font-size: 26px !important;
        margin-top: 0;
        margin-bottom: 0;
    }
    #thank .about-us {
        float: left;
        margin-bottom: 25px;
    }
    #thank .contactform {
        margin-top: 11px;
    }
    #thank .contactform input {
        border: 1px solid #949494;
        color: #605e5e;
        border-radius: 3px;
        margin-bottom: 15px;
        padding: 3px;
    }
    #thank .contactform textarea {
        border: 1px solid #949494;
        color: #605e5e;
        border-radius: 3px;
        margin-bottom: 15px;
        padding-left: 6px;
        padding-top: 7px;
    }
    #thank .contact_btn1 {
        margin-bottom: 23px;
        float: left;
    }
    #thank .contact_btn1 input {
        background-color: #325d84;
        border: none;
        border-radius: 4px;
        color: #fff;
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 20px;
        height: 43px;
        padding-top: 0;
        width: 138px !important;
    }
    #thank .address_p h1 {
        color: #325d84;
        font-family: MyriadPro-Cond;
        font-size: 28px;
        margin-bottom: 0;
    }
    #thank .address_p {
        margin-top: 5px;
    }
    #thank .about-us p {
        color: #4c4c4c;
        font-family: tahoma;
        padding-right: 15px;
        padding-top: 15px;
        font-size: 13px;
        text-align: justify;
    }
    #thank .booking_form {
        float: left;
    }
    #thank .booking_form input {
        border: 1px solid #949494;
        color: #605e5e;
        border-radius: 3px;
        margin-bottom: 15px;
        padding: 3px;
    }
    #thank .label1, #thank .label2 {
        float: left;
    }
    #thank .label1 select {
        width: 263px !important;
    }
    #thank .quoteme_now1 {
        float: left;
        margin-top: 21px;
        margin-bottom: 12px;
    }
    #thank .quoteme_now1 input {
        background-color: #1e2b37;
        border: none;
        border-radius: 4px;
        color: #fff;
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 20px;
        height: 47px;
        opacity: 0.77;
        padding-top: 0;
        width: 176px !important;
    }
    #thank .clear {
        clear: both;
    }
    #thank .noticeboard {
        border: 1px solid #d5d5d5;
        border-radius: 4px;
        box-shadow: 16px 18px 27px -15px #2e3349;
        margin-bottom: 25px;
        margin-top: 32px;
        min-height: 450px;
    }
    #thank .notice_p h1 {
        color: #666;
        font-size: 28px;
    }
    #thank .notice_p p {
        font-size: 15px;
        float: left;
    }
    #thank #space {
        padding-left: 55px;
        color: #666;
        float: right;
    }
    #thank .contactbtn12 input, #thank .contactbtn123 input {
        color: #fff;
        font-size: 14px;
        cursor: pointer;
    }
    #thank .contactbtn12, #thank .contactbtn123 {
        width: 100%;
        margin-top: 0;
        float: left;
    }
    #thank .contactbtn12 {
        margin-bottom: 20px;
        margin-left: 167px;
    }
    #thank .contactbtn12 input {
        border: 1px solid #626262;
        border-radius: 3px;
    }
    #thank .contactbtn123 {
        margin-bottom: 2px;
    }
    #thank .contactbtn123 input {
        border: 1px solid #626262;
        border-radius: 3px;
    }
    #thank .notice_p1 {
        float: left;
        margin-top: 31px;
        margin-left: 40px;
    }
    #thank .notice_p1 p {
        color: #666;
        font-size: 15px;
    }
    #thank #request2 {
        float: left;
    }
    #thank .requestquote_btn1 {
        float: left;
        background: #870d0f !important;
        border: 1px solid #870d0f !important;
        border-radius: 6px;
        padding: 10px 16px;
        margin-top: 20px;
        width: 320px;
    }
    #thank .requestquote_btn1 a {
        color: #fff;
        font-weight: 700;
        font-size: 16px;
    }
    #thank .center1 {
        float: left;
        margin-bottom: 12px;
    }
    #thank .specialdis_sec {
        float: left;
        margin-top: 2px;
    }
    #thank .specialdis_sec > h3 {
        color: #15804d;
        margin-top: 0;
        padding-left: 27px;
        font-family: MyriadPro-Cond;
        font-size: 30px;
    }
    #thank .special_discount {
        float: left;
        margin-left: 27px;
    }
    #thank .special_discount ul li {
        list-style: none;
        display: inline-block;
    }
    #thank .special_discount img {
        border: 1px solid #cbcbcb !important;
        border-radius: 8px;
        margin: 2px;
        padding: 3px;
    }
    #thank .car_img img {
        min-height: 259px;
    }
    #thank .footer_calus a {
        font-size: 23px;
    }
    #thank .address_p a {
        font-size: 21px;
        padding-left: 3px;
        color: #4c4c4c !important;
    }
    #thank .help_fq {
        float: left;
        margin-top: 12px;
    }
    #thank .myButton {
        -moz-box-shadow: 0 10px 14px -7px #276873;
        -webkit-box-shadow: 0 10px 14px -7px #276873;
        box-shadow: 0 10px 14px -7px #276873;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
        background: -moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
        background: -webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
        background: -o-linear-gradient(top, #599bb3 5%, #408c99 100%);
        background: -ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
        background: linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
        background-color: #599bb3;
        -moz-border-radius: 8px;
        -webkit-border-radius: 8px;
        border-radius: 8px;
        display: inline-block;
        cursor: pointer;
        color: #fff;
        font-family: MyriadPro-Cond;
        font-size: 25px;
        padding: 6px 77px;
        text-shadow: 0 1px 0 #3d768a;
    }
    #thank .about-us h2, #thank .about-us h3 {
        color: #000;
        font-family: MyriadPro-Cond;
        margin-bottom: 3px;
    }
    #thank .myButton:hover {
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
        background: -moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
        background: -webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
        background: -o-linear-gradient(top, #408c99 5%, #599bb3 100%);
        background: -ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
        background: linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
        background-color: #408c99;
    }
    #thank .myButton:active {
        position: relative;
        top: 1px;
    }
    #thank .about-us a {
        font-size: 22px;
    }
    #thank .about-us h2 {
        font-size: 23px;
    }
    #thank .about-us h3 {
        font-size: 21px;
    }
    #thank .banner_text {
        float: left;
        margin-top: -365px;
    }
    #thank .banner_text h3 {
        color: #4c4c4c;
        font-family: myriadpro-cond;
        font-size: 39px;
    }
    #thank .carousel-caption {
        float: left;
    }
    #thank .form1 input {
        border: 1px solid #ccc;
        padding: 6px 12px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        height: 34px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset;
        border-radius: 4px;
        width: 211px;
        font-size: 16px;
        color: #555;
    }
    #thank .welcome_sec a {
        font-size: 21px;
        color: #000 !important;
    }
    #thank .testimonial_bx p {
        color: #605e5e;
        font-family: tahoma;
        font-size: 14px;
        line-height: 23px;
        text-align: justify;
        padding: 12px 13px 7px;
    }
    #thank .testimonial_bx h2 {
        color: #605e5e;
        padding-left: 7.5%;
        bottom: -40px;
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 18px;
        position: absolute;
    }
    #thank .testimonial_bx {
        background-color: #ccc;
        border-radius: 3px;
        float: left;
        margin-bottom: 75px;
    }
    #thank .border_curve {
        border-color: #ccc transparent;
        border-style: solid;
        border-width: 30px 30px 0 0;
        bottom: -25px;
        display: block;
        position: absolute;
        left: 5%;
        width: 0;
    }
    #thank #nav li ul, #thank #nav > a {
        display: none;
    }
    #thank #nav li {
        position: relative;
    }
    #thank #nav > ul {
        height: 3.75em;
    }
    #thank #nav > ul > li {
        width: 25%;
        height: 100%;
        float: left;
    }
    #thank #nav li ul {
        position: absolute;
        top: 100%;
    }
    #thank #nav li:hover ul {
        display: block;
    }
    @media only screen and (max-width: 40em) {
        #thank #nav {
            position: relative;
        }
        #thank #nav:not(:target) > a:first-of-type, #thank #nav:target > a:last-of-type, #thank #nav:target > ul {
            display: block;
        }
        #thank #nav > ul {
            height: auto;
            display: none;
            position: absolute;
            left: 0;
            right: 0;
        }
        #thank #nav > ul > li {
            width: 100%;
            float: none;
        }
        #thank #nav li ul {
            position: static;
        }
    }
    #thank .label1 > label, #thank .label2 > label, #thank .label_one.col-lg-6 > label {
        color: #333 !important;
    }
    #thank .table-responsive {
        color: #000 !important;
    }
    #thank .imgtext a:hover {
        color: #4c4c4c !important;
        text-decoration: none !important;
    }
    #thank .img_btn.col-lg-11.col-xs-12 > a:hover {
        color: #fff !important;
    }
    #thank .main_bg {
        float: left;
        width: 100%;
    }
    #thank .dubai_bg {
        background: rgba(0, 0, 0, 0) url(../images/dubai-bg.jpg) no-repeat;
    }
    #thank .dubai_sec {
        float: left;
        margin-bottom: 54px;
        margin-top: 1px;
    }
    #thank .dubai_txt {
        float: left;
    }
    #thank .dubai_txt h1 {
        font-size: 22px !important;
        font-weight: 700;
        color: white;
        font-family: 'MyriadProRegular';
    }
    #thank .dubai_txt h2 {
        font-size: 22px;
        font-weight: 700;
        color: white;
        font-family: 'MyriadProRegular';
    }
    #thank .dubai_txt p {
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 13px;
        color: white;
    }
    #thank .dubai_bx {
        float: left;
        margin-top: 47px;
        min-height: 298px;
        opacity: 0.8;
        background-color: #631614;
    }
    #thank .box1_tx h2 {
        font-size: 14px;
        color: white;
        font-weight: 700;
        padding-left: 16px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .box1_tx p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        padding-left: 16px;
        padding-right: 16px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .box1_tx {
        float: left;
        opacity: 1;
    }
    #thank .dubai_bg2 {
        background: rgba(0, 0, 0, 0) url(../images/dubai-bg1.jpg) no-repeat;
        float: left;
        width: 100%;
    }
    #thank .dubai1_bx1 {
        float: left;
        min-height: 462px;
        opacity: 0.9;
        background: #fff;
        background: -moz-linear-gradient(top, #fff 0%, #fff 16%, #c2c0c5 74%, #c2c0c5 100%);
        background: -webkit-linear-gradient(top, #fff 0%, #fff 16%, #c2c0c5 74%, #c2c0c5 100%);
        background: linear-gradient(to bottom, #fff 0%, #fff 16%, #c2c0c5 74%, #c2c0c5 100%);
    }
    #thank .duai1_tx1 h2 {
        font-size: 14px !important;
        color: #121212;
        font-weight: 700;
        text-align: center;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .duai1_tx1 p {
        font-size: 12px;
        color: #121212;
        line-height: 19px;
        text-align: justify;
        padding-left: 16px;
        padding-right: 16px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .duabi_Sec2 {
        float: left;
        margin-bottom: 58px;
        margin-top: 15px;
    }
    #thank .dubai_cr img {
        margin: 0 auto;
        height: 169px;
    }
    #thank .dubai_cr {
        margin-top: 85px;
    }
    #thank .pakistanimg_bg {
        background: rgba(0, 0, 0, 0) url(../images/rentcarpeshwarebg.jpg) no-repeat fixed 0 0 / cover;
    }
    #thank .pakistancar_sec {
        float: left;
    }
    #thank .pakistancar_lft {
        float: left;
        margin-top: 12px;
    }
    #thank .pakistan_h1 {
        float: left;
        margin-bottom: 50px;
    }
    #thank .pakistan_h1 h1 {
        font-size: 14px !important;
        color: white;
        padding-top: 13px;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistan_h1 h2 {
        font-size: 14px;
        color: white;
        padding-top: 13px;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistan_h1 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistan_hh2 {
        float: left;
    }
    #thank .pakistan_hh2 h2 {
        font-size: 14px;
        color: white;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistan_hh2 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistancar_right {
        float: left;
    }
    #thank .pakistan1_h1 h2 {
        font-size: 20px;
        color: white;
        font-weight: 700;
        padding-top: 21px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistan1_h1 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistan2_h1 {
        float: left;
        margin-top: 40px;
    }
    #thank .pakistan2_h1 h2 {
        font-size: 20px;
        color: white;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .pakistan2_h1 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .trm_bg {
        background-color: #f6f6f6;
    }
    #thank .term_h1 {
        float: left;
        margin-bottom: 23px;
    }
    #thank .term_h1 h2 {
        font-size: 22px;
        color: #010101;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .term_h1 p {
        font-size: 12px;
        color: #010101;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rawlpindi_txt {
        float: left;
        margin-top: 35px;
    }
    #thank .rawlpindi_txt h1 {
        font-size: 22px !important;
        color: #010101;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rawlpindi_txt h2 {
        font-size: 22px;
        color: #010101;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rawlpindi_txt p {
        font-size: 12px;
        color: #010101;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rawalpindi_sc {
        float: left;
        margin-top: 35px;
    }
    #thank .rawalpindi_bx {
        float: left;
        margin-right: 74px;
        opacity: 0.6;
        margin-bottom: 14px;
        background-color: #00000b;
    }
    #thank .rawalpindi_bx h2 {
        font-size: 22px !important;
        color: white;
        padding-left: 28px;
        padding-right: 28px;
        padding-top: 25px;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rawalpindi_bx p {
        font-size: 12px;
        color: white;
        padding-left: 28px;
        padding-right: 28px;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sialkot_sc {
        float: left;
    }
    #thank .sialkotcar_txt {
        float: right;
    }
    #thank .sialkotcar_txt h1 {
        font-size: 26px !important;
        color: white;
        padding-left: 18px;
        padding-right: 28px;
        padding-top: 25px;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sialkotcar_txt h2 {
        font-size: 26px;
        color: white;
        padding-left: 18px;
        padding-right: 28px;
        padding-top: 25px;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sialkotcar_txt p {
        font-size: 12px;
        color: white;
        padding-left: 18px;
        padding-right: 28px;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sialkotbg {
        background: rgba(0, 0, 0, 0) url(../images/Sialkot.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .rawlpindi_bg {
        background: rgba(0, 0, 0, 0) url(../images/Rawalpindi.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .sialkot_Sc1 {
        float: left;
        margin-bottom: 70px;
    }
    #thank .sialkotcar_txt1 {
        float: left;
    }
    #thank .sialkotcar_txt1 h2 {
        font-size: 25px;
        color: #333;
        padding-right: 28px;
        padding-top: 25px;
        font-weight: 700;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sialkotcar_txt1 p {
        font-size: 12px;
        color: #333;
        padding-right: 48px;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sialkotcar_txt12 {
        float: left;
        margin-top: 50px;
    }
    #thank .sialkotcar_txt12 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .bookbtn11 {
        float: left;
        text-align: center;
        padding-bottom: 7px;
        padding-top: 4px;
        margin-top: 20px;
        background-color: #b01f1e;
    }
    #thank .bookbtn11 a {
        font-size: 34px;
        color: white;
        text-decoration: none;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sargodha_bg {
        background: rgba(0, 0, 0, 0) url(../images/Sargodha.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .sargodha_sc {
        float: left;
    }
    #thank .sargodha_txt {
        float: left;
    }
    #thank .sargodha_txt h1 {
        font-size: 22px !important;
        color: #000;
        padding-right: 28px;
        padding-top: 25px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sargodha_txt h2 {
        font-size: 22px;
        color: #000;
        padding-right: 28px;
        padding-top: 25px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sargodha_txt p {
        font-size: 12px;
        color: #000;
        line-height: 19px;
        padding-top: 7px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .sargodha_sc1 {
        float: left;
        margin-bottom: 77px;
        margin-top: 214px;
    }
    #thank .vicale_ocations {
        float: left;
    }
    #thank .vicale_ocations h2 {
        font-size: 20px;
        color: white;
        padding-right: 28px;
        padding-top: 25px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .vicale_ocations p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        padding-top: 7px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .borderlft3 {
        border: 2px solid white;
        width: 1px;
        height: 235px;
        margin-left: 53px;
        margin-top: 61px;
        float: left;
    }
    #thank .vicale_ocations1 {
        float: left;
    }
    #thank .vicale_ocations1 h2 {
        font-size: 20px;
        color: white;
        padding-right: 28px;
        padding-top: 25px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .vicale_ocations1 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        padding-top: 7px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .peshware_bg {
        background: rgba(0, 0, 0, 0) url(../images/rentcarpeshwarebg.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .pesh_sc {
        float: left;
        background-color: rgba(30, 43, 55, .8);
        margin-top: 120px;
    }
    #thank .renta_Txt {
        float: left;
        margin-left: 90px;
    }
    #thank .border_p {
        border: 2px solid white;
        width: 1px;
        height: 124px;
        margin-left: 52px;
        margin-top: 27px;
        margin-bottom: 24px;
        float: left;
    }
    #thank .renta_Txt h2 {
        font-size: 23px;
        color: white;
        padding-top: 25px;
        font-family: 'robotoregular';
    }
    #thank .renta_Txt h1 {
        font-size: 40px !important;
        font-weight: 700;
        color: white;
        padding-top: 25px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .renta_p {
        float: left;
        margin-bottom: 16px;
        margin-left: 39px;
        margin-top: 32px;
    }
    #thank .renta_p p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        padding-top: 7px;
        text-align: justify;
        font-family: 'robotoregular';
    }
    #thank .peshware_port {
        float: left;
    }
    #thank .psh_sc2 {
        float: left;
        margin-top: 55px;
        margin-bottom: 90px;
        background-color: white;
    }
    #thank .peshware_port h2 {
        font-size: 23px;
        color: #575757;
        padding-left: 36px;
        padding-right: 36px;
        padding-top: 25px;
        font-family: 'robotoregular';
    }
    #thank .peshware_port p {
        font-size: 12px;
        color: #575757;
        padding-left: 36px;
        padding-right: 36px;
        line-height: 19px;
        text-align: justify;
        font-family: 'robotoregular';
    }
    #thank .peshware_Trv h2 {
        font-size: 23px;
        color: white;
        padding-left: 36px;
        padding-right: 36px;
        padding-top: 25px;
        font-family: 'robotoregular';
    }
    #thank .peshware_Trv p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        padding-top: 7px;
        padding-left: 36px;
        padding-right: 36px;
        text-align: justify;
        font-family: 'robotoregular';
    }
    #thank .peshware_Trv {
        background-color: #1e2b37;
        min-height: 360px !important;
    }
    #thank .faislabad_bg {
        background: rgba(0, 0, 0, 0) url(../images/Faisalabad.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .faislabad_sc {
        float: left;
        margin-top: 40px;
    }
    #thank .rentcar_fais {
        float: left;
    }
    #thank .rentcar_fais h1 {
        font-size: 32px !important;
        color: #292b23;
        font-weight: 700;
        padding-left: 74px;
        line-height: 36px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rentcar_fais h2 {
        font-size: 32px;
        color: #292b23;
        font-weight: 700;
        padding-left: 74px;
        line-height: 36px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rentcar_fais p {
        font-size: 13px;
        color: #292b23;
        line-height: 19px;
        padding-top: 7px;
        padding-left: 74px;
        padding-right: 36px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .bookvechle_sc {
        float: left;
        margin-top: 217px;
    }
    #thank .bookvechle_hd {
        float: right;
        margin-bottom: 284px;
    }
    #thank .bookvechle_hd h2 {
        font-size: 26px;
        color: white;
        font-weight: 700;
        padding-left: 24px;
        line-height: 36px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .bookvechle_hd p {
        font-size: 13px;
        color: white;
        line-height: 19px;
        padding-top: 7px;
        padding-left: 24px;
        padding-right: 36px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .karachi_sc {
        float: left;
    }
    #thank .karachi_txt {
        float: left;
    }
    #thank .karachi_txt h1 {
        font-size: 20px !important;
        color: #060606;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .karachi_txt h2 {
        font-size: 20px;
        color: #060606;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .karachi_txt p {
        font-size: 13px;
        color: #060606;
        line-height: 19px;
        padding-top: 7px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .karachi_bg {
        background: rgba(0, 0, 0, 0) url(../images/karachi.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .karachi_img {
        float: right;
    }
    #thank .karachi_sc2 {
        float: left;
    }
    #thank .music_rent {
        float: left;
    }
    #thank .music_rent h2 {
        font-size: 16px;
        color: #060606;
        font-weight: 700;
        margin-top: 0;
        padding-top: 8px;
        margin-bottom: 4px;
        padding-left: 12px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .music_rent p {
        font-size: 12px;
        color: #060606;
        line-height: 19px;
        text-align: justify;
        padding-left: 12px;
        padding-right: 12px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .musciimg {
        float: right;
    }
    #thank .karchibx1 {
        float: left;
    }
    #thank .karschbg1 {
        background-color: #f7f7f7;
        float: left;
        width: 100%;
    }
    #thank .bookvehciles12 {
        float: left;
        background-color: #2e375f;
    }
    #thank .rentcar247 {
        float: left;
    }
    #thank .rentcar247 h2 {
        font-size: 16px;
        color: white;
        font-weight: 700;
        margin-top: 0;
        padding-left: 12px;
        padding-top: 8px;
        margin-bottom: 4px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .rentcar247 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        padding-left: 12px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .hirekar {
        float: left;
        width: 333px;
    }
    #thank .hirekar h2 {
        font-size: 16px;
        color: #060606;
        font-weight: 700;
        margin-top: 0;
        padding-left: 12px;
        padding-top: 8px;
        margin-bottom: 4px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .hirekar p {
        font-size: 12px;
        color: #060606;
        line-height: 19px;
        text-align: justify;
        padding-left: 12px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .lahore_bg {
        background: rgba(0, 0, 0, 0) url(../images/Minar-e-Pakistan.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .lahore_sc {
        float: left;
        margin-top: 12px;
    }
    #thank .lahorelft1 {
        float: left;
        margin-top: 22px;
    }
    #thank .lahorelft1 h1 {
        font-size: 16px !important;
        color: #060606;
        font-weight: 700;
        margin-top: 0;
        line-height: 36px;
        margin-bottom: -6px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .lahorelft1 h2 {
        font-size: 16px;
        color: #060606;
        font-weight: 700;
        margin-top: 0;
        line-height: 36px;
        margin-bottom: -6px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .lahorelft1 p {
        font-size: 12px;
        color: #060606;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .lahoreright1 {
        float: right;
        margin-bottom: 12px;
    }
    #thank .lahorebximg1 {
        float: left;
        background-color: #325d84;
    }
    #thank .lahorebx {
        float: left;
    }
    #thank .lahorebx h2 {
        font-size: 16px;
        color: white;
        font-weight: 700;
        margin-top: 10px;
        line-height: 36px;
        margin-bottom: -1px;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .lahorebx p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .carsrvce_img {
        float: right;
    }
    #thank .lahorebximg2 {
        float: left;
        background-color: white;
    }
    #thank .lahorebx2 {
        float: left;
    }
    #thank .lahorebx2 h2 {
        font-size: 16px;
        color: #2e375f;
        font-weight: 700;
        margin-top: 10px;
        line-height: 36px;
        margin-bottom: -1px;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .lahorebx2 p {
        font-size: 12px;
        color: #818181;
        line-height: 19px;
        text-align: justify;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .carsrvce_img2 {
        float: left;
        margin-top: 6px;
        margin-bottom: 12px;
    }
    #thank .islamabad_bg {
        background: rgba(0, 0, 0, 0) url(../images/Islamabad.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .mirpur_bg {
        background: rgba(0, 0, 0, 0) url(../images/Mirpur.jpg) no-repeat fixed 0 0 / cover;
        float: left;
        width: 100%;
    }
    #thank .islamabad_sc {
        float: left;
        margin-top: 12px;
    }
    #thank .isbbximg2 {
        float: left;
        background-color: white;
    }
    #thank .islamabdbx2 {
        float: left;
    }
    #thank .islamabdbx2 h2 {
        font-size: 16px;
        color: #2e375f;
        font-weight: 700;
        margin-top: 10px;
        line-height: 36px;
        margin-bottom: -1px;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .islamabdbx2 p {
        font-size: 12px;
        color: #818181;
        line-height: 19px;
        text-align: justify;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .carsrvce_img3 {
        float: left;
        margin-bottom: 12px;
    }
    #thank .isbbximg4 {
        float: left;
        background-color: #325d84;
    }
    #thank .isbbx4 {
        float: left;
    }
    #thank .isbbx4 h2 {
        font-size: 15px;
        color: white;
        font-weight: 700;
        margin-top: 10px;
        line-height: 36px;
        margin-bottom: -1px;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .isbbx4 p {
        font-size: 12px;
        color: white;
        line-height: 19px;
        text-align: justify;
        padding-left: 15px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .isbright1 {
        float: left;
    }
    #thank .isblft1 {
        float: right;
    }
    #thank .isblft1 h1 {
        font-size: 26px !important;
        color: #060606;
        font-weight: 700;
        margin-top: 0;
        line-height: 36px;
        margin-bottom: -6px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .isblft1 h2 {
        font-size: 16px;
        color: #060606;
        font-weight: 700;
        margin-top: 0;
        line-height: 36px;
        margin-bottom: -6px;
        padding-right: 36px;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .isblft1 p {
        font-size: 12px;
        color: #060606;
        line-height: 19px;
        text-align: justify;
        font-family: sans-serif, Geneva, sans-serif;
    }
    #thank .readmorebtn1 {
        float: left;
        width: 210px;
        height: 37px;
        margin-top: 12px;
        padding-top: 3px;
        text-align: center;
        background: rgba(0, 0, 0, 0) url(../images/readmorebtn1.jpg) no-repeat;
    }
    #thank .readmorebtn1 a {
        font-size: 22px;
        color: white;
        text-decoration: none;
    }
    #thank .carsrvce_img5 {
        float: right;
        margin-bottom: 12px;
        margin-top: 6px;
    }
    #thank .cars12img {
        float: right;
    }
    #thank .recentjob {
        float: left;
        margin: 8px !important;
        background-color: white;
        box-shadow: 0 1px 1px rgba(0, 0, 0, .15), -1px 0 0 rgba(0, 0, 0, .03), 1px 0 0 rgba(0, 0, 0, .03), 0 1px 0 rgba(0, 0, 0, .12);
    }
    #thank .recent_txt1 {
        float: left;
    }
    #thank .recent_txt1 h1 {
        font-size: 28px;
        color: black;
        margin-bottom: 0;
        padding-left: 12px;
    }
    #thank .recent_txt1 p {
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 13px;
        color: #4c4c4c;
        padding-left: 12px;
    }
    #thank .recentbtn {
        float: right;
        background-color: #325d84;
        border-top-left-radius: 18px;
        text-align: center;
        padding-bottom: 8px;
        padding-top: 4px;
    }
    #thank .recentbtn a {
        font-size: 20px;
        color: white;
        text-decoration: none;
        font-family: tahoma;
    }
    #thank .recentbtn a:hover {
        color: black;
        transition: color 800ms ease 0s !important;
    }
    #thank .detailpage_bg {
        float: right;
        margin: 8px !important;
        border-radius: 8px;
        background-color: white;
        box-shadow: 0 1px 1px rgba(0, 0, 0, .15), -1px 0 0 rgba(0, 0, 0, .03), 1px 0 0 rgba(0, 0, 0, .03), 0 1px 0 rgba(0, 0, 0, .12);
    }
    #thank .detailpage_frm {
        float: left;
        margin-top: 60px;
    }
    #thank .dtllabel_l {
        margin-bottom: 10px;
        float: left;
    }
    #thank .detailpage_frm input {
        border: 1px solid #949494;
        color: #605e5e;
        padding-left: 5px;
        border-radius: 3px;
        width: 300px;
    }
    #thank .dtllabel_l2 input {
        border: medium none;
    }
    #thank .dtlbtn_12 {
        float: left;
        text-align: center;
        padding-bottom: 8px;
        padding-top: 4px;
        border-radius: 4px;
        margin-top: 22px;
        margin-bottom: 55px;
    }
    #thank .dtlbtn_12 input {
        font-size: 20px;
        color: white;
        width: 222px !important;
        background-color: #325d84;
        border: 1px solid #325d84;
        padding: 3px;
        text-decoration: none;
        font-family: tahoma;
    }
    #thank .callus a:hover {
        color: #325d84 !important;
    }
    #thank .footer_calus a {
        color: #000;
    }
    #thank .footer_calus a:hover {
        color: #000;
    }
    #thank .welcome_sec h3 {
        font-size: 22px;
        margin-top: 0;
        margin-bottom: 3px;
        color: #000;
        font-family: MyriadPro-Cond;
    }
    #thank .welcome_sec h4 {
        font-size: 23px;
        margin-top: 0;
        margin-bottom: 3px;
        color: #000;
        font-family: MyriadPro-Cond;
    }
    #thank .footerarea_menu {
        float: left;
    }
    @media (max-width: 767px) {
        #thank .inner_pages1 {
            margin-top: 50px;
            left: 0px !important;
            margin-bottom: 50px;
            background-color: white;
            box-shadow: 0 42px 15px -38px #c5c4c3;
        }
    }
    @media (min-width: 499px) {
        #thank .inner_pages1 {
            margin-top: 50px;
            left: 0px !important;
            margin-bottom: 50px;
            background-color: white;
            box-shadow: 0 42px 15px -38px #c5c4c3;
        }
    }
    #thank .menu {
        z-index: 9999999;
    }
    #thank .searchform {
        z-index: 999999;
    }
    #thank .bottom_sec {
        float: left;
        margin-top: 45px;
        margin-bottom: 22px;
    }
    #thank .botm_bx h3 {
        color: #f4990a;
        font-family: 'robotoregular';
        font-size: 18px;
        text-align: center;
    }
    #thank .botm_bx p {
        color: white;
        font-family: "robotoregular";
        font-size: 14px;
        line-height: 18px;
        padding-left: 22px;
        padding-right: 22px;
        text-align: justify;
    }
    #thank .fa-plane-f::before, #thank .fa-plane::before {
        font-size: 41px;
        text-align: center;
        color: #f4990a;
    }
    #thank .fa-thumbs-up-f::before, #thank .fa-thumbs-up::before {
        font-size: 41px;
        text-align: center;
        color: #f4990a;
    }
    #thank .fa-heart-f::before, #thank .fa-heart::before {
        font-size: 41px;
        text-align: center;
        color: #f4990a;
    }
    #thank .footer_bg {
        background-color: #1f2b37;
        float: left;
        width: 100%;
    }
    #thank .footer_center {
        float: left;
        margin-bottom: 33px;
        margin-top: 16px;
    }
    #thank .footer1 {
        float: left;
    }
    #thank .footer1 h3 {
        font-family: 'ralewaysemibold';
        font-size: 16px;
        color: white;
        text-transform: uppercase;
    }
    #thank .footer1 p {
        font-family: 'ralewaylight';
        font-size: 14px;
        color: white;
        line-height: 22px;
        margin-bottom: 2px !important;
    }
    #thank .social_icon5 {
        float: left;
        margin-top: 8px;
    }
    #thank .social_icon5 ul li {
        list-style: none;
        display: inline-block;
    }
    #thank .social_icon5 img {
        margin: 0 auto;
        padding: 3px;
        text-align: center;
    }
    #thank .faq_btn {
        width: 32%;
        padding: 6px;
        background-color: #4c94a6;
    }
    #thank .faq_btn a {
        font-size: 16px;
        font-family: 'ralewayregular';
        text-align: center;
        color: white;
    }
    #thank .dd {
        float: left;
        margin-top: 8px;
    }
    #thank .footer2 {
        float: left;
    }
    #thank .footer2 h3 {
        font-family: 'ralewaysemibold';
        font-size: 16px;
        color: white;
        text-transform: uppercase;
    }
    #thank .footer2 ul li {
        list-style: none;
    }
    #thank .footer2 ul li a {
        font-family: 'ralewaylight';
        font-size: 14px;
        color: white;
        line-height: 18px;
    }
    #thank .menu2 {
        float: left;
    }
    #thank .menu2 ul li {
        list-style: none;
    }
    #thank .menu2 ul li a {
        font-family: 'ralewaylight';
        font-size: 14px;
        color: white;
        text-align: center;
        line-height: 18px;
    }
    #thank .footer3 {
        float: left;
        margin-left: 14px;
        margin-top: 19px;
    }
    #thank .footer4 {
        float: left;
        margin-top: 19px;
    }
    #thank .mobile_responsive {
        margin-bottom: 12px;
    }
    #thank .footer1 a {
        font-size: 14px;
        font-family: "ralewaylight";
        color: white !important;
    }
    #thank .call_btn a {
        color: white;
        font-size: 21px;
        font-family: 'oswaldmedium';
    }
    #thank .mobile_Sec {
        float: left;
    }
    #thank .mobile_responsive {
        float: left;
    }
    #thank .call_btn {
        background-color: #1e2b37;
        border-radius: 4px;
        margin: 0 auto;
        padding-bottom: 8px;
        padding-top: 9px;
        text-align: center;
        width: 68%;
    }
    #thank .slide_txt3 {
        margin: 0 auto;
        padding-top: 16px;
        text-align: center;
    }
    #thank .slide_txt3 p {
        color: white;
        font-size: 27px;
        line-height: 31px;
        text-align: center;
        font-family: 'oswaldmedium';
    }
    #thank .slide_txt3bg {
        background: rgba(18, 17, 17, .5) none repeat scroll 0 0;
        height: 165px;
        margin: 0 auto;
        text-align: center;
        width: 92%;
    }
    #thank .responsive-tt {
        margin: 42px auto 0;
        position: absolute !important;
    }
    #thank .mirpur_sec {
        float: left;
        margin-bottom: 30px;
        margin-top: 18px;
    }
    #thank .mirpurt_cont {
        float: left;
    }
    #thank .mirpurt_cont h2 {
        font-size: 18px;
        color: #4c4c4c;
        padding-left: 14px;
        font-family: 'ralewaymedium';
    }
    #thank .mirpurt_cont p {
        font-size: 14px;
        color: #4c4c4c;
        line-height: 16px;
        padding-left: 14px;
        font-family: 'ralewayregular';
    }
    #thank .header_top {
        width: 100%;
        background-color: #212122;
    }
    #thank .menu_bg {
        width: 100%;
        background-color: #1e2b37;
    }
    #thank .header_c {
        float: left;
    }
    #thank .header_phone {
        float: left;
        margin-top: 8px;
    }
    #thank .header_phone ul li {
        color: white;
        font-family: 'robotoregular';
        font-size: 11px;
        list-style: none;
        padding-right: 15px;
        display: inline-block;
    }
    #thank .socialicns_header {
        float: right;
        margin-bottom: 7px;
        margin-top: 11px;
    }
    #thank .socialicns_header ul li {
        display: inline-block;
        list-style: outside none none;
    }
    #thank .socialicns_header ul li a {
        padding: 5px;
    }
    #thank .fa.fa-phone {
        color: #f4ad60;
        font-size: 18px;
        padding-right: 8px;
    }
    #thank .fa.fa-paper-plane {
        color: #f4ad60;
        font-size: 18px;
        padding-right: 8px;
    }
    #thank .fa.fa-clock-o {
        color: #f4ad60;
        font-size: 18px;
        padding-right: 8px;
    }
    #thank .fa.fa-facebook-square {
        color: white;
        font-size: 18px;
        padding-right: 2px;
    }
    #thank .fa.fa-twitter {
        color: white;
        font-size: 18px;
        padding-right: 2px;
    }
    #thank .fa.fa-google-plus-square {
        color: white;
        font-size: 18px;
        padding-right: 2px;
    }
    #thank .header_menu {
        float: left;
        margin-top: 9px;
    }
    #thank .navbar-default .navbar-nav > li > a {
        color: white;
        font-family: 'robotomedium';
        text-transform: uppercase;
        font-size: 14px;
        padding: 15px 16px 7px 18px;
    }
    #thank .logo {
        float: left;
        z-index: 2147483647;
    }
    #thank .header {
        float: left;
        margin-bottom: 8px;
        margin-top: 9px;
    }
    #thank .navbar-default {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
        border-color: transparent !important;
        margin-bottom: 0;
    }
    #thank .leading_bg {
        min-height: 444px;
        float: left;
        width: 100%;
    }
    #thank .leading_cnter {
        float: right;
        margin-bottom: 80px;
    }
    #thank .leading_txt {
        float: left;
    }
    #thank .leading_txt h2 {
        color: white;
        font-family: 'robotoregular';
        font-size: 30px;
        margin-top: 40px;
        padding-left: 33px;
    }
    #thank .leading_txt p {
        color: white;
        font-family: 'robotoregular';
        font-size: 13px;
        margin-bottom: 0;
        padding-left: 33px;
    }
    #thank .leading_txt1 {
        float: left;
        margin-top: 12px;
    }
    #thank .leading_txt1 p {
        color: white;
        font-family: 'robotoregular';
        font-size: 13px;
        margin-bottom: 0;
        padding-left: 33px;
    }
    #thank .rightemail_sec {
        float: left;
        margin-top: 50px;
    }
    #thank .rightemail_sec p {
        color: #f3990a;
        font-family: 'robotoregular';
        font-size: 15px;
        margin-bottom: 0;
        padding-left: 33px;
    }
    #thank .borderlft {
        border-left: 1px solid #f3990a;
        height: 144px;
        width: 4px;
        margin-left: 13px;
        margin-top: -24px !important;
        float: left;
    }
    #thank .leading_txt2 {
        float: left;
    }
    #thank .leading_txt2 h2 {
        color: #f3990a;
        font-family: 'robotoregular';
        font-size: 18px;
        padding-left: 33px;
    }
    #thank .leading_txt2 p {
        color: white;
        font-family: 'robotoregular';
        font-size: 13px;
        margin-bottom: 0;
        padding-left: 33px;
    }
    #thank .copyright_bg {
        background-color: #141b21;
        width: 100%;
        float: left;
    }
    #thank .copy_center {
        float: left;
        margin-bottom: 12px;
        margin-top: 12px;
    }
    #thank .copy_left {
        float: left;
    }
    #thank .copy_left p {
        color: white;
        font-family: "robotoregular";
        font-size: 13px;
        margin-bottom: 0;
    }
    #thank .copy_right {
        float: right;
    }
    #thank .copy_right p {
        color: white;
        font-family: "robotoregular";
        font-size: 14px;
        margin-bottom: 0;
    }
    #thank .copy_right a {
        color: white;
        font-family: "robotoregular";
        font-size: 14px;
        margin-bottom: 0;
        font-weight: 700;
    }
    #thank .sliderbx {
        padding: 12px;
        border: 1px solid #ccc !important;
        margin-bottom: 25px !important;
        top: 8px;
    }
    #thank .slider_bggg {
        float: left;
        width: 100%;
    }
    #thank .slidertxt1.col-lg-12.col-xs-12 {
        background-color: white;
    }
    #thank .navbar-default .navbar-nav > li > a:hover, #thank .navbar-default .navbar-nav > li > a:focus {
        color: white !important;
    }
    #thank .bottom_bg {
        background: rgba(0, 0, 0, 0) url(../images/bg9.jpg) no-repeat fixed 0 0 / cover;
        width: 100%;
        float: left;
    }
    #thank .botm_bx {
        background-color: #1f2b37;
        min-height: 286px;
    }
    #thank .home_map {
        float: left;
        margin-bottom: 24px;
    }
    #thank .home_map h3 {
        font-family: 'robotomedium';
        color: #1f2b37;
        text-align: center;
        font-size: 35px;
        margin-bottom: 24px;
    }
    #thank .rentcar_bookingform {
        float: left;
        background: rgba(31, 43, 55, .6);
    }
    #thank .rentcar_bookingform h1 {
        color: white;
        font-size: 25px !important;
        font-family: 'robotoregular';
        text-transform: uppercase;
        font-weight: 700;
        padding-left: 15px;
        margin-bottom: -2px !important;
    }
    #thank .booking_form4 {
        padding: 15px;
    }
    #thank .form_btm {
        float: left;
        margin-top: 12px;
    }
    #thank .form_ulli {
        float: left;
        margin-top: 18px;
    }
    #thank .form_ulli ul li {
        list-style: none;
        display: inline-block;
        color: white;
        font-family: "robotoregular";
        font-size: 14px;
        margin-bottom: 0;
        padding-right: 16px;
    }
    #thank .fa.fa-check {
        color: white;
        font-size: 18px;
        padding-right: 8px;
    }
    #thank .guote_btn {
        float: right;
    }
    #thank .guote_btn input {
        background-color: #f29907 !important;
        float: right;
        border: medium none !important;
        color: #fff !important;
        font-family: Tahoma, Geneva, sans-serif !important;
        font-size: 20px !important;
        height: 38px !important;
        border-radius: 4px;
        width: 183px !important;
    }
    #thank .text_btn2 {
        border-radius: 2px;
        padding: 8px 25px;
        text-align: center;
        background-color: #f29907;
        margin-left: 13px;
        font-weight: 700;
        width: 125px;
    }
    #thank .text_btn {
        border-radius: 2px;
        padding: 6px;
        text-align: center;
        background-color: #1f2b37;
        margin-left: 13px;
        margin-botom: 10px !important;
    }
    #thank .text_btn a {
        font-family: "robotomedium";
        color: #fff !important;
        font-size: 14px;
        text-transform: none !important;
    }
    #thank .text_btn a:hover {
        color: #f4d260;
    }
    #thank .button_s {
        margin: 0 auto;
    }
    #thank .button_s {
        margin: 14px auto 15px;
    }
    #thank .price {
        float: right;
        height: 41px;
        margin-top: -216px !important;
        background-color: #f29907;
    }
    #thank .price p {
        font-size: 20px !important;
        text-align: center !important;
        padding-top: 4px;
        font-family: "robotoregular";
        color: white !important;
    }
    #thank .whychose_us {
        float: right;
        background-color: #00aeef;
        border-radius: 6px;
    }
    #thank .sub-menu {
        width: 223%;
        padding: 5px 0;
        position: absolute;
        top: 33px !important;
        left: 0;
        z-index: -1;
        opacity: 0;
        transition: opacity linear 0.15s;
        box-shadow: 0 2px 3px rgba(0, 0, 0, .2);
        background-color: #1e2b37;
    }
    #thank .sub-menu li a {
        padding: 4px 28px;
        display: block;
        font-size: 15px;
        font-family: 'robotoregular';
    }
    #thank .sub-menu li a:hover, #thank .sub-menu .current-item a {
        color: white !important;
        background-color: black;
    }
    #thank .header_menu li {
        margin: 0;
        list-style: none;
        font-family: 'robotoregular';
    }
    #thank .header_menu a {
        transition: all linear 0.15s;
        color: white;
    }
    #thank .header_menu li:hover > a, #thank .header_menu .current-item > a {
        text-decoration: none;
        color: #be5b70;
    }
    #thank .header_menu .arrow {
        font-size: 11px;
        line-height: 0%;
    }
    #thank .header_menu > ul > li {
        float: left;
        display: inline-block;
        position: relative;
        font-size: 19px;
    }
    #thank .header_menu > ul > li > a {
        padding: 10px 40px;
        display: inline-block;
        text-shadow: 0 1px 0 rgba(0, 0, 0, .4);
    }
    #thank .header_menu > ul > li:hover > a, #thank .header_menu > ul > .current-item > a {
        background: #2e2728;
    }
    #thank .header_menu li:hover .sub-menu {
        z-index: 1;
        opacity: 1;
    }
    #thank #nav > a {
        display: none;
    }
    #thank #nav li {
        position: relative;
    }
    #thank #nav > ul {
        height: 3.75em;
    }
    #thank #nav > ul > li {
        width: 25%;
        height: 100%;
        float: left;
    }
    #thank #nav li ul {
        display: none;
        position: absolute;
        top: 100%;
    }
    #thank #nav li:hover ul {
        display: block;
    }
    @media only screen and (max-width: 40em) {
        #thank #nav {
            position: relative;
        }
        #thank #nav:not(:target) > a:first-of-type, #thank #nav:target > a:last-of-type {
            display: block;
        }
        #thank #nav > ul {
            height: auto;
            display: none;
            position: absolute;
            left: 0;
            right: 0;
        }
        #thank #nav:target > ul {
            display: block;
        }
        #thank #nav > ul > li {
            width: 100%;
            float: none;
        }
        #thank #nav li ul {
            position: static;
        }
    }
    #thank .renta_p h2 {
        font-size: 23px;
        color: white;
        padding-top: 2px;
        font-family: 'robotoregular';
    }
    #thank .inner_page1 {
        margin-top: 50px;
        margin-bottom: 50px;
        background-color: white;
        box-shadow: 0 42px 15px -38px #c5c4c3;
    }
    #thank .inner_page1 h1 {
        font-size: 20px !important;
        color: #325d84;
        padding-left: 12px;
        font-family: 'robotoregular';
    }
    #thank .inner_pages1 {
        margin-top: 50px;
        left: 150px;
        margin-bottom: 50px;
        background-color: white;
        box-shadow: 0 42px 15px -38px #c5c4c3;
    }
    #thank .inner_pages1 h1 {
        font-size: 20px !important;
        color: #325d84;
        padding-left: 12px;
        font-family: 'robotoregular';
    }
    #thank .about-us1.col-lg-12 > p {
        color: #4c4c4c;
        font-family: "robotoregular";
        font-size: 13px;
        margin-bottom: 0;
        padding-left: 12px;
    }
    #thank .about-us1 h2 {
        font-size: 20px !important;
        color: #325d84;
        padding-left: 12px;
        font-family: 'robotoregular';
    }
    #thank .about-us1 h3 {
        font-size: 20px !important;
        color: #325d84;
        padding-left: 12px;
        font-family: 'robotoregular';
    }
    #thank .about-us1.col-lg-12 {
        margin-bottom: 22px;
    }
    #thank .inner_page h1 {
        color: #325d84;
        font-family: 'robotoregular';
        font-size: 20px !important;
        padding-left: 25px;
    }
    #thank .inner_page {
        margin-top: 50px;
        background-color: white;
        margin-bottom: 54px;
        box-shadow: 0 42px 15px -38px #c5c4c3;
    }
    #thank .address_p h3 {
        color: #325d84;
        font-family: 'robotoregular';
        font-size: 20px !important;
        margin-bottom: 0;
        margin-top: 0;
    }
    #thank .address_p p {
        color: #4c4c4c;
        font-family: 'robotoregular';
        font-size: 13px;
        margin-top: 8px;
        line-height: 20px;
    }
    #thank .vehiclerates h2 {
        border-bottom: 4px solid #000;
        color: #325d84;
        font-family: 'robotoregular';
        font-size: 20px;
        margin-bottom: 0;
        margin-top: 0;
        padding-bottom: 9px;
    }
    #thank .dl-menuwrapper button.dl-active, #thank .dl-menuwrapper button:hover, #thank .dl-menuwrapper ul {
        background-color: #1e2b37 !important;
    }
    #thank .about-us1.col-lg-12 a {
        font-size: 20px;
        color: #4c4c4c;
    }
    #thank .noticeboard > p {
        font-family: 'robotoregular';
        font-size: 18px !important;
    }
    #thank .notice_p {
        float: left;
        margin-bottom: 12px;
        margin-left: 34px;
        margin-top: 17px;
    }
    #thank .dropdown {
        float: left;
        margin-top: 9px;
    }
    #thank .dropdownhover-bottom {
        width: 521px !important;
    }
    #thank .menubg {
        float: right;
        margin-right: 15px;
        margin-top: -193px;
    }
    #thank .dropdown-menu.dropdownhover-bottom > li {
        border-bottom: 1px solid #d9d9d9 !important;
        margin-left: 12px !important;
        width: 150px !important;
    }
    #thank .dropdown-menu > li > a {
        color: #353535 !important;
        font-family: "robotoregular";
        font-size: 16px;
        padding: 8px 1px;
    }
    #thank .dropdown-menu > li > a:hover, #thank .dropdown-menu > li > a:focus {
        background-color: white !important;
        color: #f4ad60;
        transition: color 800ms ease 0s !important;
    }
    #thank .rentcar_bookingform.col-lg-12.col-xs-12.col-sm-12 > img {
        height: 71px !important;
        margin-left: 13px;
        margin-top: 11px;
        width: 213px !important;
    }
    #thank .inner_bg221 {
        background: url(../images/banner122.jpg) no-repeat fixed 0 0 / cover rgba(0, 0, 0, 0);
        float: left;
        width: 100%;
    }

</style>
<div class="container">
    <div class="row">
        
        <div class="col-md-12">
            <div id="thank">
                <div class="inner_page col-lg-12 col-xs-12"><h1 id="msgs" >Thank You</h1>
                    <div class="noticeboard" style="min-height:330px;"><p
                                style="padding:20px 20px 0; font-size:16px; ">Dear {{$book['name']}}, Assalam o Alaikum<br>
                            Thank You Very Much for Your booking request we will get back to you shortly. For urgent
                            questions call
                           
                            Call Us:92 0300-4412202<br> Regards,<br> Booking Team<br> Areloite Rent A car<br>
                            @isset($site)
                                {{$site['site_email']}}
                                @endisset
                        
                           </p>
                        <div class="notice_p">&nbsp;</div>
                    </div>
                </div>
            
            
            </div>
        </div>
    
    </div>
</div>

</body>
</html>