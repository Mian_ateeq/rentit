<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white left" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{route('admin.dashboard')}}">
            
            Admin Dashboard
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="{{URL::to('admin_ui/assets/img/theme/team-1-800x800.jpg')}}">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    {{--<a href="#" class="dropdown-item">--}}
                        {{--<i class="ni ni-single-02"></i>--}}
                        {{--<span>My profile</span>--}}
                    {{--</a>--}}
                   
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="#">
                            <img src="{{URL::to('admin_ui/assets/img/brand/blue.png')}}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
        
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.dashboard')}}">
                        <i class="ni ni-tv-2 text-primary"></i> Dashboard
                    </a>
                </li>
    
                @can ('vehicle')
                <li class="nav-item">
                    <a class="nav-link  collapsed" href="#navbar-car" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-car">
                        <i class="fas fa-shuttle-van text-primary"></i>
                        <span class="nav-link-text">Car</span>
                    </a>
                    <div class="collapse" id="navbar-car" style="">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('cartype') }}" class="nav-link">Car Type</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('car') }}" class="nav-link">Car</a>
                            </li>

                        </ul>
                    </div>
                </li>
                @endcan
                @can ('booking')
                <li class="nav-item">
                    <a class="nav-link  collapsed" href="#navbar-booking" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-car">
                        <i class="fa fa-book text-primary"></i>
                        <span class="nav-link-text">Booking</span>
                    </a>
                    <div class="collapse" id="navbar-booking" style="">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('new_booking') }}" class="nav-link">New Booking</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('accepted_booking') }}" class="nav-link">Accepted Booking</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('rejected_booking') }}" class="nav-link">Rejected  Booking</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('accept.calendor') }}" class="nav-link">Approve Booking</a>
                            </li>

                        </ul>
                    </div>
                </li>
                @endcan
                @can ('client')
    
                <li class="nav-item">
                    <a class="nav-link  collapsed" href="#navbar-client" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-car">
                        <i class="fa fa-user text-primary"></i>
                        <span class="nav-link-text">Client</span>
                    </a>
                    <div class="collapse" id="navbar-client" style="">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('client') }}" class="nav-link">View Client</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('client.create') }}" class="nav-link">Add Client</a>
                            </li>

                        </ul>
                    </div>
                </li>
                @endcan
                @can ('blog')
                <li class="nav-item">
                    <a class="nav-link  collapsed" href="#navbar-blog" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-car">
                        <i class="fab fa-blogger-b text-primary"></i>
                        <span class="nav-link-text">Blog</span>
                    </a>
                    <div class="collapse" id="navbar-blog" style="">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('blog') }}" class="nav-link">View Blog</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('blog.create') }}" class="nav-link">Add Blog</a>
                            </li>

                        </ul>
                    </div>
                </li>
                @endcan
                @can ('site_config')
                <li class="nav-item">
                    <a class="nav-link  collapsed" href="#navbar-setting" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-car">
                        <i class="fab fa-blogger-b text-primary"></i>
                        <span class="nav-link-text">Site Setting</span>
                    </a>
                    <div class="collapse" id="navbar-setting" style="">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('slider') }}" class="nav-link">Slider</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('site.setting') }}" class="nav-link">Site Setting</a>
                            </li>

                        </ul>
                    </div>
                </li>
                @endcan
                    @can ('admin')

                <li class="nav-item">
              <a class="nav-link  collapsed" href="#navbar-dashboards" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-dashboards">
                <i class="fa fa-user text-primary"></i>
                <span class="nav-link-text">User</span>
              </a>
              <div class="collapse" id="navbar-dashboards" style="">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="{{ route('user') }}" class="nav-link">View User</a>
                  </li>
                    <li class="nav-item">
                    <a href="{{ route('permission') }}" class="nav-link">Permission</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{route('role')}}" class="nav-link">Role</a>
                  </li>
                </ul>
              </div>
            </li>
                        @endcan
            </ul>
            <!-- Divider -->


        </div>
    </div>
</nav>
