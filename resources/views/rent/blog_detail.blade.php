@extends('rent.master')
@section('tittle')
    {{$blog->seo_tittle}}
    @stop
@section('content')
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Blog Detail</h2>
                    <ul>
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active"><a href="{{route('blog.index')}}">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-details">
                        <img src="{{URL::to($blog['image'])}}" alt="blog-detail">
                        <a href="#"><h4 class="post-title">{{ucfirst($blog['name'])}}</h4></a>
                        <div class="post-author">
                            <a href=""><i class="icofont icofont-user"></i>By Admin</a>
                            <a href=""><i class="icofont icofont-calendar"></i>{{\Carbon\Carbon::parse($blog['created_at'])->toFormattedDateString()}}</a>
                        </div>
                   
                        <p>
                            {!! $blog['body'] !!}
                        </p>
                       
                        
                        {{--<div class="comments-area">--}}
                            {{--<h4>3 comments</h4>--}}
                            {{--<ul>--}}
                                {{--<li>--}}
                                    {{--<div class="single-comment">--}}
                                        {{--<div class="single-comment-left">--}}
                                            {{--<img src="assets/img/blog/author1.jpg" alt="author" />--}}
                                        {{--</div>--}}
                                        {{--<div class="single-comment-right">--}}
                                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>--}}
                                            {{--<a href="#">Arthur Reyes- &nbsp; June 25, 2018 </a>--}}
                                            {{--<a href="#"><i class="icofont icofont-reply"></i> Reply</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<ul>--}}
                                        {{--<li>--}}
                                            {{--<div class="single-comment">--}}
                                                {{--<div class="single-comment-left">--}}
                                                    {{--<img src="assets/img/blog/author2.jpg" alt="author" />--}}
                                                {{--</div>--}}
                                                {{--<div class="single-comment-right">--}}
                                                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>--}}
                                                    {{--<a href="#">Arthur Reyes- &nbsp; June 25, 2018 </a>--}}
                                                    {{--<a href="#"><i class="icofont icofont-reply"></i> &nbsp;Reply</a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="single-comment">--}}
                                        {{--<div class="single-comment-left">--}}
                                            {{--<img src="assets/img/blog/author3.jpg" alt="author" />--}}
                                        {{--</div>--}}
                                        {{--<div class="single-comment-right">--}}
                                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>--}}
                                            {{--<a href="#">Arthur Reyes- &nbsp; June 25, 2018 </a>--}}
                                            {{--<a href="#"><i class="icofont icofont-reply"></i> &nbsp;Reply</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="blog-reply">--}}
                            {{--<h2>Leave a Comment</h2>--}}
                            {{--<p>You must be logged in to post a comment.</p>--}}
                            {{--<form action="#" method="POST">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-lg-6">--}}
                                        {{--<input type="text" name="replyname" placeholder="Name*">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-lg-6">--}}
                                        {{--<input type="text" name="replysite" placeholder="Website*">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-lg-6">--}}
                                        {{--<input type="email" name="replyemail" placeholder="Email*">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-lg-6">--}}
                                        {{--<input type="text" name="replysubject" placeholder="Subject*">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-lg-12">--}}
                                        {{--<textarea placeholder="Your Comments*"></textarea>--}}
                                        {{--<button class="theme-btn theme-btn6" type="submit" name="replysubmit">Post Comment</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                        <div id="disqus_thread"></div>
                        <script>
		
		                    /**
		                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
		                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
		                    
                            var disqus_config = function () {
                            this.page.url ='{{route('blog.view',$blog->slug)}}';  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = '{{$blog->id}}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
		                    (function() { // DON'T EDIT BELOW THIS LINE
			                    var d = document, s = d.createElement('script');
			                    s.src = 'https://areolite.disqus.com/embed.js';
			                    s.setAttribute('data-timestamp', +new Date());
			                    (d.head || d.body).appendChild(s);
		                    })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>
                </div>
               
            </div>
        </div>
    </section>
    
    @stop