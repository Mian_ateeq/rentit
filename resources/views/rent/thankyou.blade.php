@extends('rent.master')
@section('content')
    
    
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Booking </h2>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                <div id="thank">
                    <div class="inner_page col-lg-12 col-xs-12"><h1 id="msgs" style="font-size: 65px;">Thank You</h1>
                    <div class="noticeboard" style="min-height:330px;"><p
                    style="padding:20px 20px 0; font-size:16px; ">Dear {{$book['name']}}, Assalam o Alaikum<br>
                    Thank You Very Much for Your booking request we will get back to you shortly. For urgent
                    questions call
                    <!--<a href="tel:+923008557698" ><span style="padding:20px 20px 0; font-size:16px; ">-->
                    Call Us:92 300 855 7698<!--</span></a>--><br> Regards,<br> Booking Team<br> Pakistan Car
                    Rentals<br> bestrentacar.com</p>
                    <div class="notice_p">&nbsp;</div><!--notice p--> </div>
                    </div>
                    
                   
                </div>
            </div>
        
        </div>
    </div>
@stop