@extends('rent.master')
@section('tittle','Service')


@section('content')

    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Services</h2>
                    <ul>
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active"><a href="{{route('about')}}">Service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sec-title mb-50">
                        <h2>Services We Provide</h2>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-about">
                        <i ><img src="https://img.icons8.com/ios/40/000000/champagne-filled.png"></i>
                        <h4>PARTIES</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-about">
                        <i><img src="https://img.icons8.com/ios/40/000000/newlyweds-filled.png"></i>
                        <h4>WEDDINGS</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-about">
                        <i ><img src="https://img.icons8.com/ios/40/000000/drums.png"></i>
                        <h4>CONCERTS</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-about">
                        <i><img src="https://img.icons8.com/ios/40/000000/birthday-filled.png"></i>
                        <h4>BIRTHDAYS</h4>
                    </div>
                </div>


            </div>
            <br>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-about">
                        <i><img src="https://img.icons8.com/pastel-glyph/40/000000/motarboard.png"></i>
                        <h4>CONVOCATIONS</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-about">
                        <i><img src="https://img.icons8.com/ios/40/000000/airplane-landing-filled.png"></i>
                        <h4>AIRPORT TRANSFERS</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-about">
                        <i><img src="https://img.icons8.com/color/40/000000/subway.png"></i>
                        <h4>Railway TRANSFERS</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop