@extends('rent.master')
@section('tittle','About US')

@section('content')
    <style>
        .tickmarks {
            margin: 20px 0 0;
            padding: 0;
            width: 650px;
            font-size: 12px;
        }
        .tickmarks li {
            background-image: url('http://www.rentacarservice.pk/images/tick-arrow.jpg');
            background-repeat: no-repeat;
            list-style: none outside none;
            margin: 0;
            padding-bottom: 8px;
            padding-left: 20px;
            font-size: 14px;
        }
        h2  {
            color: black
        }
        </style>
            <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>About Us</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>About Us</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>

    <section class="about-bottom2 ptb-100">
        <div></div>
        <div class="container">



            <div class="row m-3">

                <div class="container">
                    <h2 >Who we are?</h2>
                    <br>

                    <p class="text-justify">We are very pleased to inform that for establishing a solid foundation in the Pakistan, we are opening car rental service in Lahore And Islamabad. It is great honor for us introducing Best Rent A Car in Lahore, offering cheap and quality car rental service in Lahore And Islamabad the business capital of Pakistan.  We have been providing ground transportation services to corporate clients, tourists and individual users for the last 15 years.  Our rent a car service features trusted special occasions rent a car services like wedding and reception, sightseeing and tours, city to city trip and much more. </p>
                    <p class="text-justify">Our success can be associated to our approach towards client satisfaction. We can arrange transportation from and to any destination, including airport terminal or railway station pick-ups and drop off. We provide range of cars based upon your particular needs</p>
                    <p class="text-justify">Our committed and experienced staffs are capable of react as fast as possible, providing the car of your liking. The main features of our services are, convenience, performance, capacity, comfort and value for money, additionally we take great pride in our professionalism, reliability and focus on the smaller points. Regardless if you are a long term corporate client or require a car for leisure we have wide-ranging easily available vehicles to fulfill your all travelling requiremen</p>

                    <p class="text-justify">Best Rent a Car Pakistan has a variety of vehicles in outstanding condition with affordable rental packages. Our workplace is situated in Lahore, which is a very appealing spot for business guests from other locations of Pakistan.</p>

                </div>


                <div class="container">
                    <br>

                    <h2>Mission</h2>
                    <br>

                    <p class="text-justify">At Best  Rent a Car Service , providing something special to every aspect of your customer experience is our objective. More clients than ever are considering us as their car hire in Pakistan and especially in Lahore.</p>

                    <br>
                    <p>We aim to earn our clients' long-term loyalty by trying to provide more than which is promised, being reliable and honest and "going further" to offer superb customized support that produces an excellent business experience.</p>
                </div>



                <div class="container">
                    <br>


                    <h2>Values</h2>
                    <br>

                    <ul class="tickmarks" style="width: 100%">

                        <li>A promise to great value for clients</li>
                        <li>A focus to quality and support</li>
                        <li>Highest standards of reliability and professionalism</li>
                        <li>Pleasant car rental experience for employees and  clients equally.</li>
                        <li>We are focused on a well-maintained, secure and nice  rent a car service</li>
                    </ul>

                </div>



                <div class="container">
                    <br>

                    <h2>Contact Information:</h2>
                    <br>
                    <br>
                    <p style="width: 100%"><strong>Best Rent A Car </strong><br>
                        <strong>Office # 1</strong> : Office #14 Dubai Plaza Liaqat Chowk Lahore Pakistan <br>

                        0300-4629435 </p>

                </div>

            </div>





        </div>
    </section>


@stop