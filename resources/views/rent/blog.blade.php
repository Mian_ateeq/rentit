@extends('rent.master')
@section('tittle','Blog')
@section('content')
    <style>
        .pagination>li>a, .pagination>li>span { border-radius: 50% !important;margin: 0 5px;
            
            /* border: 3px solid; */
            color: #ffff;
            display: block;
            /* font-size: 18px; */
            /* font-weight: 700; */
            line-height: 36px;
            height: 52px;
            width: 50px;
        }
        .pagination {
            display: -ms-flexbox;
            display: -webkit-inline-box;
            padding-left: 0;
            list-style: none;
            border-radius: .25rem;
        }
        
        .pagination-contents{
            
            text-align: center;
            position: relative;
            padding: 80px 0;
        }
        
        .pagination-contents:after, .pagination-content:before {
            background-color: #f4f4f4;
            content: '';
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
        }
        .pagination-contents:before {
            right: 100%;
        }
        .pagination-contents:after {
            left: 100%;
        }
        .pagination-contents:after, .pagination-contents:before {
            background-color: #f4f4f4;
            content: '';
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
        }
    </style>
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Blog</h2>
                    <ul>
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active"><a href="{{route('blog.index')}}">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-70 pb-100">
        <div class="container">
            <div class="row">
                
                @foreach($blogs as $blog)
                <div class="col-xl-6 col-lg-12">
                    <div class="single-post">
                        <div class="row flexbox-center">
                            <div class="col-sm-5">
                                <div class="post-thumbnail">
                                    <a href="{{route('blog.view',$blog->slug)}}"><img src="{{$blog->image}}" alt="blog"></a>
                                </div>
                            </div>
                            <div class="col-sm-7" style="padding-top: 10px">
                                <div class="post-details" style="padding-bottom: 10px">
                                    <a href=""><h4 class="post-title">{{$blog['name']}}</h4></a>
                                    <div class="post-author">
                                        <a href="{{route('blog.view',$blog->slug)}}"><i class="icofont icofont-user"></i>Admin</a>
                                        <a href="{{route('blog.view',$blog->slug)}}"><i class="icofont icofont-calendar"></i>{{\Carbon\Carbon::parse($blog['created_at'])->toFormattedDateString()}}</a>
                                    </div>
                                    <p>{!! ucfirst(substr(strip_tags($blog->body),0,50)) !!}...</p>
                                    <a href="{{route('blog.view',$blog->slug)}}" class="theme-btn theme-btn2">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                    @endforeach
                
            </div>
            
        </div>
        <div class="container" style="align-content: center">
         <div class="row">
             <div class="col-md-3"></div>
             <div class="col-md-6">
                 {{$blogs->onEachSide(5)->links() }}
             </div>
         </div>
        </div>
    </section>
    
    @stop