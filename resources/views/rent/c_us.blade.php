@extends('rent.master')
@section('tittle','Contact US')

@section('content')
    @if(Session::has('success'))
        <script type="text/javascript">
            swal("Your Message Has Been Saved We Contact Us AS Soon As Possible!", "", "success");
        </script>
    @endif
    <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Contact Us</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Contact Us</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section><!-- breadcrumb area end -->
    <!-- contact section start -->
    <section class="gauto-contact-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-lg-7">
                  <div class="contact-left">
                     <h3>Get in touch</h3>
                     <form method="post" action="{{route('contact.us.submit')}}">
                        @csrf
                        <div class="row">
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="text" placeholder="Your Name" required name="name">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="tel" placeholder="Last Name" name="lastname" required> 
                              </div>
                           </div>
                          
                        </div>
                        <div class="row">
                        <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="email" placeholder="Email Address"name="email" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="text" placeholder="Subject" name="subject" required>
                              </div>
                           </div>
                           
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="single-contact-field">
                                 <textarea placeholder="Write here your message" name="message" required></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="single-contact-field">
                                 <button type="submit" class="gauto-theme-btn"><i class="fa fa-paper-plane"></i> Send Message</button>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="contact-right">
                     <h3>Contact information</h3>
                     <div class="contact-details">
                        <p><i class="fa fa-map-marker"></i> Office #14 Dubai Plaza Liaqat Chowk Lahore Pakistan </p>
                        <div class="single-contact-btn">
                           <h4>Email Us</h4>
                           <a href="#">bestrentacar@gmail.com</a>
                        </div>
                        <div class="single-contact-btn">
                           <h4>Call Us</h4>
                           <a href="#">+923004629435</a>
                        </div>
                        <div class="social-links-contact">
                           <h4>Follow Us:</h4>
                           <ul>
                              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                              <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                              <li><a href="#"><i class="fa fa-skype"></i></a></li>
                              <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="gauto-blog-area section_70">
      <div class="container">
      <div class="row">
            <div class="col-md-12">
               <div class="site-heading">
                  <h2>Office Location</h2>
                
               </div>
            </div>
         </div>
         <div class="row mt-2 ">
            <div class="col-md-12">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3401.372882960892!2d74.25804401514414!3d31.51391708137171!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39190370c2e047fb%3A0x8331dba146403cc5!2sBest%20Rent%20A%20Car!5e0!3m2!1sen!2s!4v1583078285602!5m2!1sen!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
         </div>

      </div>
   </section>

    @stop