@extends('rent.master')
@section('tittle','Booking')
@section('content')
    <style>
       
        #div .theme-page-section {
            padding: 30px 0;
        }
        
        #div .theme-page-section-no-pad {
            padding: 0;
        }
        
        #div .theme-page-section-sm {
            padding: 20px 0;
        }
        
        #div .theme-page-section-lg {
            padding: 45px 0;
        }
        
        @media (max-width: 992px) {
            #div .theme-page-section-lg {
                padding: 30px 0;
            }
        }
        
        #div .theme-page-section-xl {
            padding: 60px 0;
        }
        
        @media (max-width: 992px) {
            #div .theme-page-section-xl {
                padding: 45px 0;
            }
        }
        
        #div .theme-page-section-xxl {
            padding: 90px 0;
        }
        
        @media (max-width: 992px) {
            #div .theme-page-section-xxl {
                padding: 60px 0;
            }
        }
        
        #div .theme-page-section-no-pad-bottom {
            padding-bottom: 0;
        }
        
        #div .theme-page-section-dark {
            background: #333;
        }
        
        #div .theme-page-section-gray {
            background: #e6e6e6;
        }
        
        #div .theme-page-section-white {
            background: #fff;
        }
        
        #div .theme-page-section-b-bottom,
        #div .theme-page-section-bb {
            border-bottom: 1px solid #d9d9d9;
        }
        
        #div .theme-page-section-header {
            text-align: center;
            margin-bottom: 30px;
            position: relative;
        }
        
        #div .theme-page-section-header-bb {
            margin-bottom: 20px;
            padding-bottom: 10px;
            border-bottom: 1px solid #e6e6e6;
        }
        
        #div .theme-page-section-header-white {
            color: #fff;
        }
        
        #div .theme-page-section-header-white .theme-page-section-title {
            color: #fff;
        }
        
        #div .theme-page-section-header-white .theme-page-section-title b {
            color: #fff;
        }
        
        #div .theme-page-section-header-sm {
            margin-bottom: 15px;
        }
        
        #div .theme-page-section-header-sm .theme-page-section-title {
            font-size: 18px;
            letter-spacing: 0;
        }
        
        #div .theme-page-section-header-sm .theme-page-section-subtitle {
            font-size: 13px;
            letter-spacing: 0;
            margin-top: 7px;
        }
        
        #div .theme-page-section-header-link {
            margin-top: 10px;
            display: inline-block;
            color: #595959;
            font-size: 17px;
            opacity: 0.45;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=45)";
            filter: alpha(opacity=45);
            text-transform: uppercase;
        }
        
        #div .theme-page-section-header-link:hover {
            text-decoration: none;
            color: #595959;
            opacity: 1;
            -ms-filter: none;
            filter: none;
        }
        
        #div .theme-page-section-header-link-rb {
            position: absolute;
            bottom: 0;
            right: 0;
        }
        
        @media (max-width: 992px) {
            #div .theme-page-section-header-link-rb {
                position: relative;
            }
        }
        
        #div .theme-page-section-subtitle {
            margin-bottom: 0;
            margin-top: 10px;
            font-size: 20px;
            opacity: 0.45;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=45)";
            filter: alpha(opacity=45);
        }
        
        #div .theme-page-section-subtitle-sm {
            font-size: 16px;
        }
        
        #div .theme-page-section-title {
            margin-top: 0;
            margin-bottom: 0;
            letter-spacing: 0.6px;
            font-size: 35px;
            font-weight: 300;
            color: #616161;
        }
        
        #div .theme-page-section-title b {
            color: #505050;
        }
        
        #div .theme-page-section-title-sm {
            font-size: 25px;
            letter-spacing: 0;
        }
        
        #div .theme-page-section-title-lg {
            font-size: 32px;
        }
        
        #div .theme-page-section-title-white {
            color: #fff;
            text-shadow: 1px 2px 10px rgba(0, 0, 0, 0.2);
        }
        
        #div .theme-page-section-lg {
            padding: 45px 0;
        }
        
        @media (max-width: 992px) {
            #div .theme-page-section-lg {
                padding: 30px 0;
            }
        }
        
        #div .row.row-col-static > [class^=col-] {
            position: static;
        }
        
        @media (max-width: 992px) {
            #div .row.row-col-mob-gap > [class^=col-] {
                margin-bottom: 30px;
            }
        }
        
        #div .theme-payment-page-sections .theme-payment-page-sections-item:last-child {
            margin-bottom: 0;
            padding-bottom: 0;
            border-bottom: none;
        }
        
        #div .theme-payment-page-sections-item {
            margin-bottom: 30px;
            padding-bottom: 30px;
            border-bottom: 1px solid #e6e6e6;
        }
        
        #div .theme-payment-page-sections-item-title {
            margin-top: 0;
            margin-bottom: 20px;
            font-size: 20px;
        }
        
        #div .theme-payment-page-sections-item-new-link {
            margin-top: 20px;
            display: inline-block;
            text-decoration: none !important;
        }
        
        #div .theme-payment-page-sections-item-new-extend {
            padding-top: 20px;
        }
        
        #div .theme-payment-page-form-title {
            margin-top: 0;
            margin-bottom: 15px;
            font-size: 16px;
            font-weight: normal;
            font-family: helvetica, Arial, sans-serif;
            letter-spacing: 0;
        }
        
        #div .theme-payment-page-form-item {
            position: relative;
            margin-bottom: 0;
        }
        
        #div .theme-payment-page-form-item select.form-control {
            padding-right: 30px;
        }
        
        #div .theme-payment-page-form-item .form-control {
            border-radius: 2px;
            height: 45px;
            outline: black !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            -webkit-appearance: none;
        }
        
        #div .theme-payment-page-form-item .form-control:before {
            content: "foo";
        }
        
        #div .theme-payment-page-form-item .form-control:focus {
            border-color: #0093d2;
        }
        
        #div .theme-payment-page-form-item > .fa {
            position: absolute;
            top: 0;
            right: 10px;
            content: "";
            font-family: "FontAwesome";
            height: 45px;
            line-height: 45px;
            display: block;
            pointer-events: none;
        }
        
        #div .theme-payment-page-form-item {
            position: relative;
            margin-bottom: 0;
        }
        
        #div .theme-payment-page-form-item select.form-control {
            padding-right: 30px;
        }
        
        #div .theme-payment-page-form-item .form-control {
            border-radius: 2px;
            height: 45px;
            outline: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            -webkit-appearance: none;
        }
        
        #div .theme-payment-page-form-item .form-control:before {
            content: "foo";
        }
        
        #div .theme-payment-page-form-item .form-control:focus {
            border-color: #0093d2;
        }
        
        #div .theme-payment-page-form-item > .fa {
            position: absolute;
            top: 0;
            right: 10px;
            content: "";
            font-family: "FontAwesome";
            height: 45px;
            line-height: 45px;
            display: block;
            pointer-events: none;
        }
        
        #div .btn-primary-invert,
        #div .btn-primary-inverse {
            background: #d01818;
            border-color: #ff570f;
            color: #fff;
        }
        
        #div .btn-primary-invert:hover,
        #div .btn-primary-inverse:hover {
            color: #fff;
            background: #ff570f;
            border-color: #f04800;
        }
        
        #div .btn-ghost.btn-primary-inverse {
            color: #d01818;
        }
        
        #div .btn-ghost.btn-primary-inverse:hover {
            background: #d01818;
        }
        
        #div .theme-hero-text-btn.btn-primary-inverse {
            -webkit-box-shadow: 0 5px 15px rgba(90, 27, 0, 0.25);
            box-shadow: 0 5px 15px rgba(90, 27, 0, 0.25);
        }
        
        #div .theme-payment-page-form-item .form-control {
            border-radius: 2px;
            height: 45px;
            outline: auto !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            -webkit-appearance: none;
        }
    
    </style>
    
    
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Booking </h2>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <form action="{{route('booking')}}" method="post" id="booking">
                @csrf
            
            <div class="col-md-12">
                
                <div id="div">
                    <div class="theme-page-section theme-page-section-lg">
                        <div class="container">
                            <div class="row row-col-static row-col-mob-gap" id="sticky-parent" data-gutter="60">
                                <div class="col-md-12">
                                    <h3 class="theme-payment-page-sections-item-title">Booking a Car Was Never much
                                        Easy. Just fill The form and get the Car</h3>
                                </div>
                                <br>
                                <br>
                                <div class="col-md-2"></div>
                             
                                <div class="col-md-8 ">
                                    <div class="theme-payment-page-sections">
                                        
                                        
                                        <div class="theme-payment-page-sections-item">
                                            <h3 class="theme-payment-page-sections-item-title"></h3>
                                            <div class="theme-payment-page-form">
                                                <div class="row row-col-gap" data-gutter="20">
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <input class="form-control" type="text" name="name" required placeholder="Name"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <input class="form-control" name="email" required type="email"
                                                                   placeholder="Email"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <input class="form-control international-inputmask" id="international-mask" name="phone" required type="text"
                                                                   placeholder="Phone No"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <input class="form-control cc-inputmask" name="cnic" required type="text"
                                                                   placeholder="CNIC"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="theme-payment-page-sections-item">
                                            <div class="theme-payment-page-form _mb-20">
                                                <div class="row row-col-gap" data-gutter="20">
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <i class="fa fa-angle-down"></i>
                                                            <select type="number" class="form-control" required name="city">
                                                                <option value="">Select City</option>
                                                                <option value="Lahore">Lahore</option>
                                                                <option value="Islamabad">Islamabad</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <i class="fa fa-angle-down"></i>
                                                            <select class="form-control" required name="driver" >
                                                                <option value="">Driver</option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <i class="fa fa-angle-down"></i>
                                                            <select class="form-control car_tpe" required name="vehicle_type" >
                                                                <option value="">Vehicle Type</option>
                                                                @foreach($types as $type)
                                                                    <option value="{{$type['id']}}">{{ucfirst($type['name'])}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <i class="fa fa-angle-down"></i>
                                                            <select class="form-control car_id" required name="vehicle_model" >
                                                                <option value="">Select Type First</option>


                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <input class="form-control" required name="day_oute" data-toggle="datepicker"
                                                                   type="text" placeholder="Day Out"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <input class="form-control" required name="day_return" data-toggle="datepicker"
                                                                   type="text" placeholder="Day Return"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <i class="fa fa-angle-down"></i>
                                                            <select class="form-control" required name="time_out">
                                                                <option value="">Time Out</option>
                                                                <option value="06:00 AM">06:00 AM</option>
                                                                <option value="06:30 AM">06:30 AM</option>
                                                                <option value="07:00 AM">07:00 AM</option>
                                                                <option value="07:30 AM">07:30 AM</option>
                                                                <option value="08:00 AM">08:00 AM</option>
                                                                <option value="08:30 AM">08:30 AM</option>
                                                                <option value="09:00 AM" selected="selected">9:00 AM</option>
                                                                <option value="09:30 AM">09:30 AM</option>
                                                                <option value="10:00 AM">10:00 AM</option>
                                                                <option value="10:30 AM">10:30 AM</option>
                                                                <option value="11:00 AM">11:00 AM</option>
                                                                <option value="11:30 AM">11:30 AM</option>
                                                                <option value="12:00 PM">12:00 PM</option>
                                                                <option value="12:30 PM">12:30 PM</option>
                                                                <option value="01:00 PM">01:00 PM</option>
                                                                <option value="01:30 PM">01:30 PM</option>
                                                                <option value="02:00 PM">02:00 PM</option>
                                                                <option value="02:30 PM">02:30 PM</option>
                                                                <option value="03:00 PM">03:00 PM</option>
                                                                <option value="03:30 PM">03:30 PM</option>
                                                                <option value="04:00 PM">04:00 PM</option>
                                                                <option value="04:30 PM">04:30 PM</option>
                                                                <option value="05:00 PM">05:00 PM</option>
                                                                <option value="05:30 PM">05:30 PM</option>
                                                                <option value="06:00 PM">06:00 PM</option>
                                                                <option value="06:30 PM">06:30 PM</option>
                                                                <option value="07:00 PM">07:00 PM</option>
                                                                <option value="07:30 PM">07:30 PM</option>
                                                                <option value="08:00 PM">08:00 PM</option>
                                                                <option value="08:30 PM">08:30 PM</option>
                                                                <option value="09:00 PM">09:00 PM</option>
                                                                <option value="09:30 PM">09:30 PM</option>
                                                                <option value="10:00 PM">10:00 PM</option>
                                                                <option value="10:30 PM">10:30 PM</option>
                                                                <option value="11:00 PM">11:00 PM</option>
                                                                <option value="11:30 PM">11:30 PM</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="theme-payment-page-form-item form-group">
                                                            <i class="fa fa-angle-down"></i>
                                                            <select class="form-control" required name="time_return">
                                                                <option value="">Time Return</option>
                                                                <option value="06:00 AM">06:00 AM</option>
                                                                <option value="06:30 AM">06:30 AM</option>
                                                                <option value="07:00 AM">07:00 AM</option>
                                                                <option value="07:30 AM">07:30 AM</option>
                                                                <option value="08:00 AM">08:00 AM</option>
                                                                <option value="08:30 AM">08:30 AM</option>
                                                                <option value="09:00 AM" selected="selected">9:00 AM</option>
                                                                <option value="09:30 AM">09:30 AM</option>
                                                                <option value="10:00 AM">10:00 AM</option>
                                                                <option value="10:30 AM">10:30 AM</option>
                                                                <option value="11:00 AM">11:00 AM</option>
                                                                <option value="11:30 AM">11:30 AM</option>
                                                                <option value="12:00 PM">12:00 PM</option>
                                                                <option value="12:30 PM">12:30 PM</option>
                                                                <option value="01:00 PM">01:00 PM</option>
                                                                <option value="01:30 PM">01:30 PM</option>
                                                                <option value="02:00 PM">02:00 PM</option>
                                                                <option value="02:30 PM">02:30 PM</option>
                                                                <option value="03:00 PM">03:00 PM</option>
                                                                <option value="03:30 PM">03:30 PM</option>
                                                                <option value="04:00 PM">04:00 PM</option>
                                                                <option value="04:30 PM">04:30 PM</option>
                                                                <option value="05:00 PM">05:00 PM</option>
                                                                <option value="05:30 PM">05:30 PM</option>
                                                                <option value="06:00 PM">06:00 PM</option>
                                                                <option value="06:30 PM">06:30 PM</option>
                                                                <option value="07:00 PM">07:00 PM</option>
                                                                <option value="07:30 PM">07:30 PM</option>
                                                                <option value="08:00 PM">08:00 PM</option>
                                                                <option value="08:30 PM">08:30 PM</option>
                                                                <option value="09:00 PM">09:00 PM</option>
                                                                <option value="09:30 PM">09:30 PM</option>
                                                                <option value="10:00 PM">10:00 PM</option>
                                                                <option value="10:30 PM">10:30 PM</option>
                                                                <option value="11:00 PM">11:00 PM</option>
                                                                <option value="11:30 PM">11:30 PM</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                   
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="theme-payment-page-sections-item">
                                            <div class="theme-payment-page-booking">
                                                <button type="submit" class="btn _tt-uc btn-primary-inverse btn-lg btn-block"  href="#">Book
                                                    Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            
            
            </div>
            </form>
        
        </div>
    </div>
@stop