@extends('rent.master')
@section('tittle','Car Detail')

@section('content')
<section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Car Detail</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>car Detail</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="gauto-car-booking section_70">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <div class="car-booking-image">
                  
                     <div id="gallery1" style="width: 100%!important">
       
               <img alt="Preview Image 1" src="{{URL::to($car['car_img1'])}}" data-image="{{URL::to($car['car_img1'])}}" data-description="Preview Image 1 Description" style="width: 100%!important">
               <img alt="Preview Image 1" src="{{URL::to($car['car_img2'])}}" data-image="{{URL::to($car['car_img2'])}}" data-description="Preview Image 1 Description" style="width: 100%!important">
               <img alt="Preview Image 1" src="{{URL::to($car['car_img3'])}}" data-image="{{URL::to($car['car_img3'])}}" data-description="Preview Image 1 Description" style="width: 100%!important">
               <img alt="Preview Image 1" src="{{URL::to($car['car_img4'])}}" data-image="{{URL::to($car['car_img4'])}}" data-description="Preview Image 1 Description" style="width: 100%!important">

        
      </div>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="car-booking-right">
                     <p class="rental-tag">rental</p>
                     <h3>{{ $car['name'] }}</h3>
                     <div class="price-rating">
                       
                        <div class="car-rating">
                           <ul>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star-half-o"></i></li>
                           </ul>
                           <p>(123 rating)</p>
                        </div>
                     </div>
                     <p> {{$car['description']}}</p>
                     <div class="car-features clearfix">
                        <ul>
                      
                           <li><i class="fa fa-car"></i> Model:{{ $car['model'] }}</li>
                           <li><i class="fa fa-cogs"></i> {{ $car['Engine_type'] }}</li>
                           <li><i class="fa fa-car"></i> {{ $car['fuel_type'] }}</li>
                           
                          
                        </ul>
                        <ul>
                           <li><i class="fa fa-eye"></i> GPS Navigation</li>
                           <li><i class="fa fa-lock"></i> Anti-Lock Brakes</li>
                          
                        </ul>
                        <ul>
                        <li><i class="fa fa-dashboard"></i> {{ $car['km_drive'] }}km</li>
                        <li><i class="fa fa-empire"></i> {{ $car['engine_power'] }}r</li>
                         
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="gauto-contact-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-lg-7">
                  <div class="contact-left">
                     <h3>Get in touch</h3>
                     <form method="post" action="{{route('contact.us.submit')}}">
                        @csrf
                        <div class="row">
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="text" placeholder="Your Name" required name="name">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="tel" placeholder="Last Name" name="lastname" required> 
                              </div>
                           </div>
                          
                        </div>
                        <div class="row">
                        <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="email" placeholder="Email Address"name="email" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="text" placeholder="Subject" name="subject" required>
                              </div>
                           </div>
                           
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="single-contact-field">
                                 <textarea placeholder="Write here your message" name="message" required></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="single-contact-field">
                                 <button type="submit" class="gauto-theme-btn"><i class="fa fa-paper-plane"></i> Send Message</button>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="contact-right">
                     <h3>Contact information</h3>
                     <div class="contact-details">
                        <p><i class="fa fa-map-marker"></i> Office #14 Dubai Plaza Liaqat Chowk Lahore Pakistan </p>
                        <div class="single-contact-btn">
                           <h4>Email Us</h4>
                           <a href="#">bestrentacar@gmail.com</a>
                        </div>
                        <div class="single-contact-btn">
                           <h4>Call Us</h4>
                           <a href="#">+923004629435</a>
                        </div>
                        <div class="social-links-contact">
                           <h4>Follow Us:</h4>
                           <ul>
                              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                              <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                              <li><a href="#"><i class="fa fa-skype"></i></a></li>
                              <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
     


@stop