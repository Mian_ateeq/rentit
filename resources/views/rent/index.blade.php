<!DOCTYPE html>
<html lang="en-US">
@include('rent.layout.head')

<body>


   @include('rent.layout.top_area')


   @include('rent.layout.header')


   <!-- Slider Area Start -->
   <section class="gauto-slider-area fix">
      <div id="gallery1" style="width: 100%!important">
         @foreach($sliders as $key=>$slider)
         <img alt="Preview Image 1" src="{{ URL::to($slider->image) }}" data-image="{{ URL::to($slider->image) }}" data-description="Preview Image 1 Description" style="width: 100%!important">

         @endforeach
      </div>


   </section>
   <!-- Slider Area End -->


   <section class="gauto-offers-area section_70">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="site-heading">
                  <h4>Come with</h4>
                  <h2>Hot offers</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="offer-tabs">
                  <ul class="nav nav-tabs" id="offerTab" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">All Brands</a>
                     </li>
                     @foreach($car_type as $type)
                     <li class="nav-item">
                        <a class="nav-link" id="nissan-tab" data-toggle="tab" href="#{{$type['name'] }}" role="tab" aria-controls="nissan" aria-selected="false"> {{ $type['name']   }}</a>
                     </li>
                     @endforeach

                  </ul>
                  <div class="tab-content" id="offerTabContent">
                     <!-- All Tab Start -->
                     <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                        <div class="row">
                           @foreach($cars as $car)
                           <div class="col-lg-4">
                              <div class="single-offers">
                                 <div class="offer-image">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <img src="{{ URL::to($car->car_img1) }}" alt="offer 1" />
                                    </a>
                                 </div>
                                 <div class="offer-text">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <h3>{{ $car['name'] }}</h3>
                                    </a>
                                    <!-- <h4>$50.00<span>/ Day</span></h4> -->
                                    <ul>
                                       <li><i class="fa fa-car"></i>Model:{{ $car['model'] }}</li>
                                       <li><i class="fa fa-cogs"></i>{{ $car['Engine_type'] }}</li>
                                       <li><i class="fa fa-dashboard"></i>{{ $car['engine_power'] }}</li>
                                    </ul>
                                    <div class="offer-action">
                                       <a href="tel:+03004629435" class="offer-btn-1">Rent Car</a>
                                       <a href="{{route('vehicle.detail',$car['slug'])}}" class="offer-btn-2">Details</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach

                        </div>

                     </div>
                     <!-- All Tab End -->

                     <!-- Nissan Tab Start -->
                     <div class="tab-pane fade" id="Toyota" role="tabpanel" aria-labelledby="nissan-tab">
                        <div class="row">
                           @foreach($limos_cars as $car)
                           <div class="col-lg-4">
                              <div class="single-offers">
                                 <div class="offer-image">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <img src="{{ URL::to($car->car_img1) }}" alt="offer 1" />
                                    </a>
                                 </div>
                                 <div class="offer-text">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <h3>{{ $car['name'] }}</h3>
                                    </a>
                                    <!-- <h4>$50.00<span>/ Day</span></h4> -->
                                    <ul>
                                       <li><i class="fa fa-car"></i>Model:{{ $car['model'] }}</li>
                                       <li><i class="fa fa-cogs"></i>{{ $car['Engine_type'] }}</li>
                                       <li><i class="fa fa-dashboard"></i>{{ $car['engine_power'] }}</li>
                                    </ul>
                                    <div class="offer-action">
                                       <a href="tel:+03004629435" class="offer-btn-1">Rent Car</a>
                                       <a href="{{route('vehicle.detail',$car['slug'])}}" class="offer-btn-2">Details</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                     <!-- Nissan Tab End -->

                     <!-- Toyota Tab Start -->
                     <div class="tab-pane fade" id="Suzuki" role="tabpanel" aria-labelledby="Toyota-tab">
                        <div class="row">
                           @foreach($sedan_cars as $car)
                           <div class="col-lg-4">
                              <div class="single-offers">
                                 <div class="offer-image">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <img src="{{ URL::to($car->car_img1) }}" alt="offer 1" />
                                    </a>
                                 </div>
                                 <div class="offer-text">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <h3>{{ $car['name'] }}</h3>
                                    </a>
                                    <!-- <h4>$50.00<span>/ Day</span></h4> -->
                                    <ul>
                                       <li><i class="fa fa-car"></i>Model:{{ $car['model'] }}</li>
                                       <li><i class="fa fa-cogs"></i>{{ $car['Engine_type'] }}</li>
                                       <li><i class="fa fa-dashboard"></i>{{ $car['engine_power'] }}</li>
                                    </ul>
                                    <div class="offer-action">
                                       <a href="tel:+03004629435" class="offer-btn-1">Rent Car</a>
                                       <a href="{{route('vehicle.detail',$car['slug'])}}" class="offer-btn-2">Details</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                     <!-- Toyota Tab Start -->

                     <!-- Audi Tab Start -->
                     <div class="tab-pane fade" id="Honda" role="tabpanel" aria-labelledby="Audi-tab">
                        <div class="row">
                           @foreach($suv_cars as $car)
                           <div class="col-lg-4">
                              <div class="single-offers">
                                 <div class="offer-image">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <img src="{{ URL::to($car->car_img1) }}" alt="offer 1" />
                                    </a>
                                 </div>
                                 <div class="offer-text">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <h3>{{ $car['name'] }}</h3>
                                    </a>
                                    <!-- <h4>$50.00<span>/ Day</span></h4> -->
                                    <ul>
                                       <li><i class="fa fa-car"></i>Model:{{ $car['model'] }}</li>
                                       <li><i class="fa fa-cogs"></i>{{ $car['Engine_type'] }}</li>
                                       <li><i class="fa fa-dashboard"></i>{{ $car['engine_power'] }}</li>
                                    </ul>
                                    <div class="offer-action">
                                       <a href="tel:+03004629435" class="offer-btn-1">Rent Car</a>
                                       <a href="{{route('vehicle.detail',$car['slug'])}}" class="offer-btn-2">Details</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                     <!-- Audi Tab End -->

                     <!-- Marcedes Tab Start -->

                     <!-- Marcedes Tab End -->

                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>




   <!-- Service Area Start -->
   <section class="gauto-service-area section_70">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="site-heading">
                  <h4>see our</h4>
                  <h2>Latest Services</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="service-slider owl-carousel">
                  <div class="single-service">
                     <span class="service-number">01 </span>
                     <div class="service-icon">
                        <!-- <img src="assets/img/city-transport.png" alt="city trasport" /> -->
                     </div>
                     <div class="service-text">
                        <a href="#">
                           <h3>Daily Rent</h3>
                        </a>

                     </div>
                  </div>
                  <div class="single-service">
                     <span class="service-number">02 </span>
                     <div class="service-icon">
                        <!-- <img src="assets/img/airport-transport.png" alt="airport trasport" /> -->
                     </div>
                     <div class="service-text">
                        <a href="#">
                           <h3>Weekly Rent</h3>
                        </a>

                     </div>
                  </div>

                  <div class="single-service">
                     <span class="service-number">03</span>
                     <div class="service-icon">
                        <!-- <img src="assets/img/wedding-ceremony.png" alt="wedding trasport" /> -->
                     </div>
                     <div class="service-text">
                        <a href="#">
                           <h3>Monthly Rent</h3>
                        </a>

                     </div>
                  </div>
                  <div class="single-service">
                     <span class="service-number">04 </span>
                     <div class="service-icon">
                        <!-- <img src="assets/img/hotel-transport.png" alt="wedding trasport" /> -->
                     </div>
                     <div class="service-text">
                        <a href="#">
                           <h3>Wedding Cermony</h3>
                        </a>

                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- Service Area End -->




   <!-- Driver Area Start -->
   <section class="gauto-driver-area section_70">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="site-heading">

                  <h2>our Members</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-3 col-sm-6">
               <div class="single-driver">
                  <!-- <div class="driver-image">
                     <img src="/assets/img/member3.jpeg" alt="driver 1" />
                     <div class="hover">

                     </div>
                  </div> -->
                  <div class="driver-text">
                     <div class="driver-name">
                        <a href="#">
                           <h3> CEO</h3>
                           <h4> Haji Idrees</h4>
                           <p style="font-weight: bold">Cell No:+923004629435</p>
                        </a>

                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-sm-6">
               <div class="single-driver">
                  <!-- <div class="driver-image">
                     <img src="/assets/img/member2.jpeg" alt="driver 1" />
                     <div class="hover">

                     </div>
                  </div> -->
                  <div class="driver-text">
                     <div class="driver-name">
                        <a href="#">
                           <h3> </h3>
                           <h4>Umair Idrees</h4>
                           <p style="font-weight: bold">Cell No:+923354512837</p>

                        </a>

                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-sm-6">
               <div class="single-driver">
                  <!-- <div class="driver-image">
                     <img src="/assets/img/member1.jpeg" alt="driver 1" />
                     <div class="hover">

                     </div>
                  </div> -->
                  <div class="driver-text">
                     <div class="driver-name">
                        <a href="#">
                           <h3> </h3>
                           <h4>CH Ahmed Jutt</h4>
                           <p style="font-weight: bold">Cell No:+923034333463</p>
                        </a>

                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-sm-6">
               <div class="single-driver">
                  <!-- <div class="driver-image">
                     <img src="/assets/img/member4.jpeg" alt="driver 1" />
                     <div class="hover">

                     </div>
                  </div> -->
                  <div class="driver-text">
                     <div class="driver-name">
                        <a href="#">
                           <h3> </h3>
                           <h4>Sajid juttt</h4>
                           <p style="font-weight: bold">Cell No:+923219496996</p>
                        </a>

                     </div>
                  </div>
               </div>
            </div>


         </div>

      </div>
   </section>
   <!-- Driver Area End -->





   <!-- Blog Area Start -->
   <section class="gauto-blog-area section_70">
      <div class="container">
      <div class="row">
            <div class="col-md-12">
               <div class="site-heading">
                  <h2>Office Location</h2>
                
               </div>
            </div>
         </div>
         <div class="row mt-2 ">
            <div class="col-md-12">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3401.372882960892!2d74.25804401514414!3d31.51391708137171!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39190370c2e047fb%3A0x8331dba146403cc5!2sBest%20Rent%20A%20Car!5e0!3m2!1sen!2s!4v1583078285602!5m2!1sen!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
         </div>

      </div>
   </section>
   <section class="gauto-blog-area section_70">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="site-heading">
                  <h4>latest</h4>
                  <h2>our blog</h2>
               </div>
            </div>
         </div>
         <div class="row">
            @foreach($blogs as $blog)
            <div class="col-lg-4">
               <div class="single-blog">
                  <div class="blog-image">
                     <a href="#">
                        <img src="{{URL::to($blog['image'])}}" alt="blog 1" />
                     </a>
                  </div>
                  <div class="blog-text">
                     <h3><a href="#">{{ $blog['name'] }}.</a></h3>
                     <div class="blog-meta-home">
                        <div class="blog-meta-left">
                           <p>{{ $blog->created_at->toFormattedDateString() }}</p>
                        </div>
                        <div class="blog-meta-right">
                           <p><i class="fa fa-eye"></i> 322</p>
                           <p><i class="fa fa-commenting"></i> 67</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endforeach

         </div>
      </div>
   </section>
   <!-- Blog Area End -->


   @include('rent.layout.footer')
</body>

</html>