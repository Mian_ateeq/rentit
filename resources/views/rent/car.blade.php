@extends('rent.master')
@section('tittle','Vehicle')

@section('content')
    <style>
        .pagination>li>a, .pagination>li>span { border-radius: 50% !important;margin: 0 5px;
            
            /* border: 3px solid; */
            color: #ffff;
            display: block;
            /* font-size: 18px; */
            /* font-weight: 700; */
            line-height: 36px;
            height: 52px;
            width: 50px;
        }
        .pagination {
            display: -ms-flexbox;
            display: -webkit-inline-box;
            padding-left: 0;
            list-style: none;
            border-radius: .25rem;
        }
        
        .pagination-contents{
            
            text-align: center;
            position: relative;
            padding: 80px 0;
        }
        
        .pagination-contents:after, .pagination-content:before {
            background-color: #f4f4f4;
            content: '';
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
        }
        .pagination-contents:before {
            right: 100%;
        }
        .pagination-contents:after {
            left: 100%;
        }
        .pagination-contents:after, .pagination-contents:before {
            background-color: #f4f4f4;
            content: '';
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
        }
    </style>
     <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Car </h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Car </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section><!-- breadcrumb area end -->
    <!-- blog section start -->
    <section class="gauto-car-listing section_70">
         <div class="container">
            <div class="row">
           
               <div class="col-lg-12">
                  <div class="car-listing-right">
                   
                     <div class="car-grid-list">
                        <div class="row">
                            @foreach($cars as $car)
                           <div class="col-md-4">
                              <div class="single-offers">
                                 <div class="offer-image">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                    <img src="{{ URL::to($car->car_img1) }}" alt="offer 1" />
                                    </a>
                                 </div>
                                 <div class="offer-text">
                                    <a href="{{route('vehicle.detail',$car['slug'])}}">
                                       <h3>{{ $car['name'] }}</h3>
                                    </a>
                                    
                                    <ul>
                                       <li><i class="fa fa-car"></i>Model:{{ $car['model'] }}</li>
                                       <li><i class="fa fa-cogs"></i>{{ $car['Engine_type'] }}</li>
                                       <li><i class="fa fa-dashboard"></i>{{ $car['engine_power'] }}</li>
                                    </ul>
                                    <div class="offer-action">
                                       <a href="{{route('vehicle.detail',$car['slug'])}}" class="offer-btn-1">Rent Car</a>
                                       <a href="{{route('vehicle.detail',$car['slug'])}}" class="offer-btn-2">Details</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                           
                        </div>
                       
                     </div>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
    @stop