<!DOCTYPE HTML>
<html lang="zxx">

@include('rent.layout.head')
<body>

@include('rent.layout.header')

@yield('content')
@include('rent.layout.footer')

</body>

</html>