@extends('rent.master')
@section('tittle','Confirm Booking')
@section('content')
    
    
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Confirm Booking</h2>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                <div id="thank">
                    {{--<div class="inner_page col-lg-12 col-xs-12"><h1 id="msgs" style="font-size: 65px;">Thank You</h1>--}}
                    {{--<div class="noticeboard" style="min-height:330px;"><p--}}
                    {{--style="padding:20px 20px 0; font-size:16px; ">Dear Ateeq, Assalam o Alaikum<br>--}}
                    {{--Thank You Very Much for Your booking request we will get back to you shortly. For urgent--}}
                    {{--questions call--}}
                    {{--<!--<a href="tel:+923008557698" ><span style="padding:20px 20px 0; font-size:16px; ">-->--}}
                    {{--Call Us:92 300 855 7698<!--</span></a>--><br> Regards,<br> Booking Team<br> Pakistan Car--}}
                    {{--Rentals<br> bookings@rentcars247.com</p>--}}
                    {{--<div class="notice_p">&nbsp;</div><!--notice p--> </div>--}}
                    {{--</div>--}}
                    <style>
                        #gallery{
                            max-width: 100%!important;
                        }
                    </style>
    
               
                    <div class="inner_bg221">
                        <div class="container">
                            <div class="inner_page col-lg-12 col-xs-12" style="box-shadow:none !important;"><h1
                                        id="msgs" style="font-size: 65px;"></h1>
                                <div class="noticeboard"
                                     style="width:93%; float:left; margin-top:0px !important; box-shadow:none !important; border:none !important;">
                                    @if($diff_in_days==0)
                                    <p style="padding:1px 30px 0; font-size:30px; ">Rs. {{$vehicle['per_day_charge'] *1}} For {{1}} Days
                                    @else
                                    <p style="padding:1px 30px 0; font-size:30px; ">Rs. {{$vehicle['per_day_charge'] *$diff_in_days}} For {{$diff_in_days}} Days
                                        @endif
                                    @if($driver==1)
                                            With driver
                                        @else
                                        Without Driver
                                        @endif
                                    </p>
                                    <div class="notice_p col-lg-7 col-xs-11">
    
                                        <div id="gallery">
        
                                            <img alt="Preview Image 1"
                                                 src="{{URL::to($vehicle->car_img1)}}"
                                                 data-image="{{URL::to($vehicle->car_img1)}}"
                                                 data-description="Preview Image 1 Description"> <img alt="Preview Image 1"
                                                 src="{{URL::to($vehicle->car_img2)}}"
                                                 data-image="{{URL::to($vehicle->car_img2)}}"
                                                 data-description="Preview Image 1 Description"> <img alt="Preview Image 1"
                                                 src="{{URL::to($vehicle->car_img3)}}"
                                                 data-image="{{URL::to($vehicle->car_img3)}}"
                                                 data-description="Preview Image 1 Description"> <img alt="Preview Image 1"
                                                 src="{{URL::to($vehicle->car_img4)}}"
                                                 data-image="{{URL::to($vehicle->car_img4)}}"
                                                 data-description="Preview Image 1 Description">
    
    
    
                                        </div>
                                    </div>
                                    <div class="notice_p col-lg-4 col-xs-11">
                                        <div class="quoteme_now1">
                                            <form action="{{route('booking_confirm')}}"
                                                  method="post">
                                                @csrf
                                                <input type="submit" value="Book Now" name="btnBooking"
                                                                       class="quoteme_now1" style="cursor: pointer"></form>
                                        </div>
                                        <div style="width:80%; float:left;"><p><strong>Date Out: </strong></p>
                                            <p>&nbsp;{{\Carbon\Carbon::parse($data['day_oute'])->toFormattedDateString()}} ({{$Day}})</p></div>
                                        <div style="width:80%; float:left;"><p><strong>Return Date: </strong></p>
                                            <p>&nbsp;{{\Carbon\Carbon::parse($data['day_return'])->toFormattedDateString()}} ({{$Day2}})</p></div>
                                        <div style="width:80%; float:left;"><p><strong>Car : </strong></p>
                                            <p>&nbsp;{{ucfirst($vehicle['name'])}}</p></div>
                                        <div style="width:80%; float:left;"><p><strong>No of Days : </strong></p>
                                            @if($diff_in_days==0)
                                            <p>&nbsp;{{1}} Days</p></div>
                                        @else
                                            <p>&nbsp;{{$diff_in_days}} Days</p></div>
                                        @endif
                                        <div style="width:80%; float:left;"><p><strong>Per Days Rent : </strong></p>
                                            <p>&nbsp; RS. {{number_format($vehicle['per_day_charge'],2)}}</p></div>
                                        <div style="width:80%; float:left;"><p><strong>Total Rent : </strong>&nbsp;</p>
                                            @if($diff_in_days==0)
                                            <p>RS. {{number_format($vehicle['per_day_charge'] *1)}} For {{1}}</p>
                                            @else
                                            <p>RS. {{number_format($vehicle['per_day_charge'] *$diff_in_days)}} For {{$diff_in_days}}</p>
                                                
                                                @endif
                                        
                                        </div>
                                    </div><!--notice p--> </div>
                            </div><!--container--> </div>
                       
                    </div>
                    
                </div>
            </div>
        
        </div>
    </div>
@stop