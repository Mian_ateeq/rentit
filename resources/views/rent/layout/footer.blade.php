     <!-- Footer Area Start -->
     <footer class="gauto-footer-area">
         <div class="footer-top-area">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4">
                     <div class="single-footer">
                        <div class="footer-logo">
                           <a href="{{url('/')}}">
                           <img src="{{ URL::to($site['logo']) }}" alt="gauto" />
                           </a>
                        </div>
                        <div class="footer-address">
                           <h3>Head office</h3>
                           <ul>
                              <!-- <li>Phone: 326487652 </li>
                              <li>Email: example@mail.com</li>
                              <li>Office Time: 9AM- 4PM</li> -->
                              <li>Office #14 Dubai Plaza Liaqat Chowk  ،</li>
                              <li>Lahore Pakistan</li>
                              <li>Cell No 1:+923004629435</li>
                              <li>Cell No 2:+923219403035</li>
                              <li>Cell No 3:+923354512837</li>
                              <li>Cell No 4:+923214399994</li>
                              <li>Cell No 5:+923034333463</li>
                             
                            
                              
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="single-footer quick_links">
                        <h3>Quick Links</h3>
                        <ul class="quick-links">
                           <li><a href="{{route('about')}}">About us</a></li>
               
                           <li><a href="#">Cars</a></li>
                           <li><a href="{{route('contact.us')}}">Contact us</a></li>
                        </ul>
                       
                     </div>
                     <div class="single-footer newsletter_box">
                        <h3>newsletter</h3>
                        <form>
                           <input type="email" placeholder="Email Address" />
                           <button type="submit"><i class="fa fa-paper-plane"></i></button>
                        </form>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="single-footer">
                        <h3>Recent post</h3>
                        <ul>
                          @foreach($blogs as $blog)
                          <li>
                              <div class="single-footer-post">
                                 <div class="footer-post-image">
                                    <a href="#">
                                    <img src="{{URL::to($blog['image'])}}" alt="footer post" />
                                    </a>
                                 </div>
                                 <div class="footer-post-text">
                                    <h3>
                                       <a href="#">
                                     {{$blog['name']}}
                                       </a>
                                    </h3>
                                    <p>Posted on: {{ $blog->created_at->toFormattedDateString() }}</p>
                                 </div>
                              </div>
                           </li>
                          @endforeach
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer-bottom-area">
            <div class="container">
               <div class="row">
                  <div class="col-md-6">
                     <div class="copyright">
                        <p>Design With <i class="fa fa-heart"></i> from <a href="#">Themescare</a></p>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="footer-social">
                        <ul>
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                           <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                           <li><a href="#"><i class="fa fa-skype"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- Footer Area End -->
       

      <!--Jquery js-->
      <script src="{{ URL::to('assets/js/jquery.min.js') }}"></script>

      <!-- Popper JS -->
      <script src="{{URL::to('assets/js/popper.min.js')}}"></script>
      <!--Bootstrap js-->
      <script src="{{URL::to('assets/js/bootstrap.min.js')}}"></script>
      <!--Owl-Carousel js-->
      <script src="{{URL::to('assets/js/owl.carousel.min.js')}}"></script>
      <!--Lightgallery js-->
      <script src="{{URL::to('assets/js/lightgallery-all.js')}}"></script>
      <script src="{{URL::to('assets/js/custom_lightgallery.js')}}"></script>
      <!--Slicknav js-->
      <script src="{{URL::to('assets/js/jquery.slicknav.min.js')}}"></script>
      <!--Magnific js-->
      <script src="{{URL::to('assets/js/jquery.magnific-popup.min.js')}}"></script>
      <!--Nice Select js-->
      <script src="{{URL::to('assets/js/jquery.nice-select.min.js')}}"></script>
      <!-- Datepicker JS -->
      <script src="{{URL::to('assets/js/jquery.datepicker.min.js')}}"></script>
      <!--ClockPicker JS-->
      <script src="{{URL::to('assets/js/jquery-clockpicker.min.js')}}"></script>
      <!--Main js-->
      <script src="{{URL::to('assets/js/main.js')}}"></script>
      <script type='text/javascript' src='{{URL::to("unitegallery/js/unitegallery.min.js")}}'></script>	
	
   <script type='text/javascript' src='{{URL::to("unitegallery/themes/slider/ug-theme-slider.js")}}'></script>


      <script type="text/javascript">



   jQuery("#gallery1").unitegallery({
				gallery_theme: "slider",
            slider_control_zoom: false
			});



</script>