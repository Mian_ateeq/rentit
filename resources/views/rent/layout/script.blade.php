<!-- jquery main JS -->
<script src="{{URL::to('assets/js/jquery.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{URL::to('assets/js/bootstrap.min.js')}}"></script>
<!-- Slicknav JS -->
<script src="{{URL::to('assets/js/jquery.slicknav.min.js')}}"></script>
<!-- owl carousel JS -->
<script src="{{URL::to('assets/js/owl.carousel.min.js')}}"></script>
<!-- Popup JS -->
<script src="{{URL::to('assets/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Counterup JS -->
<script src="{{URL::to('assets/js/jquery.counterup.min.js')}}"></script>
<!-- Counterup waypoints JS -->
<script src="{{URL::to('assets/js/waypoints.min.js')}}"></script>
<!-- Isotope JS -->
<script src="{{URL::to('assets/js/isotope.pkgd.min.js')}}"></script>
<!-- Vega JS -->
<script src="{{URL::to('assets/js/vegas.min.js')}}"></script>
<!-- main JS -->
<script src="{{URL::to('assets/js/main.js')}}"></script>
<script type='text/javascript' src="{{URL::to('assets/unitegallery/js/unitegallery.min.js')}}"></script>

<script type='text/javascript' src="{{URL::to('assets/unitegallery/themes/slider/ug-theme-slider.js')}}"></script>
{{--<script type='text/javascript' src="{{URL::to('js/jquery.inputmask.bundle.js')}}"></script>--}}
<script src="{{URL::to('js/jquery.inputmask.bundle.min.js')}}"
        type="text/javascript"></script>
<script src="{{URL::to('js/form-inputmask.js')}}" type="text/javascript"></script>

<script>
	


</script>

<script>
    function initMap() {
        var uluru = {lat: 31.475666, lng: 74.282361};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }

    $('#booking_btn').click(function () {
	
	    document.getElementById("booking").submit();
    });
</script>
{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnKyOpsNq-vWYtrwayN3BkF3b4k3O9A_A&amp;callback=initMap"></script> --}}
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHTzipdXC0jU1q1s6JOEJRTlEo97Q47wg&callback=initMap"
type="text/javascript"></script>
<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('.car_tpe').change(function(){
        var car_tpe = $(this).val();

        if(car_tpe){
            $.ajax({
                url: "{{route('car_type')}}",
                type: 'post',
                data: {
                    _token: CSRF_TOKEN,
                    car_tpe:car_tpe
                },
                dataType: 'JSON',
                success: function (data) {

                    if(data){
                        $(".car_id").empty();
                        $.each(data,function(key,value){
                            $(".car_id").append('<option value="'+value.id+'">'+value.name+'</option>');
                        });

                    }else{
                        $(".car_id").empty();
                    }













                }
            });
        }else{
            $("#state").empty();
            $("#city").empty();
        }
    });
</script>
<script type="text/javascript">
	
	jQuery(document).ready(function(){
		
		jQuery("#gallery").unitegallery({
			gallery_mousewheel_role: "none"
		});
		
	});

</script>
<script>
	$(document).ready(function() {
		/*----------------------------
		START - Vega slider
		------------------------------ */
		$("#slideslow-bg").vegas({
			overlay: true,
			autoHeight: true,
			transition: 'fade',
			transitionDuration: 2000,
			delay: 4000,
			color: '#000',
			animation: 'random',
			animationDuration: 20000,
			slides: [
					@foreach($sliders as $slider)
				{
					src: '{{$slider->image}}'
				},
				@endforeach
			
			]
		});
	});
</script>

<script src="{{URL::to('js/date.js')}}"></script>
<script>
	$('.international-inputmask').inputmask("(9999)999-9999");
	$('.cc-inputmask').inputmask("99999 9999- 9999");
	
	// $(".selector").flatpickr();
	
	$(function () {
		'use strict';
		
		var $date = $('.docs-date');
		var $container = $('.docs-datepicker-container');
		var $trigger = $('.docs-datepicker-trigger');
		var options = {
			show: function (e) {
				console.log(e.type, e.namespace);
			},
			hide: function (e) {
				console.log(e.type, e.namespace);
			},
			pick: function (e) {
				console.log(e.type, e.namespace, e.view);
			}
		};
		
		$date.on({
			'show.datepicker': function (e) {
				console.log(e.type, e.namespace);
			},
			'hide.datepicker': function (e) {
				console.log(e.type, e.namespace);
			},
			'pick.datepicker': function (e) {
				console.log(e.type, e.namespace, e.view);
			}
		}).datepicker(options);
		
		$('.docs-options, .docs-toggles').on('change', function (e) {
			var target = e.target;
			var $target = $(target);
			var name = $target.attr('name');
			var value = target.type === 'checkbox' ? target.checked : $target.val();
			var $optionContainer;
			
			switch (name) {
				case 'container':
					if (value) {
						value = $container;
						$container.show();
					} else {
						$container.hide();
					}
					
					break;
				
				case 'trigger':
					if (value) {
						value = $trigger;
						$trigger.prop('disabled', false);
					} else {
						$trigger.prop('disabled', true);
					}
					
					break;
				
				case 'inline':
					$optionContainer = $('input[name="container"]');
					
					if (!$optionContainer.prop('checked')) {
						$optionContainer.click();
					}
					
					break;
				
				case 'language':
					$('input[name="format"]').val($.fn.datepicker.languages[value].format);
					break;
			}
			
			options[name] = value;
			$date.datepicker('reset').datepicker('destroy').datepicker(options);
		});
		
		$('.docs-actions').on('click', 'button', function (e) {
			var data = $(this).data();
			var args = data.arguments || [];
			var result;
			
			e.stopPropagation();
			
			if (data.method) {
				if (data.source) {
					$date.datepicker(data.method, $(data.source).val());
				} else {
					result = $date.datepicker(data.method, args[0], args[1], args[2]);
					
					if (result && data.target) {
						$(data.target).val(result);
					}
				}
			}
		});
		
		var dateToday = new Date();
		$('[data-toggle="datepicker"]').datepicker({
			autoclose: true,
			todayHighlight: true,
			startDate: new Date()
		});
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	
	
	
</script>

