<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="Best Rent A Car">
      <meta name="keyword" content="taxi,car,rent,hire,transport">
      <meta name="author" content="Themescare">
      <!-- Title -->
      @if(Route::current()->getName() == 'home')
        <title>Home</title>
    
    @else
        <title>@yield('tittle')</title>
    
    @endif
      <!-- Favicon -->
      <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
      <!--Bootstrap css-->
      <link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.css') }}">
      <!--Font Awesome css-->
      <link rel="stylesheet" href="{{ URL::to('assets/css/font-awesome.min.css') }}">
      <!--Magnific css-->
      <link rel="stylesheet" href="{{ URL::to('assets/css/magnific-popup.css') }}">
      <!--Owl-Carousel css-->
      <link rel="stylesheet" href="{{URL::to('assets/css/owl.carousel.min.css')}}">
      <link rel="stylesheet" href="{{URL::to('assets/css/owl.theme.default.min.css')}}">
      <!--Animate css-->
      <link rel="stylesheet" href="{{URL::to('assets/css/animate.min.css')}}">
      <!--Datepicker css-->
      <link rel="stylesheet" href="{{URL::to('assets/css/jquery.datepicker.css')}}">
      <!--Nice Select css-->
      <link rel="stylesheet" href="{{URL::to('assets/css/nice-select.css')}}">
      <!-- Lightgallery css -->
      <link rel="stylesheet" href="{{URL::to('assets/css/lightgallery.min.css')}}">
      <!--ClockPicker css-->
      <link rel="stylesheet" href="{{URL::to('assets/css/jquery-clockpicker.min.css')}}">
      <!--Slicknav css-->
      <link rel="stylesheet" href="{{URL::to('assets/css/slicknav.min.css')}}">
      <!--Site Main Style css-->
      <link rel="stylesheet" href="{{ URL::to('assets/css/style.css') }}">
      <!--Responsive css-->
      <link rel="stylesheet" href="{{ URL::to('assets/css/responsive.css') }}">
    	

      <link rel='stylesheet' href='{{URL::to("unitegallery/css/unite-gallery.css")}}' type='text/css' />
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js' type='text/css' />
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
   #gallery1{
                            max-width: 100%!important;
                        }
</style>
   </head>