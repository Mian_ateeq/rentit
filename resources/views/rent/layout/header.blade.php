<header class="gauto-main-header-area">
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <div class="site-logo">
                     <a href="{{ url('/') }}">
                     <img src="{{ URL::to($site['logo']) }}" alt="gauto" />
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-9">
                  <div class="header-promo">
                     <div class="single-header-promo">
                        <div class="header-promo-icon">
                           <img src="{{ URL::to('assets/img/globe.png') }}" alt="globe" />
                        </div>
                        <div class="header-promo-info">
                           <h3>Lahore, Pakistan</h3>
                          
                        </div>
                     </div>
                     <div class="single-header-promo">
                        <div class="header-promo-icon">
                           <img src="{{URL::to('assets/img/clock.png')}}" alt="clock" />
                        </div>
                        <div class="header-promo-info">
                           <h3>Monday to Saturday</h3>
                           <p>9:00am - 9:00pm</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3">
                  <div class="header-action">
                     <a href="tel:+03004629435"><i class="fa fa-phone"></i> Request a call</a>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <section class="gauto-mainmenu-area">
         <div class="container">
            <div class="row">
               <div class="col-lg-9">
                  <div class="mainmenu">
                     <nav>
                        <ul id="gauto_navigation">
                           <li class="{{ Request::path() == '/' ? 'active' : '' }}"><a href="{{url('/')}}">home</a></li>
                           <li class="{{ Route::current()->getName() == 'vehicle' ? 'active' : '' }}"><a href="{{route('vehicle')}}">Cars</a></li>
                           <li class="{{ Route::current()->getName() == 'about' ? 'active' : '' }}"><a  href="{{ route('about') }}">about</a></li>
                           <li class="{{ Route::current()->getName() == 'contact.us' ? 'active' : '' }}"><a  href="{{ route('contact.us') }}">contact</a></li>
                           <li class="{{ Route::current()->getName() == 'gallery' ? 'active' : '' }}"><a  href="{{ route('gallery') }}">Gallery</a></li>

                        </ul>
                     </nav>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-12">
                  <div class="main-search-right">
                     <!-- Responsive Menu Start -->
                     <div class="gauto-responsive-menu"></div>
                     <!-- Responsive Menu Start -->
                      
                     <!-- Cart Box Start -->
                     
                     <!-- Search Box End -->
                      
                  </div>
               </div>
            </div>
         </div>
      </section>