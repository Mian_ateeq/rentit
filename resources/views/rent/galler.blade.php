@extends('rent.master')
@section('tittle','About US')

@section('content')
<section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Gallery</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Gallery</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
  
      <!-- Breadcromb Area End -->
       
       
      <!-- Gallery Area Start -->
      <section class="gauto-gallery-area section_70">
         <div class="container">
            <div class="row" id="lightgallery">
             @foreach($cars as $car)
             <div class="col-lg-4" data-src="{{ URL::to($car->car_img1) }}">
                  <div class="single-gallery">
                     <div class="img-holder">
                        <img src="{{ URL::to($car->car_img1) }}" alt="gallery 1" />
                        <div class="overlay-content">
                           <div class="inner-content">
                              <div class="title-box">
                                 <h3><a href="{{route('vehicle.detail',$car['slug'])}}">{{ $car['name'] }}</a></h3>
                              </div>
                           </div>
                        </div>
                        <div class="link-zoom-button">
                           <div class="single-button">
                              <a href="{{route('vehicle.detail',$car['slug'])}}"><span class="fa fa-link"></span>Details</a>
                           </div>
                           <div class="single-button">
                              <a class="zoom lightbox-image" href="{{ URL::to($car->car_img1) }}">
                              <span class="fa fa-search"></span>Zoom
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
             @endforeach
               
            </div>
            
         </div>
      </section>


@stop