<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','RentController@index')->name('home');
Route::get('/vehicle/view','RentController@vehicle')->name('vehicle');
Route::get('/gallery/view','RentController@gallery')->name('gallery');
Route::get('/cars/{slug}','RentController@vehicleDetail')->name('vehicle.detail');
Route::get('/contact/us','ContactController@index')->name('contact.us');
Route::post('/contact/us','ContactController@store')->name('contact.us.submit');


Route::get('/about/us','ContactController@about')->name('about');
Route::get('/service','ContactController@service')->name('service');

Route::post('/car/type','RentController@car_type')->name('car_type');
Route::get('/post','BlogController@index')->name('blog.index');
Route::get('/post/{slug}','BlogController@blog')->name('blog.view');







//Auth::routes();


Route::get('admin/login','Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login','Auth\LoginController@login')->name('admin.login.submit');
Route::post('admin/logout','Auth\LoginController@logout')->name('logout');

Route::get('/admin', 'HomeController@index')->name('admin.dashboard');


Route::get('/test',function (){
    
    
    return view('rent.master');
});




Route::post('/confirm/booking','BookingController@booking')->name('booking');
Route::post('/booking','BookingController@booking_confirm')->name('booking_confirm');
Route::get('/booking','BookingController@carbooking')->name('carbooking');



