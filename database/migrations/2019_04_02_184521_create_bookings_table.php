<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone_no');
            $table->string('cnic');
            $table->enum('driver',['Yes','No'])->default('No');
            $table->string('city');
            $table->integer('vehicle_type');
            $table->integer('vehicle_model');
            $table->string('day_out');
            $table->string('time_out')->nullable();
            $table->string('day_return');
            $table->string('time_return')->nullable();
            $table->enum('status',['Accepted','Rejected','Pending'])->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
