<?php

use Illuminate\Database\Seeder;
    use Spatie\Permission\Models\Permission;
    use Spatie\Permission\Models\Role;
    
    class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'blog',
            'booking',
            'vehicle',
            'site_config',
            'client',
            'admin'
    
        ];
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission,
                'guard_name'=>'web']);
        }
        $role = Role::create(['name' => 'super-admin',
            'guard_name'=>'web']);
        $role->givePermissionTo(Permission::all());
        $vendor=\App\User::find(1);
    
        $role_r = Role::where('id', '=', $role->id)->firstOrFail();
        $vendor->assignRole($role_r);
    }
}
