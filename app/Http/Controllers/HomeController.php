<?php

namespace App\Http\Controllers;

use App\Booking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Bookinng\Entities\Client;
use Spatie\Analytics\Period;
use Analytics;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//         $order_item=Booking::where('status','Accepted')->get()->groupBy(function($date) {
//             return Carbon::parse($date->updated_at)->format('Y-m-d');
        
//         });
//         $sum=array();
    
//         foreach ($order_item as $key => $value) {
//             $s = 0;

// //            return $value;
//             foreach ($value as $val) {
    
    
//                 $sum[$key]=$s+=$val['car']->per_day_charge;
        
//             }
//         }
        
        
        
    
//         $page_vist = Analytics::fetchTotalVisitorsAndPageViews(Period::days(7));
//         $pages = Analytics::fetchVisitorsAndPageViews(Period::days(7));
//         $browser = Analytics::fetchTopBrowsers(Period::days(25), 1000);
//         $analyticsTopCountry = Analytics::performQuery(
//             Period::years(1),
//             'ga:sessions',
//             [
//                 'metrics' => 'ga:sessions',
//                 'dimensions' => 'ga:month, ga:country'
//             ]
//         );
    
//         $analyticsCountry = $analyticsTopCountry->rows;
//         $analyticsDevices = Analytics::performQuery(
//             Period::months(1),
//             'ga:sessions',
//             [
//                 'metrics' => 'ga:sessions',
//                 'dimensions' => 'ga:month, ga:deviceCategory'
//             ]
//         );
//       $devices=  $analyticsDevices->rows;
//         $pending=Booking::where('status','Pending')->get();
//         $accepted=Booking::where('status','Accepted')->get();
//         $rejected=Booking::where('status','Rejected')->get();
//         $client=Client::all();
        
    
        return view('admin.dashboard');
    }



}
