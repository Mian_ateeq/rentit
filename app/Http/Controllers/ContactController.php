<?php

namespace App\Http\Controllers;

//use http\Message;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    public function index(){


        return view('rent.c_us');
    }


    public function store(Request $request){

//        return $request->all();
        $request->validate([
            'name'=>'required',
            'lastname'=>'required',
            'email'=>'required',
            'subject'=>'required',
            'message'=>'required',

        ]);

        $message=new Message();
        $message['name']=$request['name'];
        $message['lastname']=$request['lastname'];
        $message['email']=$request['email'];
        $message['subject']=$request['subject'];
        $message['message']=$request['message'];

        $message->save();
        session::flash('success', '');

        return redirect()->back();
    }


    public function about(){


        return view('rent.about');
    }
    public function service(){


        return view('rent.service');
    }
}
