<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Blog\Entities\Blog;
use Modules\Car\Entities\Car;
use Modules\Car\Entities\CarType;
use Modules\SiteConfigutaion\Entities\Slider;

class RentController extends Controller
{
    //

    public function index(){

        $sliders=Slider::all();

        $cars=Car::all();
        $car_type=CarType::all();



        $limos_cars =  Car::whereHas('type', function ($query) {
            $query->where('name', 'Toyota');
        })->get();
          $sedan_cars = Car::whereHas('type' , function ($query) {
            $query->where('name', 'Suzuki');
        })->get();

        $suv_cars = Car::whereHas('type' , function ($query) {
            $query->where('name', 'Honda');
        })->get();

        $blogs=Blog::limit(3)->get();

        return view('rent.index',compact('cars','car_type','limos_cars','sedan_cars','suv_cars','sliders','blogs'));
    }


    public function vehicle(){


        $cars=Car::OrderBy('id','desc')->paginate(10);



        return view('rent.car',compact('cars'));
    }
    public function gallery(){


        $cars=Car::OrderBy('id','desc')->paginate(10);



        return view('rent.galler',compact('cars'));
    }


    public function car_type(Request $request){



       $car=Car::where('vehicle_type',$request->car_tpe)->where('status',1)->get();

       return $car;
    }

    public function vehicleDetail($slug){
        $car=Car::where('slug',$slug)->first();



        return view('rent.carDetail',compact('car'));

    }
}
