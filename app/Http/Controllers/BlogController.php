<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Blog\Entities\Blog;

class BlogController extends Controller
{
    //
    
    
    public function index(){
        
        $blogs=Blog::OrderBy('id','desc')->paginate(10);
        
        return view('rent.blog',compact('blogs'));
    }
    
    
    public function blog($slug){
        
        
        $blog=Blog::where('slug',$slug)->first();
        
        
        return view('rent.blog_detail',compact('blog'));
    }
}
