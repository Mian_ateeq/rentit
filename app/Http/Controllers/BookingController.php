<?php

namespace App\Http\Controllers;

use App\Booking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Modules\Car\Entities\Car;
use Modules\Car\Entities\CarType;

class BookingController extends Controller
{
    //
    
    
    public function booking(Request $request){
    
//        return $request->all();
        
        if($request->driver){
            if($request->driver=="Yes"){
                $driver=1;
            }
            else{
                $driver=0;
            }
            
        }
        else{
            $driver=0;
        }
    
        $a = str_replace('/','-',$request->day_oute);
        $with =str_replace('/','-',$request->day_return);
        $to = \Carbon\Carbon::createFromFormat('m-d-Y', $with);
        $from = \Carbon\Carbon::createFromFormat('m-d-Y', $a);
         $diff_in_days = $to->diffInDays($from);
        
        session()->put('name',$request->name);
        session()->put('email',$request->email);
        session()->put('phone',$request->phone);
        session()->put('cnic',$request->cnic);
        session()->put('city',$request->city);
        session()->put('vehicle_type',$request->vehicle_type);
        session()->put('vehicle_model',$request->vehicle_model);
        session()->put('day_oute',$request->day_oute);
        session()->put('day_return',$request->day_return);
        session()->put('time_out',$request->time_out);
        
        session()->put('driver',$request->driver);
        session()->put('time_return',$request->time_return);
        
        $data=$request->all();
    
        $date_out = $request->day_oute;
        $date_return = $request->day_return;
        $Day = date('D', strtotime($date_out));
        $Day2 = date('D', strtotime($date_return));
       
        
        
        
        $vehicle=Car::where('id',$request->vehicle_model)->first();
        
        
        
        return view('rent.confirm_booking',compact('data','vehicle','diff_in_days','Day','Day2','driver'));
        
    }
    
    
    public function booking_confirm(Request $request){
    
        
        $book=new Booking();
       $book['name']= session()->get('name');
        $book['email']= session()->get('email');
        $book['phone_no']= session()->get('phone');
    
        
        $string1 = str_replace('-', '', session()->get('cnic'));
        $cnic_orinal= preg_replace('/\s+/', '', $string1);
        $book['cnic']= $cnic_orinal;
        $book['driver']= session()->get('driver');
        $book['time_out']= session()->get('time_out');
        $book['time_return']= session()->get('time_return');
       $book['city']= session()->get('city');
        $book['vehicle_type']=session()->get('vehicle_type');
       $book['vehicle_model']= session()->get('vehicle_model');
       $book['day_out']= str_replace('/','-',session()->get('day_oute'));
       $book['day_return']=  str_replace('/','-',session()->get('day_return'));
        $book['time_out']=  session()->get('time_out',$request->time_out);
    
        if(session()->get('driver')){
    
            $book['driver']= session()->get('driver');
        }
        else{
    
            $book['driver']="No";
    
        }
        $book['time_return']=  session()->get('time_return');
       
       $book->save();
    
        Mail::send('mail',["book"=>$book] , function($message)  use($book){
            $message->subject('Booking');
        
            $message->from("areolite@gmail.com",'Areolite');
            $message->to($book->email);
        
        });
       
       
       
       
       return view('rent.thankyou',compact('book'));
        
    }
    
    
    public function carbooking(Request $request){
        
        $types=CarType::all();
        
        return view('rent.booking',compact('types'));
        
        
    }
}
