<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Analytics\Period;
use Analytics;

class TestController extends Controller
{
    public function test(){
//        return 1;
        $analyticsTopCountry = Analytics::performQuery(
            Period::years(1),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions',
                'dimensions' => 'ga:month, ga:country'
            ]
        );
    
        $analyticsCountry = $analyticsTopCountry->rows;
//        $analyticsData = Analytics::fetchTopBrowsers(Period::days(25), 1000);
//        dd( $analyticsCountry);
        $analyticsDevices = Analytics::performQuery(
            Period::months(1),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions',
                'dimensions' => 'ga:month, ga:deviceCategory'
            ]
        );
        
        dd($analyticsDevices['rows']);

        return view('test');
//        dd(config('laravel-analytics'));
    }
}
