<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Modules\Car\Entities\Car;

class Booking extends Model
{
    //
    
    protected $fillable=['status'];
    
    public function car(){
        return $this->belongsTo(Car::class,'vehicle_model','id');
    }
}
