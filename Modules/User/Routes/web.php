<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->group(function() {
//    Route::group(['middleware' => ['auth','permission:dummy 1']], function () {
    
    Route::group(['middleware' => ['permission:admin']], function () {
        
        Route::get('/', 'UserController@index')->name('user');
    Route::get('/create', 'UserController@create')->name('user.create');
    Route::post('/store', 'UserController@store')->name('user.store');
    Route::post('/update', 'UserController@update')->name('user.update');
    Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
//});
});
});


Route::prefix('permission')->group(function() {
    Route::group(['middleware' => ['permission:admin']], function () {
        
        
        Route::get('/index', 'PermissionController@index')->name('permission');
    Route::post('/save', 'PermissionController@store')->name('permission.save');
    Route::post('/update', 'PermissionController@update')->name('permission.update');
    Route::post('/delete', 'PermissionController@destroy')->name('permission.delete');
});
});

Route::prefix('role')->group(function() {
    Route::group(['middleware' => ['permission:admin']], function () {
        
        
        Route::get('/index', 'RoleController@index')->name('role');
    Route::get('/create', 'RoleController@create')->name('role.create');
    Route::get('/edit/{id}', 'RoleController@edit')->name('role.edit');
    Route::post('/save', 'RoleController@store')->name('role.save');
    Route::post('/update', 'RoleController@update')->name('role.update');
    Route::post('/delete', 'RoleController@destroy')->name('role.delete');
});
});