@extends('admin.master')
@section('page_name')
    Role
@stop

@section('header')
    <div class="row">
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Role</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($roles)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fa fa-user"></i>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-xl-2 "></div>
        <div class="col-xl-8 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Role</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{route('permission')}}" class="btn btn-sm btn-primary">View Permission</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('role.save')}}">
                        @csrf
                        <h6 class="heading-small text-muted mb-4">Role</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group focused">
                                        <label class="form-control-label" for="input-username">Role Name</label>
                                        <input type="text" name="name" id="input-username" class="form-control form-control-alternative" placeholder="Username" value="">
                                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                        </div>
                        <h6 class="heading-small text-muted mb-4">Permission</h6>
                        <div class="pl-lg-4">
                            <div class="row">

                                @foreach($permission->chunk(2) as $permissions )
                                    @foreach($permissions as $permiss )

                                        <div class="col-md-6 col-sm-12">
                                       <div class="form-group focused">



                                            <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                                <input class="custom-control-input" name="permissions[]" value="{{$permiss->id}}" id="customCheck{{$permiss->id}}" type="checkbox" >
                                                <label class="custom-control-label" for="customCheck{{$permiss->id}}">{{$permiss['name']}}</label>
                                            </div>
                                            </div>

                                    </div>
                                    @endforeach
                                @endforeach
                                </div>

                            </div>
                        <button type="submit" class="btn btn-block btn-primary">Save</button>





                    </form>
                </div>
            </div>
        </div>
    </div>


@stop

@section('script')
    @if(Session::has('success'))
        <script>
            toastr.success('Permission Saved!', 'Success');
        </script>
    @elseif(Session::has('update'))
        <script>
            toastr.success('Permission Updated!', 'Update');
        </script>
    @endif

    @if ($errors->has('name'))
        {{--<span class="text-danger">{{ $errors->first('name') }}</span>--}}
        <script>
            toastr.error('{{ $errors->first("name") }}', 'Error!');
        </script>

    @endif

    <script>

        function edit(id,name) {
            $('#id').val(id);
            $('#name').val(name);
            $('#edit').modal();



        }
        function delete_role(id) {
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "No, cancel plx!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            })
                .then(isConfirm => {

                    if (isConfirm) {
                        $.ajax({
                            url: "{{route('role.delete')}}",
                            type: 'post',
                            data: {
                                id: id,
                                _token: CSRF_TOKEN,
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                console.log(data);
                                if (data.status == 200) {


                                    swal("Permission has been deleted!", {
                                        icon: "success",
                                    });
                                    // alert($("tr").data("id",id));
                                    // $("#del").attr("data-id", id).remove();
                                    $("#del[data-id="+id+"]").remove();

                                }
                                if (data.status == 199) {


                                    swal("Cancelled", "It's safe.", "error");


                                }

                            },
                        });
                    } else {
                        swal("Cancelled", "It's safe.", "error");
                    }
                });

        }
    </script>
@stop

