@extends('admin.master')
@section('page_name')
    User
@stop
@section('head')

    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dropify.css')}}">

    @stop


@section('header')
    <span class="mask bg-gradient-default opacity-8"></span>
    <div class="row">
        <div class="col-md-12 ">
            <h1 class="display-2 text-white">Edit User</h1>
        </div>
    </div>
@stop

@section('content')
    <style>


    </style>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">User Management</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{route('user')}}" class="btn btn-sm btn-primary">Back to list</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('user.update')}}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <h6 class="heading-small text-muted mb-4">User information</h6>
                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-name">Name</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ $user['name'] }}" required="" autofocus="">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-name">UserName</label>
                                    <input type="text" name="username" id="input-name" class="form-control form-control-alternative {{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="UserName" value="{{ $user['username'] }}" required="" autofocus="">
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Email</label>
                                    <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{ $user['email'] }}" required="">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>




                                <h6 class="heading-small text-muted mb-4">Role</h6>

                               <div class="row">
                                @foreach($roles->chunk(2) as $role )
                                    @foreach($role as $role_s )

                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group focused">



                                                <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                                    <input class="custom-control-input" name="roles[]" @foreach($user->roles as $roless) @if($roless->id==$role_s->id) checked @endif @endforeach value="{{$role_s->id}}" id="customCheck{{$role_s->id}}" type="checkbox" >
                                                    <label class="custom-control-label" for="customCheck{{$role_s->id}}">{{ucfirst($role_s['name'])}}</label>
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                @endforeach
                               </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-block btn-success mt-4">Save</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>


         </div>




@stop

@section('script')
    <script src="{{URL::to('admin_ui/assets/js/dropify.js')}}"></script>
    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();


            // Used events
            var drEvent = $('.dropify-event').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.filename + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });
        });
    </script>

@stop

