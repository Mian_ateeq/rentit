@extends('admin.master')
@section('page_name')
    User
@stop

@section('header')
    <div class="row">
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total User</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($user)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-users"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Role</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($role)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-cogs"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Permission</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($permissions)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-lock"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
@stop

@section('content')

    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Users</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{route('user.create')}}" class="btn btn-sm btn-primary">Add user</a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th class="text-center">Sr</th>
                            <th  class="text-center">UserName</th>
                            <th  class="text-center">Email</th>
                            <th  class="text-center">Profile Pic</th>
                            <th  class="text-center">Role</th>

                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user as $users)
                            <tr id="del" data-id="{{$users->id}}">
                                <th  class="text-center">
                                    {{$loop->iteration}}
                                </th>
                                <td  class="text-center">
                                    {{ucfirst($users['username'])}}
                                </td>
                                <td  class="text-center">
                                    {{$users['email']}}
                                </td>
                                <td  class="text-center">
                                    <img src="{{URL::to($users['image'])}}" alt="" class="rounded-circle" style="width: 100%;
    border-radius: 50%;height: 32px;width: 32px">
                                </td>

                                <td class="text-center">{{ucfirst($users->roles()->pluck('name')->implode(' ') )}}</td>



                                <td class="text-center">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="{{route('user.edit',$users->id)}}" >Edit</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="    border-bottom: 1px solid #e9ecef;">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Permission</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('permission.update')}}">

                    @csrf
                    <input type="hidden" name="id" id="id">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="Enter Name">
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>

                            @endif
                        </div>



                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-block btn-primary">Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('script')
    @if(Session::has('success'))
        <script>
            toastr.success('Permission Saved!', 'Success');
        </script>
    @elseif(Session::has('update'))
        <script>
            toastr.success('Permission Updated!', 'Update');
        </script>
    @endif

    @if ($errors->has('name'))
        {{--<span class="text-danger">{{ $errors->first('name') }}</span>--}}
        <script>
            toastr.error('{{ $errors->first("name") }}', 'Error!');
        </script>

    @endif

    <script>

        function edit(id,name) {
            $('#id').val(id);
            $('#name').val(name);
            $('#edit').modal();



        }
        function delete_permission(id) {
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "No, cancel plx!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            })
                .then(isConfirm => {

                    if (isConfirm) {
                        $.ajax({
                            url: "{{route('permission.delete')}}",
                            type: 'post',
                            data: {
                                id: id,
                                _token: CSRF_TOKEN,
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                console.log(data);
                                if (data.status == 200) {


                                    swal("Permission has been deleted!", {
                                        icon: "success",
                                    });
                                    // alert($("tr").data("id",id));
                                    // $("#del").attr("data-id", id).remove();
                                    $("#del[data-id="+id+"]").remove();

                                }
                                if (data.status == 199) {


                                    swal("Cancelled", "It's safe.", "error");


                                }

                            },
                        });
                    } else {
                        swal("Cancelled", "It's safe.", "error");
                    }
                });

        }
    </script>
@stop

