<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('bookinng')->group(function() {
    Route::group(['middleware' => ['permission:booking']], function () {
        
        Route::get('/', 'BookinngController@index')->name('new_booking');
    Route::get('/Accepted/booking', 'BookinngController@accepted_booking')->name('accepted_booking');
    Route::get('/Rejected/booking', 'BookinngController@rejected_booking')->name('rejected_booking');
    Route::post('/accept', 'BookinngController@accept')->name('accept.booking');
    Route::get('/accept/booking/view', 'BookinngController@calendor')->name('accept.calendor');
    
    Route::get('/invoice/{id}','BookinngController@invoice')->name('invoice');
});
});
Route::prefix('client')->group(function() {
    Route::group(['middleware' => ['permission:client']], function () {
        
        Route::get('/', 'ClientController@index')->name('client');
    Route::get('/create', 'ClientController@create')->name('client.create');
    Route::get('/edit/{id}', 'ClientController@edit')->name('client.edit');
    Route::post('/store', 'ClientController@store')->name('client.store');
    Route::post('/update', 'ClientController@update')->name('client.update');
    Route::post('/destory', 'ClientController@destroy')->name('client.destory');
    Route::get('/client/booking/{id}', 'ClientController@booking')->name('client.booking');
  
});
});
