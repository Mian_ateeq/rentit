<?php

namespace Modules\Bookinng\Entities;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name','s_name','cnic','phone_no','address','city','driving_license'];
}
