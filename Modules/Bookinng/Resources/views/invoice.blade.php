@extends('admin.master')
@section('page_name')
    Invoice
@stop
@section('head')
    
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dropify.css')}}">

@stop


@section('header')
    <span class="mask bg-gradient-default opacity-8"></span>
    <div class="row">
        <div class="col-md-12 ">
            <h1 class="display-2 text-white">Invoice</h1>
        </div>
    </div>
@stop


@section('content')
    <style>
        @media print
        {
            body * { visibility: hidden; overflow-y: hidden; }
            .none{display: none;}
            .print_area * { visibility: visible;  }
            
            /*.qrcode-container-download * { visibility: visible;  }*/
            
            .left{display: none;}
            .right{display: none;}
            
        }
    </style>
    
    <div class="content-body print_area"><section class="card">
            <div id="invoice-template" class="card-body">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-center text-md-left">
                        <div class="media">
                            <img src="{{URL::to('assets/img/logo.png')}}" alt="company logo" class=""/>
                            <div class="media-body">
                                <ul class="ml-2 px-0 list-unstyled">
                                    <li class="text-bold-800">Areolite Rent A car</li>
                                    <li>House # 55,</li>
                                    <li> BlocK G,</li>
                                    <li>Johar Town,</li>
                                    <li>Lahore</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @php
                        $a = str_replace('/','-',$booking->day_out);
                        $with =str_replace('/','-',$booking->day_return);
                        $to = \Carbon\Carbon::createFromFormat('m-d-Y', $with);
                          $from = \Carbon\Carbon::createFromFormat('m-d-Y', $a);
                            $diff_in_days = $to->diffInDays($from);
                    if($diff_in_days==0){
                    $diff_in_days=1;
                    }
                    @endphp
                    <div class="col-md-6 col-sm-12 text-center text-md-right">
                        <h2>INVOICE</h2>
                        <p class="pb-3"># INV-{{$booking['id']}}</p>
                        <ul class="px-0 list-unstyled">
                            <li>Balance Due</li>
                            <li class="lead text-bold-800">RS. {{$booking['car']['per_day_charge']*$diff_in_days}}</li>
                        </ul>
                    </div>
                </div>
                <!--/ Invoice Company Details -->
                
                <!-- Invoice Customer Details -->
                <div id="invoice-customer-details" class="row pt-2">
                    <div class="col-sm-12 text-center text-md-left">
                        <p class="text-muted">Book By</p>
                    </div>
                    <div class="col-md-6 col-sm-12 text-center text-md-left">
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-800"> Mr.{{$booking['name']}}</li>
                            <li class="text-bold-800"> {{$booking['phone_no']}}</li>
                            <li>From</li>
                            <li>{{$booking['city']}}</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12 text-center text-md-right">
                        <p><span class="text-muted">Booking Date :</span>{{\Carbon\Carbon::parse($booking['day_out'])->toFormattedDateString()}}
                        </p>
                        <p><span class="text-muted">Return Date :</span> {{\Carbon\Carbon::parse($booking['day_return'])->toFormattedDateString()}}</p>
                    </div>
                </div>
                <!--/ Invoice Customer Details -->
                
                <!-- Invoice Items Details -->
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="table-responsive col-sm-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Car Name</th>
                                    <th class="text-right">Car Model</th>
                                    <th class="text-right">Rate</th>
                                    <th class="text-right">Day</th>
                                    <th class="text-right">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>
                                        <p>{{$booking['car']['name']}}</p>
                                    </td>
                                    <td class="text-center">
                                        <p>{{$booking['car']['model']}}</p>
                                    </td>
                                    <td class="text-right">Rs. {{$booking['car']['per_day_charge']}}</td>
                                    <td class="text-right">{{$diff_in_days}}</td>
                                    <td class="text-right">Rs {{$booking['car']['per_day_charge'] * $diff_in_days}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-sm-12 text-center text-md-left">
                            <p class="lead">Company Logo:</p>
                            <div class="row">
                                <div class="col-md-8">
                                    <img src="{{URL::to('assets/img/logo.png')}}" alt="company logo" class=""/>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12">
                            <p class="lead">Total due</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td class="text-right">RS. {{$booking['car']['per_day_charge'] * $diff_in_days}}</td>
                                    </tr>
                                   
                                    <tr>
                                        <td class="text-bold-800">Total</td>
                                        <td class="text-bold-800 text-right"> RS. {{$booking['car']['per_day_charge'] * $diff_in_days}}</td>
                                    </tr>
                                 
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <p>Authorized person</p>
                                <img src="{{URL::to('assets/img/signature5ca7b1edb0afe.png')}}" alt="signature" class="height-100"/>
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Invoice Footer -->
                <div id="invoice-footer left">
                    <div class="row">
                        <div class="col-md-7 col-sm-12">
                                </div>
                        <div class="col-md-5 col-sm-12 text-center">
                            <button type="button" class=" left btn btn-primary btn-lg my-1"><i class="fa fa-paper-plane-o"></i> Print Invoice</button>
                        </div>
                    </div>
                </div>
                <!--/ Invoice Footer -->
            
            </div>
        </section>
    </div>
@stop

@section('script')
    
    <script>
		$('.my-1').click(function () {
			
			window.print();
			
			
			
		});
    </script>
    @stop