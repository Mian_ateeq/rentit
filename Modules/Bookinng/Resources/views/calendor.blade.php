@extends('admin.master')
@section('page_name')
    Accepted Booking
@stop
@section('head')
    
    
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/fullcalendar.min.css')}}">



@stop

@section('header')
    
    @stop
@section('content')
    <div class="row">
        <div class="col">
            <!-- Fullcalendar -->
            <div class="card card-calendar">
                <!-- Card header -->
                <div class="card-header">
                    <!-- Title -->
                    <h5 class="h3 mb-0">Calendar</h5>
                </div>
                <!-- Card body -->
                <div class="card-body p-0">
                    <div id='fc-agenda-views'></div>
                </div>
            </div>
            
        </div>
    </div>
    @stop

@section('script')
    
    <script src="{{URL::to('admin_ui/vendors.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('admin_ui/assets/js/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('admin_ui/assets/js/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('admin_ui/fullcalendar.js')}}" type="text/javascript"></script>

    <script>
	    $('#fc-agenda-views').fullCalendar({
		    header: {
			    left: 'prev,next today',
			    center: 'title',
			    right: 'month,agendaWeek,agendaDay'
		    },
		    defaultDate: '{{\Carbon\Carbon::now()}}',
		    defaultView: 'month',
		    editable: false,
		    eventLimit: true, // allow "more" link when too many events
		    events: [
                
                    @foreach($booking as $book)
			    {
				    title : "\n{{ $book['name']}}\n{{$book->car['name']}}\n",
				    start: '{{$book->day_out}}',
                
                    @php
                        $originalDate = $book->date_out;
                   $newDate = date("Y-m-d", strtotime($originalDate));
                
                    @endphp
                        
                        
                            @if($newDate==date('Y-m-d'))
				    color: '#228B22',
                    @else
				    color: '#5e72e4',
                    @endif
	                    textColor:'#fff'
			    },
                @endforeach
		    ]
	    });
    </script>

@stop