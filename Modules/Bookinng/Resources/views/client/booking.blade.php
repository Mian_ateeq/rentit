@extends('admin.master')
@section('page_name')
    Client Booking
@stop
@section('head')
    
    
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/select.bootstrap4.min.css')}}">

@stop

@section('header')
    <div class="row">
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Booking</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($booking)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fa fa-book"></i>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    
    
    </div>
@stop

@section('content')
    <form action="{{route('accept.booking')}}" method="post">
        @csrf
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <h3 class="mb-0">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select name="status" class="form-control" id="">
                                                <option value="1">Accepted</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            
                            </h3>
                        
                        </div>
                        <div class="table-responsive" style="padding: 10px" >
                            <table class="table align-items-center table-flush table-hover dataTable" id="datatable-basic" role="grid" aria-describedby="datatable-basic_info" style="padding: 10px">
                                <thead class="thead-light">
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="table-check-all" type="checkbox">
                                            <label class="custom-control-label" for="table-check-all"></label>
                                        </div>
                                    </th>
                                    <th  class="text-center">Vehicle Name</th>
                                    <th  class="text-center">Vehicle Image</th>
                                    <th  class="text-center">Customer Name</th>
                                    <th  class="text-center">Customer CNIC</th>
                                    <th  class="text-center">No of Days</th>
                                    <th  class="text-center">Booking Date</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($booking as $book)
                                    <tr>
                                        @php
                                            $a = str_replace('/','-',$book->day_out);
                                            $with =str_replace('/','-',$book->day_return);
                                            $to = \Carbon\Carbon::createFromFormat('m-d-Y', $with);
                                              $from = \Carbon\Carbon::createFromFormat('m-d-Y', $a);
                                                $diff_in_days = $to->diffInDays($from);
                                        @endphp
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input check_all_input" name="booking_id[]" value="{{$book->id}}" id="table-check-all{{$book->id}}" type="checkbox">
                                                <label class="custom-control-label" for="table-check-all{{$book->id}}"></label>
                                            </div>
                                        
                                        </td>
                                        <td class="text-center">
                                            
                                            {{ucfirst($book['car']['name'])}}
                                        
                                        </td>
                                        <td class="text-center">
                                            
                                            <img src="{{URL::to($book['car']['car_img1'])}}" style="height: 50px" alt="">
                                        
                                        </td>
                                        <td>
                                            {{ucfirst($book['name'])}}
                                        </td>
                                        <td>
                                            {{ucfirst($book['cnic'])}}
                                        </td>
                                        <td class="text-center">
                                            {{$diff_in_days }} Day
                                        </td>
                                        <td class="text-center">
                                            {{\Carbon\Carbon::parse($book['day_out'])->toFormattedDateString()}}
                                        </td>
                                        <td>
                                    <span class="badge badge-dot mr-4">
                                      <i class="bg-warning"></i>
                                        <span class="status">{{$book->status}}</span>
                                         </span>
                                        </td>
                                    </tr>
                                @endforeach
                                
                                
                                </tbody>
                            </table>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </form>

@stop



@section('script')
    
    @if(Session::has('success'))
        <script>
			toastr.success('Car  Saved!', 'Success');
        </script>
    @elseif(Session::has('update'))
        <script>
			toastr.success('Car  Updated!', 'Update');
        </script>
    @endif
    
    @if ($errors->has('name'))
        {{--<span class="text-danger">{{ $errors->first('name') }}</span>--}}
        <script>
			toastr.error('{{ $errors->first("name") }}', 'Error!');
        </script>
    
    @endif
    
    <script>
		
		
		function edit(id,name) {
			$('#id').val(id);
			$('#name').val(name);
			$('#edit').modal();
			
			
			
		}
		function delete_permission(id) {
			let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this imaginary file!",
				icon: "warning",
				buttons: {
					cancel: {
						text: "No, cancel plx!",
						value: null,
						visible: true,
						className: "",
						closeModal: false,
					},
					confirm: {
						text: "Yes, delete it!",
						value: true,
						visible: true,
						className: "",
						closeModal: false
					}
				}
			})
				.then(isConfirm => {
					
					if (isConfirm) {
						$.ajax({
							url: "{{route('car.delete')}}",
							type: 'post',
							data: {
								id: id,
								_token: CSRF_TOKEN,
							},
							dataType: 'JSON',
							success: function (data) {
								console.log(data);
								if (data.status == 200) {
									
									
									swal("Car  has been deleted!", {
										icon: "success",
									});
									// alert($("tr").data("id",id));
									// $("#del").attr("data-id", id).remove();
									$("#del[data-id="+id+"]").remove();
									
								}
								if (data.status == 199) {
									
									
									swal("Cancelled", "It's safe.", "error");
									
									
								}
								
							},
						});
					} else {
						swal("Cancelled", "It's safe.", "error");
					}
				});
			
		}
    
    </script>
    <script>
		$("#table-check-all").click(function(){
			$('input:checkbox').not(this).prop('checked', this.checked);
		});
    
    </script>
@stop

