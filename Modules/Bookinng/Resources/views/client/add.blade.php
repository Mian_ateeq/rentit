@extends('admin.master')
@section('page_name','Add Client')

@section('header')
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dropify.css')}}">


@stop
@section('content')
    <style>
        .dz-message{
            
            cursor: pointer;
        }
    </style>
    <div class="row">
        <div class="col">
            <div class="card-wrapper">
             
                <!-- Default browser form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Add Client</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                    
                        <form method="post" action="{{route('client.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault01">Name</label>
                                        <input type="text" class="form-control" name="name" id="validationDefault01" placeholder="name" value="{{old('name')}}" required="">
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault02">Father name</label>
                                        <input type="text" class="form-control" name="s_name" id="validationDefault02" placeholder="Father name" value="{{old('s_name')}}" required="">
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefaultUsername">CNIC</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control cc-inputmask" name="cnic" id="validationDefaultUsername" value="{{old('cnic')}}" placeholder="CNIC" aria-describedby="inputGroupPrepend2" required="">
                                            @if ($errors->has('cnic'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cnic') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault03">Phone No</label>
                                        <input type="text" class="form-control international-inputmask" name="phone_no" value="{{old('phone_no')}}" id="validationDefault03" placeholder="Phone No" required="">
                                        @if ($errors->has('phone_no'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_no') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault04">Driving License</label>
                                        <input type="text" class="form-control" name="driving_license" id="validationDefault04" value="{{old('driving_license')}}" placeholder="Driving License" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault05">Address</label>
                                        <textarea  class="form-control" id="validationDefault05" name="address" placeholder="Address" required="">{{old('address')}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault05">City</label>
                                        <input type="text"  class="form-control" id="validationDefault05" name="city"  value="{{old('city')}}" placeholder="City" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault05">CNIC Photo Front</label>
                                        <input type="file" id="input-file-now-custom-1" name="cnic_photo_1"  class="dropify"  />

                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault05">CNIC Photo Back</label>
                                        <input type="file" id="input-file-now-custom-1" name="cnic_photo_2" multiple class="dropify"  />

                                    </div>
                                </div>
                               
                            </div>
                           
                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
    @stop

@section('script')
    <script src="{{URL::to('js/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script>
	    $('.international-inputmask').inputmask("(9999)999-9999");
	    $('.cc-inputmask').inputmask("99999 9999- 9999");
    </script>
   
    @stop