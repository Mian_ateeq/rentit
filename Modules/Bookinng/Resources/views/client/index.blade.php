@extends('admin.master')
@section('page_name')
    Client
@stop
@section('head')
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/select.bootstrap4.min.css')}}">

@stop

@section('header')
    <div class="row">
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Accepted Booking</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($clients)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fa fa-book"></i>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    
    
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">
                          All Client
                        
                        </h3>
                    
                    </div>
                    <div class="table-responsive" style="padding: 10px" >
                        <table class="table align-items-center table-flush table-hover dataTable" id="datatable-basic" role="grid" aria-describedby="datatable-basic_info" style="padding: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>
                                   Sr #
                                </th>
                                <th  class="text-center">Name</th>
                                <th  class="text-center">F Name</th>
                                <th  class="text-center">CNIC</th>
                                <th  class="text-center">Phone No</th>
                                <th  class="text-center">City</th>
                                <th  class="text-center">View Booking</th>
                                <th class="text-center">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                                <tr id="del" data-id="{{$client->id}}">
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($client['name'])}}</td>
                                <td>{{ucfirst($client['s_name'])}}</td>
                                <td>{{$client['cnic']}}</td>
                                <td>{{$client['phone_no']}}</td>
                                <td>{{$client['city']}}</td>
                                <td> <button onclick="window.location.href='{{route("client.booking",$client->cnic)}}'" class="btn btn-outline-info">View Booking</button> </td>
                                <td class="table-actions">
                                    <a href="{{route('client.edit',$client->id)}}" class="table-action" data-toggle="tooltip" data-original-title="Edit Client Information">
                                        <i class="fas fa-user-edit"></i>
                                    </a>
                                    <a onclick="delete_permission('{{$client->id}}')" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete Client">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                                </tr>
                            
                            @endforeach
                            
                            
                            </tbody>
                        </table>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    
    
    @stop

@section('script')
    @if(Session::has('success'))
        <script>
			toastr.success('Client  Saved!', 'Success');
        </script>
    @elseif(Session::has('update'))
        <script>
			toastr.success('Client  Updated!', 'Update');
        </script>
    @endif
    
    @if ($errors->has('name'))
        {{--<span class="text-danger">{{ $errors->first('name') }}</span>--}}
        <script>
			toastr.error('{{ $errors->first("name") }}', 'Error!');
        </script>
    
    @endif
    <script>
     
	    function delete_permission(id) {
		    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		
		    swal({
			    title: "Are you sure?",
			    text: "You will not be able to recover this imaginary file!",
			    icon: "warning",
			    buttons: {
				    cancel: {
					    text: "No, cancel plx!",
					    value: null,
					    visible: true,
					    className: "",
					    closeModal: false,
				    },
				    confirm: {
					    text: "Yes, delete it!",
					    value: true,
					    visible: true,
					    className: "",
					    closeModal: false
				    }
			    }
		    })
			    .then(isConfirm => {
				
				    if (isConfirm) {
					    $.ajax({
						    url: "{{route('client.destory')}}",
						    type: 'post',
						    data: {
							    id: id,
							    _token: CSRF_TOKEN,
						    },
						    dataType: 'JSON',
						    success: function (data) {
							    console.log(data);
							    if (data.status == 200) {
								
								
								    swal("Client  has been deleted!", {
									    icon: "success",
								    });
								    // alert($("tr").data("id",id));
								    // $("#del").attr("data-id", id).remove();
								    $("#del[data-id="+id+"]").remove();
								
							    }
							    if (data.status == 199) {
								
								
								    swal("Cancelled", "It's safe.", "error");
								
								
							    }
							
						    },
					    });
				    } else {
					    swal("Cancelled", "It's safe.", "error");
				    }
			    });
		
	    }
    </script>
    @stop