<?php

namespace Modules\Bookinng\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class BookinngController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $booking=Booking::where('status','Pending')->OrderBy('id','desc')->get();
        return view('bookinng::index',compact('booking'));
    }
    public function accepted_booking()
    {
        $booking=Booking::where('status','Accepted')->OrderBy('id','desc')->get();
        return view('bookinng::accpet',compact('booking'));
    }
    public function rejected_booking()
    {
        $booking=Booking::where('status','Rejected')->OrderBy('id','desc')->get();
        return view('bookinng::reject',compact('booking'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bookinng::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('bookinng::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('bookinng::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function accept(Request $request){
        
        if($request->booking_id){
        
        
        if($request->status==1){
            
            foreach ($request->booking_id as $id){
                
                Booking::where('id',$id)->update(['status'=>"Accepted"]);
            }
            
            
        }
        elseif($request->status==0){
            foreach ($request->booking_id as $id){
        
                Booking::where('id',$id)->update(['status'=>"Rejected"]);
            }
        }
            }
        
        
        return redirect()->back();
        
    }
    
    
    public function calendor(){
        $booking=Booking::where('status','Accepted')->get();
    
        return view('bookinng::calendor',compact('booking'));
    
    }
    
    public function invoice($id){
        
        $booking=Booking::find($id);
        return view('bookinng::invoice',compact('booking'));
    }
}
