<?php

namespace Modules\Bookinng\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Bookinng\Entities\Client;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $clients=Client::OrderBy('id','desc')->get();
        return view('bookinng::client.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
        
        return view('bookinng::client.add');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cnic'=>'required|unique:clients',
            'phone_no'=>'required',
        ]);
      
        $client=new Client($request->all());
    
        if ($request->exists('cnic_photo_1')) {
            $file = Input::file('cnic_photo_1');
            $path = $file->move('uploads/client', str_random(10) . $file->getClientOriginalName());
            $client->cnic_photo_1 = $path;
        
        
        }
        if ($request->exists('cnic_photo_2')) {
            $file = Input::file('cnic_photo_2');
            $path = $file->move('uploads/client', str_random(10) . $file->getClientOriginalName());
            $client->cnic_photo_2 = $path;
        
        
        }
        $client->save();
    
        session()->flash('success', '');
        
        
        return redirect()->route('client');
        
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('bookinng::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        
        $client=Client::find($id);
        return view('bookinng::client.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'cnic' => "required|unique:clients,cnic,$request->id",
            'phone_no'=>'required',
        ]);
        $client=Client::find($request->id);
    
        if ($request->exists('cnic_photo_1')) {
            $file = Input::file('cnic_photo_1');
            $path = $file->move('uploads/client', str_random(10) . $file->getClientOriginalName());
            $client->cnic_photo_1 = $path;
        
        
        }
        if ($request->exists('cnic_photo_2')) {
            $file = Input::file('cnic_photo_2');
            $path = $file->move('uploads/client', str_random(10) . $file->getClientOriginalName());
            $client->cnic_photo_2 = $path;
        
        
        }
        $client->update($request->all());
        session()->flash('update', '');
    
    
    
        return redirect()->route('client');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request)
    {
    
        $cartype=Client::where('id',$request->id)->first();
    
        if($cartype) {
            $cartype->delete();
            return json_encode(["status" => 200]);
        }
        else {
            return json_encode(["status" => 199]);
        }
    }
    
    
    public function booking($cnic){
    
        
        $string1 = str_replace('-', '', $cnic);
        $cnic_orinal= preg_replace('/\s+/', '', $string1);
        
        $booking=Booking::where('cnic',$cnic_orinal)->get();
        
        return view('bookinng::client.booking',compact('booking'));
        
    }
}
