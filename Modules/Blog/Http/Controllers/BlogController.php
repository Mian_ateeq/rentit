<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Blog\Entities\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $blogs=Blog::OrderBy('id','desc')->get();
        return view('blog::index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('blog::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//       return $request->all();
        
        
        $post=new Blog($request->all());
    
        if ($request->exists('cnic_photo_2')) {
           
            $file = Input::file('cnic_photo_2');
            $path = $file->move('uploads/post', str_random(10) . $file->getClientOriginalName());
            $post->image = $path;
            
        
        
        }
    
        $post->save();
        session()->flash('save','');
    
        return redirect()-> route('blog');
    
    
    
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('blog::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $post=Blog::find($id);
        return view('blog::edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $post=Blog::find($request->id);
    
        if ($request->exists('cnic_photo_2')) {
        
            $file = Input::file('cnic_photo_2');
            $path = $file->move('uploads/post', str_random(10) . $file->getClientOriginalName());
            $post->image = $path;
        
        
        
        }
    
        $post->update($request->all());
        session()->flash('save','');
    
        return redirect()-> route('blog');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $cartype=Blog::where('id',$request->id)->first();
    
        if($cartype) {
            $cartype->delete();
            return json_encode(["status" => 200]);
        }
        else {
            return json_encode(["status" => 199]);
        }
    }
}
