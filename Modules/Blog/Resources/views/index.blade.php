@extends('admin.master')
@section('page_name')
    Post
@stop
@section('head')
    {{--<script src="../../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>--}}
    
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/select.bootstrap4.min.css')}}">

@stop

@section('header')
    <div class="row">
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total Post</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($blogs)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fab fa-blogger-b"></i>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    
    
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card shadow">
                    <div class="card-header border-0">
                    </div>
                    <div class="table-responsive" style="padding: 10px" >
                        <table class="table table-flush dataTable" id="datatable-basic" role="grid" aria-describedby="datatable-basic_info" style="padding: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th class="text-center">Sr</th>
                                <th  class="text-center">Post Name</th>
                                <th  class="text-center">Image</th>
                                <th  class="text-center">Date</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($blogs as $post)
                                <tr id="del" data-id="{{$post->id}}">
                                    <th  class="text-center">
                                        {{$loop->iteration}}
                                    </th>
                                    <td  class="text-center">
                                        {{ucfirst($post['name'])}}
                                    </td>
                                    <td  class="text-center">
                                        <img src="{{URL::to($post->image)}}" style="height: 50px" alt="">
                                    </td>
                                   <td class="text-center">
                                       {{\Carbon\Carbon::parse($post['created_at'])->toFormattedDateString()}}
                                   </td>
    
    
    
                                    <td class="table-actions" style="text-align: center">
                                        <a href="{{route('blog.edit',$post->id)}}" class="table-action" data-toggle="tooltip" data-original-title="Edit Client Information">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a onclick="delete_permission('{{$post->id}}')" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete Client">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            
                            </tbody>
                        </table>
                    </div>
                
                </div>
            </div>
        </div>
    </div>

@stop



@section('script')
    
    @if(Session::has('success'))
        <script>
			toastr.success('Car  Saved!', 'Success');
        </script>
    @elseif(Session::has('update'))
        <script>
			toastr.success('Car  Updated!', 'Update');
        </script>
    @endif
    
    @if ($errors->has('name'))
        {{--<span class="text-danger">{{ $errors->first('name') }}</span>--}}
        <script>
			toastr.error('{{ $errors->first("name") }}', 'Error!');
        </script>
    
    @endif
    
    <script>
		
		
		function edit(id,name) {
			$('#id').val(id);
			$('#name').val(name);
			$('#edit').modal();
			
			
			
		}
		function delete_permission(id) {
			let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this imaginary file!",
				icon: "warning",
				buttons: {
					cancel: {
						text: "No, cancel plx!",
						value: null,
						visible: true,
						className: "",
						closeModal: false,
					},
					confirm: {
						text: "Yes, delete it!",
						value: true,
						visible: true,
						className: "",
						closeModal: false
					}
				}
			})
				.then(isConfirm => {
					
					if (isConfirm) {
						$.ajax({
							url: "{{route('blog.destroy')}}",
							type: 'post',
							data: {
								id: id,
								_token: CSRF_TOKEN,
							},
							dataType: 'JSON',
							success: function (data) {
								console.log(data);
								if (data.status == 200) {
									
									
									swal("Blog  has been deleted!", {
										icon: "success",
									});
									// alert($("tr").data("id",id));
									// $("#del").attr("data-id", id).remove();
									$("#del[data-id="+id+"]").remove();
									
								}
								if (data.status == 199) {
									
									
									swal("Cancelled", "It's safe.", "error");
									
									
								}
								
							},
						});
					} else {
						swal("Cancelled", "It's safe.", "error");
					}
				});
			
		}
    </script>
    <script>
		
		function myFunction(id,status) {
			let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			
			
			$.ajax({
				url: "{{route('car.status')}}",
				type: 'post',
				data: {
					id: id,
					st: status,
					_token: CSRF_TOKEN,
				},
				dataType: 'JSON',
				success: function (data) {
				
				}
			});
			
			
		}
    
    </script>
@stop

