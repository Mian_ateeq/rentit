@extends('admin.master')
@section('page_name','Add POst')

@section('header')
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dropify.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">


@stop
@section('content')
    <style>
        .dz-message{
            
            cursor: pointer;
        }
    </style>
    <div class="row">
        <div class="col">
            <div class="card-wrapper">
                
                <!-- Default browser form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Add Post</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        
                        <form method="post" action="{{route('blog.update')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$post['id']}}">
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault01">Name</label>
                                        <input type="text" class="form-control" name="name" id="validationDefault01" placeholder="name" value="{{$post['name']}}" required="">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault01">Body</label>
                                        <textarea type="text" class="form-control" name="body" id="summernote" placeholder="name"  required="">{{$post['body']}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault01">Seo Tittle</label>
                                        <input type="text" class="form-control" name="seo_tittle" id="validationDefault01" placeholder="Seo Tittle" value="{{$post['seo_tittle']}}" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault02">Seo Keyword</label>
                                        <input type="text" class="form-control" name="seo_keyword" id="validationDefault02" placeholder="Seo Keyword" value="{{$post['seo_keyword']}}" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault05">Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="cnic_photo_2" data-default-file="{{URL::to($post['image'])}}" multiple class="dropify"  />
        
                                    </div>
                                </div>
                               
                              
                            </div>
                           
                            
                            <button class="btn btn-primary btn-block" type="submit">Update</button>
                        </form>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{URL::to('js/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>
    
    <script>
		$('.international-inputmask').inputmask("(9999)999-9999");
		$('.cc-inputmask').inputmask("99999 9999- 9999");
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
    <script>
	    $(document).ready(function() {
		    $('#summernote').summernote({
			    tabsize: 2,
			    height: 200
            });
	    });
    </script>

@stop