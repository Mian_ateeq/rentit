<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('blog')->group(function() {
    Route::group(['middleware' => ['permission:blog']], function () {
    Route::get('/', 'BlogController@index')->name('blog');
    Route::get('/create', 'BlogController@create')->name('blog.create');
    Route::get('/edit/{id}', 'BlogController@edit')->name('blog.edit');
    Route::post('/store', 'BlogController@store')->name('blog.store');
    Route::post('/update', 'BlogController@update')->name('blog.update');
    Route::post('/destroy', 'BlogController@destroy')->name('blog.destroy');
});
});
