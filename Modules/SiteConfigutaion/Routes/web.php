<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('siteconfigutaion')->group(function() {
    
    Route::group(['middleware' => ['permission:site_config']], function () {
        
        Route::get('/slider', 'SiteConfigutaionController@index')->name('slider');
    Route::post('/slider/store', 'SiteConfigutaionController@store')->name('slider.post');
    Route::post('/slider/update', 'SiteConfigutaionController@update')->name('slider.update');
    Route::post('/slider/delete', 'SiteConfigutaionController@destroy')->name('slider.destroy');
    
    
    
    Route::get('/', 'SiteConfigutaionController@site')->name('site.setting');
    Route::post('/save', 'SiteConfigutaionController@site_save')->name('site.setting.save');
    
});
});
