@extends('admin.master')
@section('page_name','Site Setting')

@section('header')
    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dropify.css')}}">


@stop
@section('content')
    <style>
        .dz-message{
            
            cursor: pointer;
        }
    </style>
    <div class="row">
        <div class="col">
            <div class="card-wrapper">
                
                <!-- Default browser form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Site Setting</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        
                        <form method="post" action="{{route('site.setting.save')}}" enctype="multipart/form-data">
                            @csrf
                            @isset($site)
                                <input type="hidden" name="id" value="{{$site['id']}}">
                                @endisset
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault01">Email</label>
                                        <input type="email" class="form-control" name="email" id="validationDefault01" placeholder="email"
                                               @isset($site) value="{{$site['site_email']}}" @endisset >
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault02">Logo</label>
                                        <input type="file" id="input-file-now-custom-1" name="logo" multiple class="dropify"
                                               @isset($site) data-default-file="{{URL::to($site->logo)}}" @endisset  />

                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault02">Favicon</label>
                                        <input type="file" id="input-file-now-custom-1" name="favicon" multiple class="dropify"
                                               @isset($site) data-default-file="{{URL::to($site->favicon)}}" @endisset/>
        
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault02">Footer Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="footer_image" multiple class="dropify"
                                               @isset($site) data-default-file="{{URL::to($site->footer_image)}}" @endisset/>

                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault02">Counter Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="counter_image" multiple class="dropify"
                                               @isset($site) data-default-file="{{URL::to($site->counter_image)}}" @endisset/>

                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="validationDefault02">Page Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="page_image" multiple class="dropify"
                                               @isset($site) data-default-file="{{URL::to($site->page_image)}}" @endisset/>

                                    </div>
                                </div>
                               
                                
                            </div>
                           
                            
                            <button class="btn btn-primary" type="submit">Save</button>
                        </form>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{URL::to('js/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>
    
    <script>
		$('.international-inputmask').inputmask("(9999)999-9999");
		$('.cc-inputmask').inputmask("99999 9999- 9999");
    </script>

@stop