<?php

namespace Modules\SiteConfigutaion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\SiteConfigutaion\Entities\SiteConfig;
use Modules\SiteConfigutaion\Entities\Slider;

class SiteConfigutaionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $sliders=Slider::OrderBy('id','desc')->get();
        return view('siteconfigutaion::slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('siteconfigutaion::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    
        $slider=new Slider();
        if ($request->exists('image')) {
            $file = Input::file('image');
            $path = $file->move('uploads/slider', str_random(10) . $file->getClientOriginalName());
            $slider->image = $path;
        
        
        }
        
        $slider->save();
        
        return redirect()->back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('siteconfigutaion::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('siteconfigutaion::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $slider=Slider::find($request->id);
        if ($request->exists('image')) {
            $file = Input::file('image');
            $path = $file->move('uploads/slider', str_random(10) . $file->getClientOriginalName());
            $slider->image = $path;
        
        
        }
    
        $slider->save();
    
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $cartype=Slider::where('id',$request->id)->first();
    
        if($cartype) {
            $cartype->delete();
            return json_encode(["status" => 200]);
        }
        else {
            return json_encode(["status" => 199]);
        }
    }
    
    public function site(){
    
        $site=SiteConfig::find(1);
    
    
        return view('siteconfigutaion::index',compact('site'));
    
    }
    
    public function site_save(Request $request){
    
        if($request->id){
    
            $slider=SiteConfig::find($request->id);
    
        }
        else{
    
            $slider=new SiteConfig();
        }
        
        
        
        if ($request->exists('logo')) {
            $file = Input::file('logo');
            $path = $file->move('uploads/slider', str_random(10) . $file->getClientOriginalName());
            $slider->logo = $path;
        
        
        }
        if ($request->exists('favicon')) {
            $file = Input::file('favicon');
            $path = $file->move('uploads/slider', str_random(10) . $file->getClientOriginalName());
            $slider->favicon = $path;
        
        
        }
        if ($request->exists('footer_image')) {
            $file = Input::file('footer_image');
            $path = $file->move('uploads/slider', str_random(10) . $file->getClientOriginalName());
            $slider->footerImage = $path;
        
        
        }
        if ($request->exists('counter_image')) {
            $file = Input::file('counter_image');
            $path = $file->move('uploads/slider', str_random(10) . $file->getClientOriginalName());
            $slider->counter_image = $path;
        
        
        }
        if ($request->exists('page_image')) {
            $file = Input::file('page_image');
            $path = $file->move('uploads/slider', str_random(10) . $file->getClientOriginalName());
            $slider->page_image = $path;
        
        
        }
        
        $slider->site_email=$request->email;
    
        if($request->id){
    
            $slider->update();
    
        }
        else{
    
            $slider->save();
        }
        
        
        return redirect()->back();
    }
}
