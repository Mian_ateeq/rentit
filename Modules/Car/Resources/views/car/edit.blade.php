@extends('admin.master')
@section('page_name')
   Vehicle
@stop
@section('head')

    <link rel="stylesheet" href="{{URL::to('admin_ui/assets/css/dropify.css')}}">

@stop


@section('header')
    <span class="mask bg-gradient-default opacity-8"></span>
    <div class="row">
        <div class="col-md-12 ">
            <h1 class="display-2 text-white">Add Vehicle</h1>
        </div>
    </div>
@stop

@section('content')
    <style>
        .dropify-font-upload:before, .dropify-wrapper .dropify-message span.file-icon:before {
            display: none;        }

    </style>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Vehicle Information</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{route('car')}}" class="btn btn-sm btn-primary" >Back to list</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('car.update')}}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$car['id']}}">
                            <h6 class="heading-small text-muted mb-4">Vehicle information</h6>
                            <div class="pl-lg-4">
                               <div class="row">
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label class="form-control-label" for="input-name">Vehicle Name</label>
                                           <input type="text" name="name" id="input-name" class="form-control form-control-alternative {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ $car['name'] }}" required="" autofocus="">
                                           @if ($errors->has('name'))
                                               <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                          </span>
                                           @endif
                                       </div>
                                   </div>
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label class="form-control-label" for="input-name">Vehicle Type</label>
                                           <select name="vehicle_type" id="" class="form-control">
                                              @foreach($vehicle_type as $car_type)
                                                   <option value="{{$car_type->id}}" @if($car_type->vehicle_type==$car_type->id) selected @endif>{{ucfirst($car_type['name'])}}</option>
                                                  @endforeach
                                           </select>
                                                @if ($errors->has('vehicle_type'))
                                               <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('vehicle_type') }}</strong>
                                          </span>
                                           @endif
                                       </div>
                                   </div>
                               </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name">Fuel Type</label>
                                            <select name="fuel_type" id="" class="form-control">
                                                <option value="Petrol" @if($car->fuel_type=="Petrol") selected @endif>Petrol</option>
                                                <option value="Diesel"  @if($car->fuel_type=="Diesel") selected @endif>Diesel</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name">Engine Type</label>
                                            <select name="engine_type" id="" class="form-control">
                                                <option value="Manual" @if($car->engine_type=="Manual") selected @endif>Manual</option>
                                                <option value="Automatic"  @if($car->engine_type=="Automatic") selected @endif> Automatic</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name">KM Drive</label>
                                            <input type="number" name="km_drive" id="input-name" class="form-control form-control-alternative {{ $errors->has('km_drive') ? ' is-invalid' : '' }}" placeholder="KM Drive" value="{{ $car['km_drive'] }}" required="" autofocus="">
                                            @if ($errors->has('km_drive'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('km_drive') }}</strong>
                                          </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name">Model</label>
                                            <input type="number" name="model" id="input-name"  class="form-control form-control-alternative {{ $errors->has('model') ? ' is-invalid' : '' }}" placeholder="Model" value="{{ $car['model'] }}" required="" autofocus="">
                                            @if ($errors->has('model'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('model') }}</strong>
                                          </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Engine Power</label>
                                        <input type="text" name="engine_power" class="form-control" value="{{$car['engine_power']}}" placeholder="Per Day Charge">
                                    </div>
                                </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Per Day Charge</label>
                                            <input type="number" name="per_day_charge" class="form-control" placeholder="Per Day Charge" value="{{$car['per_day_charge']}}">
                                        </div>
                                    </div>

                                </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Car Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="car_img1" multiple class="dropify" data-default-file="{{URL::to($car['car_img1'])}}" required  />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Car Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="car_img2" multiple class="dropify" data-default-file="{{URL::to($car['car_img2'])}}"  />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Car Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="car_img3" multiple class="dropify" data-default-file="{{URL::to($car['car_img3'])}}"  />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Car Image</label>
                                        <input type="file" id="input-file-now-custom-1" name="car_img4" multiple class="dropify" data-default-file="{{URL::to($car['car_img4'])}}"  />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Car Detail</label>
                                        <textarea name="description" class="form-control" id="" cols="30" rows="10" placeholder="Car Detail">{{$car['description']}}</textarea>
                                    </div>
                                </div>
                            </div>





                                <div class="text-center">
                                    <button type="submit" class="btn btn-block btn-success mt-4">Save</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>


    </div>




@stop

@section('script')
    <script src="{{URL::to('admin_ui/assets/js/dropify.js')}}"></script>
    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();


            // Used events
            var drEvent = $('.dropify-event').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.filename + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });
        });
    </script>

@stop

