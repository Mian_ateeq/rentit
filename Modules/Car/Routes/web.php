<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('car')->group(function() {
    Route::group(['middleware' => ['permission:vehicle']], function () {
        
        Route::get('/type', 'TypeController@index')->name('cartype');
    Route::post('/type/store', 'TypeController@store')->name('cartype.store');
    Route::post('/type/update', 'TypeController@update')->name('cartype.update');
    Route::post('/type/delete', 'TypeController@destroy')->name('cartype.delete');






    Route::get('/', 'CarController@index')->name('car');
    Route::get('/create', 'CarController@create')->name('car.create');
    Route::get('/edit/{id}', 'CarController@edit')->name('car.edit');
    Route::post('/store', 'CarController@store')->name('car.store');
    Route::post('/update', 'CarController@update')->name('car.update');
    Route::post('/delete', 'CarController@destroy')->name('car.delete');
    Route::post('/change', 'CarController@status')->name('car.status');

});
});
