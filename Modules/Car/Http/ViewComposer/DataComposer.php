<?php
/**
 * Created by PhpStorm.
 * User: ateeq
 * Date: 2/18/19
 * Time: 11:24 PM
 */

namespace Modules\Car\Http\ViewComposer;


use Illuminate\View\View;
use Modules\Blog\Entities\Blog;
use Modules\Car\Entities\CarType;
use Modules\SiteConfigutaion\Entities\SiteConfig;
use Modules\SiteConfigutaion\Entities\Slider;

class DataComposer
{
    public function compose(View $view)
    {
        $tye=CarType::all();
        $site=SiteConfig::find(1);
        $blogs=Blog::OrderBy('id')->paginate(6);
        $sliders=Slider::OrderBy('id','desc')->get();
        $view->with('tye', $tye);
        $view->with('sliders', $sliders);
        $view->with('blogs', $blogs);
        $view->with('site', $site);
    
    }
}