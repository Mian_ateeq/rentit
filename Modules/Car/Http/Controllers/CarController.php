<?php

namespace Modules\Car\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Car\Entities\Car;
use Modules\Car\Entities\CarType;
use File;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cars=Car::OrderBY('id','desc')->get();
        return view('car::car.index',compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $vehicle_type=CarType::all();
        return view('car::car.create',compact('vehicle_type'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//        return $request->all();

        $request->validate([
           'name'=>'required',
           'vehicle_type'=>'required',
           'fuel_type'=>'required',
           'engine_type'=>'required',
           'km_drive'=>'required',
           'model'=>'required',
           'car_img1'=>'required',
           'per_day_charge'=>'required',
           'description'=>'required',
        ]);

        $car=new Car();
        $car['name']=$request->name;
        $car['vehicle_type']=$request->vehicle_type;
        $car['fuel_type']=$request->fuel_type;
        $car['Engine_type']=$request->engine_type;
        $car['km_drive']=$request->km_drive;
        $car['model']=$request->model;
        $car['description']=$request->description;
        $car['engine_power']=$request->engine_power;
        $car['per_day_charge']=$request->per_day_charge;
        if ($request->exists('car_img1')) {
            $file = Input::file('car_img1');
            $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
            $car->car_img1 = $path;


        }  if ($request->exists('car_img2')) {
            $file = Input::file('car_img2');
            $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
            $car->car_img2 = $path;


        }  if ($request->exists('car_img3')) {
            $file = Input::file('car_img3');
            $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
            $car->car_img3 = $path;


        }  if ($request->exists('car_img4')) {
            $file = Input::file('car_img4');
            $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
            $car->car_img4 = $path;


        }

        $car->save();


        return redirect()->route('car');






    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('car::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $car=Car::find($id);
        $vehicle_type=CarType::all();
        return view('car::car.edit',compact('car','vehicle_type'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'vehicle_type'=>'required',
            'fuel_type'=>'required',
            'engine_type'=>'required',
            'km_drive'=>'required',
            'model'=>'required',
            'per_day_charge'=>'required',
            'description'=>'required',
        ]);

        $car=Car::find($request->id);
        $car['name']=$request->name;
        $car['vehicle_type']=$request->vehicle_type;
        $car['fuel_type']=$request->fuel_type;
        $car['Engine_type']=$request->engine_type;
        $car['km_drive']=$request->km_drive;
        $car['model']=$request->model;
        $car['description']=$request->description;
        $car['engine_power']=$request->engine_power;

        $car['per_day_charge']=$request->per_day_charge;
        if ($request->exists('car_img1')) {
            File::delete($car->car_img1);
            $file = Input::file('car_img1');
            $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
            $car->car_img1 = $path;


        }  if ($request->exists('car_img2')) {
        File::delete($car->car_img2);
        $file = Input::file('car_img2');
        $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
        $car->car_img2 = $path;


    }  if ($request->exists('car_img3')) {
        File::delete($car->car_img3);
        $file = Input::file('car_img3');
        $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
        $car->car_img3 = $path;


    }  if ($request->exists('car_img4')) {
        File::delete($car->car_img4);

        $file = Input::file('car_img4');
        $path = $file->move('uploads/image', str_random(10) . $file->getClientOriginalName());
        $car->car_img4 = $path;


    }

        $car->update();


        return redirect()->route('car');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $cartype=Car::where('id',$request->id)->first();

        if($cartype) {
            $cartype->delete();
            return json_encode(["status" => 200]);
        }
        else {
            return json_encode(["status" => 199]);
        }
    }
    
    
    public function status(Request $request){
        
       
        
        $car=Car::where('id',$request->id)->first();
        
        if($car->status!=0){
            
           
            $car['status']=0;
            $car->update();
        }
        else{
    
            $car['status']=1;
            $car->update();
        }
        
        
        
    }
}
