<?php

namespace Modules\Car\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\Car\Entities\CarType;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $car_types=CarType::all();
        return view('car::car_type.index',compact('car_types'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('car::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name'=>'required|unique:car_types',
        ]);

        $name = $request['name'];
        $permission = new CarType();
        $permission->name = $name;
        $permission->save();
        session::flash('success', '');

        return redirect()->back();
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('car::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('car::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'=>"required|unique:car_types,name,$request->id",
        ]);

        $name = $request['name'];
        $permission =CarType::find($request->id);
        $permission->name = $name;
        $permission->update();
        session::flash('update', '');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $cartype=CarType::where('id',$request->id)->first();

        if($cartype) {
            $cartype->delete();
            return json_encode(["status" => 200]);
        }
        else {
            return json_encode(["status" => 199]);
        }
    }
}
