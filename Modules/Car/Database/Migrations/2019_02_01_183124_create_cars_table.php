<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('vehicle_type');
            $table->string('slug')->nullable();
            $table->longText('car_img1');
            $table->longText('car_img2')->nullable();
            $table->longText('car_img3')->nullable();
            $table->longText('car_img4')->nullable();
            $table->enum('fuel_type',['Petrol','Diesel']);
            $table->enum('Engine_type',['Manual','Automatic']);
            $table->string('km_drive');
            $table->string('model');
            $table->longText('description')->nullable();
            $table->longText('engine_power')->nullable();
            $table->integer('per_day_charge');
            $table->integer('status')->default(1);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
