<?php
    
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();

            $table->timestamps();
        });
    
        $data = array(
            array('name' => 'BMW'),
            array('name' => 'LIMOS'),
            array('name' => 'Alfa Romeo'),
            array('name' => 'Aston Martin'),
            array('name' => 'SEDANS'),
            array('name' => 'SUVs'),
     
        );
    
        DB::table('car_types')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_types');
    }
}
